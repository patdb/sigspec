#include "ctype.h"
#include "float.h"
#include "limits.h"
#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"

#define MAXCHAR 65536
#define SPCOLS 7
#define ITDIGITS 6
#define TIDIGITS 6
#define MFDIGITS 6
#define SIMPAR 6
#define GAUSSQUAL 10
#define RESCOLS 8
#define SOCKPH 30
#define PHDISTPH 30

#define PI 3.14159265358979323846264
#define LG_E 0.434294481903251827651128918916605

struct String
{
   int l;
   int* c;
};

struct TS
{
   int fi;                      
   int it;                      
   double** d;                  
   struct String** s;           
   int c;                       
   int n;                       
   int r;                       
   int* cf;                     
   int* cw;                     
   double* w;                   
   int* ss;                     
   int nss;                     
   int* nsse;                   
   int ct;                      
   int pi;                      
   double rms;                  
   double ppsc;                 
   double meantime;             

                 
};

struct FFT
{
   double* a;                   
   double* b;                   
};

struct Profile
{
   double* a0;                  
   double* b0;                  
   double* t0;                  
};

struct SigSpec
{
   double* a;                   
   double* b;                   
   double* sig;                 
   double norm;                 
   double* ca;                  
   double* cb;                  
   double scale;
   double rms;
};

struct Harmonic
{
   double sig;                  
   double amp;                  
   double th;                   
   double a0;                   
   double b0;                   
   double t0;                   
};

struct Result
{
   struct Harmonic* h;
   double f;                    
   double sig;                  
   double rms;                  
   double ppsc;                 
   double csig;                 
};

int DataFile_ColWidth(          
   FILE* F,
   int col
   );
int DataFile_CountCol(          
   FILE* F
   );
int DataFile_CountRows(         
   FILE* F
   );
int DataFile_CountStartString(  
   FILE* F,
   struct String*
   );
int DataFile_Display(           
   double** data,
   struct String**,
   int startcol,
   int endcol,
   int startrow,
   int endrow,
   int* numcol,
   int* cwidth
   );
int DataFile_NumericCols(       
   FILE* F,
   int* col,
   int startcol,
   int endcol,
   int startrow,
   int endrow
   );
int DataFile_Read(              
   FILE* F,
   double** data,
   struct String**,
   int startcol,
   int endcol, 
   int startrow,
   int endrow,
   int* numcol
   );
int DataFile_StrLen(            
   FILE* F,
   struct String**,
   int startcol,
   int endcol, 
   int startrow,
   int endrow,
   int* numcol
   );
int DataFile_Write(             
   FILE* F,
   double** data,
   struct String**,
   int startcol,
   int endcol,
   int startrow,
   int endrow,
   int* numcol,
   int* cwidth
   );
int DataFile_WriteDouble(       
   FILE* F,
   double** data,
   int startcol,
   int endcol,
   int startrow,
   int endrow
   );
int Dataset_GetIndex(           
   int rawindex,
   int*
   );
int Dataset_NConsistency(       
   double**,
   double**,
   int startcol,
   int endcol,
   int startrow,
   int endrow
   );
double Dataset_NSortAsc(        
   double**,
   struct String**,
   int sortcol, 
   int dstart,
   int dend,
   int sstart,
   int send,
   int startrow,
   int endrow,
   int*,
   char*
   );
double DFT_Amp(                 
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end, 
   double freq
   );
int DFT_MaxAmp(                 
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   double freq,
   double range,
   double* fout,
   double* rout,
   double* phout
   );
double DFT_Ph(                  
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end, 
   double freq
   );
double DFT_Pow(                 
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   double freq
   );
int FFT_AmpSp(                  
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   double lf,
   double uf,
   int freqs,
   double* fa,
   double* a,
   double* b
   );
int FFT_AmpSpW(                 
   double* tdata,
   double* weight,
   int start,
   int end,
   double lf,
   double uf,
   int freqs,
   double* fa,
   double* sp,
   double* ph
   );
int NumFormat_FixDigits(        
   int val,
   struct String*
   );
double Stat_AutoCorr(           
   double* data,
   double* weight,
   int start,
   int end,
   int lag,
   double llim,
   double ulim,
   int* valid
   );
int Stat_Correlogram(           
   double* data,
   double* weight,
   int lorder,
   int uorder,
   int start,
   int end,
   FILE* F,
   int* valid
   );
double Stat_Mean(               
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   );
double Stat_PPScatter(          
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   );
double Stat_SDev(               
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   );
double Stat_Variance(           
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   );
double Stat_ZeroMean(           
   double* data,
   double* weight,
   int start,
   int end,
   int* ssid,
   int ss,
   int nss,
   double* zm
   );
char* String_AsChars(           
   struct String*,
   char* c
   );
struct String*
   String_Chars2String(         
   char* c,
   struct String*
   );
int String_Comp(                
   struct String*,
   struct String*
   );
int String_Comp2Char(           
   struct String*,
   char* to
   );
int String_Copy(                
   struct String*,
   struct String*,
   int at
   );
int String_Display(             
   struct String*,
   int len
   );
int String_IsEmpty(             
   struct String*
   );
int String_GetLength(           
   struct String*
   );
int String_Read(                
   struct String*,
   FILE* F
   );
int String_Reset(               
   struct String*
   );
int String_GetChar(
   struct String* str,
   int pos
   );
int String_SetChar(             
   struct String* str,
   char c,
   int pos
   );
int String_SetChars(            
   struct String*,
   char* c,
   int pos
   );
int String_SetLength(           
   struct String*,
   int len
   );
int String_Write(               
   struct String*,
   FILE* F,
   int length
   );
int Vector_Flush(               
   double*,
   int start,
   int end
   );
double Vector_SProd(            
   double*,
   int start1,
   int end1,
   double*,
   int start2,
   int end2
   );
int AntiAlC_Candidates(         
   struct TS*,
   struct TS*,
   struct Profile*,
   struct SigSpec*,
   struct Result**,
   int*,
   struct TS*,
   struct TS*,
   struct SigSpec*,
   int
   );
int AntiAlC_Cascade(            
   struct TS*,
   struct TS*,
   struct Profile*,
   struct SigSpec*,
   struct Result*,
   struct TS*,
   struct TS**,
   struct SigSpec*,
   int
   );
int AntiAlC_Sort(               
   struct Result*,
   int
   );
int CmdLine_Scan(               
   int argc,
   char* argv[]
   );
int Correlogram_Generate(       
   struct TS*,
   struct TS*,
   int ti
   );
int Debug_AntiAlC(              
   int iteration,
   int iterations,
   int level,
   int levels,
   int*,
   int*,
   struct Result*,
   struct Result*,
   struct Result*,
   struct TS*,
   struct TS*,
   char*
   );
int Debug_MultiSine(            
   int iteration,
   int signals,
   struct Result*,
   struct Result*,
   struct Result*,
   struct TS*,
   struct TS*,
   char*
   );
int Diff_Comp(                 
   struct TS*,
   struct TS*,
   double freq,
   double*,
   double*
   );
double Diff_CompAlign(         
   struct TS*,
   struct TS*,
   double freq,
   double*
   );
int Diff_Init(                  
   double*,
   double*
   );
int Diff_Level(                 
   struct TS*,
   struct TS*,
   double freq,
   struct TS*,
   double*,
   double*
   );
int Diff_SpComp(                
   struct TS*,
   struct TS*,
   struct SigSpec*
   );
int Diff_SpCompAlign(           
   struct TS*,
   struct TS*,
   struct SigSpec*
   );
int Diff_SpWhite(               
   double*
   );
int File_Name(                  
   struct String*,
   int fi,
   int sep,
   int cpi,
   char* ftype,
   int it,
   int ti,
   char* ext,
   struct String*
   );
int File_NameLength(            
   struct String* path,
   int fi,
   int sep,
   int cpi,
   char* ftype,
   int it,
   int ti,
   char* ext
   );
double Fit_Amp(                 
   struct TS*,
   struct TS*,
   double f,
   double *ph
   );
double Fit_Phase(               
   struct TS*,
   struct TS*,
   double f
   );
int Fit_Prewhitening(           
   struct TS*,
   struct TS*,
   struct Result*,
   struct TS*
   );
int Fit_SinglePrewhitening(     
   struct TS*,
   struct TS*,
   double f,
   double*,
   double*,
   struct TS*
   );
int Harmonics_Write (           
   struct Result*,
   struct String*
   );
int Help_Display(               
   );
int IniFile_Calculate(          
   struct TS*
   );
int IniFile_CheckIni(           
   );
int IniFile_Cind(               
   struct TS*
   );
int IniFile_LoadIni(            
   );
int IniFile_PhDistColours(      
   );
int IniFile_SockColours(        
   );
int IniFile_SSCols(             
   );
int IniFile_WCols(              
   );
int Log_Double(                 
   char* descr,
   double val
   );
int Log_ErrorMessage(           
   int errcode
   );
int Log_Goodbye(                
   );
int Log_Header(                 
   char* hdr
   );
int Log_Int(                    
   char* descr,
   int val
   );
int Log_Percent(                
   char* descr,
   double val1,
   double val2
   );
int Log_Profile(                
   struct TS*
   );
int Log_TS(                     
   struct TS*
   );
void Log_Welcome(               
   );
int MultiFile_Count(            
   );
int MultiSine_ClearTracks(
   );
int MultiSine_DDWA(             
   struct TS*,
   struct TS*,
   struct Result*,
   int oi,
   int end
   );
int MultiSine_Derive(           
   struct TS*,
   struct TS*,
   struct Result*,
   struct Result*,
   struct Result*,
   int oi,
   int end
   );
int MultiSine_DWA(              
   struct TS*,
   struct TS*,
   struct Result*,
   int oi,
   int end
   );
int MultiSine_Fit(              
   struct TS*,
   struct TS*,
   struct Result*,
   int ot,
   int oi,
   int end
   );
int MultiSine_Newton(           
   struct TS*,
   struct TS*,
   struct Result*,
   int oi,
   int end
   );
int MultiSine_WriteProfiles(    
   struct TS*,
   struct TS*,
   struct Result*,
   int ti,
   int end
   );
int MultiSine_WriteTracks(      
   struct TS*,
   struct Result*,
   int ti,
   int rows
   );
double Nyquist_Coef(            
   double*,
   int start,
   int end,
   double nf
   );
double Nyquist_Freq(            
   double*,
   int start,
   int end,
   double nc
   );
double Nyquist_Scan(            
   struct TS*,
   int start,
   int end
   );
int PhaseDiagram_Generate(      
   struct TS*,
   struct TS*,
   struct TS*,
   struct Result*,
   int ti,
   int rows
   );
int PhD_Generate(               
   struct TS*,
   struct TS*,
   int ti
   );
double PhD_SinglePeriod(        
   struct TS*,
   struct TS*,
   double period
   );
int PhDist_Cartesian(           
   struct TS* ts,
   struct Profile* prof,
   int ti
   );
int PhDist_Colours(             
   struct TS* ts,
   struct Profile* prof,
   int ti
   );
int PhDist_Cylindrical(         
   struct TS* ts,
   struct Profile* prof,
   int ti
   );
int PhDist_Generate(            
   struct TS*,
   struct Profile*,
   int ti
   );
int Preview_Generate(           
   struct TS*,
   struct Profile*,
   struct SigSpec*,
   struct TS*,
   struct SigSpec*,
   int ti
   );
int Profile_DFT(                
   struct TS*,
   struct Profile*,
   int ti
   );
int Profile_Generate(           
   struct TS*,
   struct Profile*,
   int ti
   );
int Profile_Lomb(               
   struct TS*,
   struct Profile*,
   int ti
   );
int Profile_SigSpec(            
   struct TS*,
   struct Profile*,
   int ti
   );
int Result_Copy(                
   struct Result*,
   struct Result*
   );
int Result_Display(             
   struct Result*
   );
int Result_Reset(               
   struct Result*
   );
int Result_TimeShift(           
   struct Result*,
   double shift
   );
int Result_Reset(               
   struct Result*
   );
int Result_Write (              
   struct TS*,
   struct Result*,
   int ti,
   int rows
   );
int Rnd_Init(                   
   int*
   );

int Rnd_Write(                  
   int*
   );
int SigSpec_Cascade(            
   struct TS*,
   struct Profile*,
   struct TS*,
   struct SigSpec*,
   int ti
   );
double SigSpec_Copy(            
   struct SigSpec* from,
   struct SigSpec* to
   );
double SigSpec_CSig(            
   double csig,
   double sig
   );
double SigSpec_DFT(             
   struct TS*,
   struct TS*,
   struct Result*,
   struct TS*,
   struct TS*,
   double norm,
   double scale
   );
double SigSpec_Lomb(            
   struct TS*,
   struct TS*,
   struct Result*,
   struct TS*,
   struct TS*,
   double norm,
   double scale
   );
double SigSpec_MaxSig(          
   struct TS*,
   struct TS*,
   struct Result*,
   double*,
   struct TS*,
   struct TS*,
   double norm,
   double scale
   );
double SigSpec_Sig(             
   struct TS*,
   struct TS*,
   struct Result*,
   struct TS*,
   struct TS*,
   double norm,
   double scale
   );
double SigSpec_SigSpec(         
   struct TS*,
   struct TS*,
   struct Profile*,
   struct SigSpec*,
   struct SigSpec*
   );
double SigSpec_SigSpecMax(      
   struct TS*,
   struct TS*,
   struct Profile*,
   struct SigSpec*,
   struct Result*,
   struct TS*,
   struct TS*,
   struct SigSpec*
   );
int Sim_Exp(                    
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar
   );
int Sim_Poly(                   
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar
   );
int Sim_RndSteps(               
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar,
   int* s
   );
int Sim_Run(                    
   struct TS* ts
   );
int Sim_SerCorr(                
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar,
   int* s
   );
int Sim_Signal(                 
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar
   );
int Sim_TempCorr(               
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar,
   int* s
   );
double Sim_ZeroMean(            
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   int* ssid,
   int nss,
   double* simpar
   );
int Sock_Cartesian(             
   struct TS* ts,
   struct Profile* prof,
   int ti
   );
int Sock_Colours(               
   struct TS* ts,
   struct Profile* prof,
   int ti
   );
int Sock_Cylindrical(           
   struct TS* ts,
   struct Profile* prof,
   int ti
   );
int Sock_Generate(              
   struct TS*,
   struct Profile*,
   int ti
   );
int Spec_Write(                 
   struct TS*,
   struct Profile*,
   struct SigSpec*,
   int ti
   );
int Subsets_Assign(             
   double** data,
   struct String** str,
   int cols,
   int rows,
   int* sscol,
   int nssc,
   int* tscolf,
   int* tscolw,
   int* ssid,
   int* nsse
   );
int Subsets_CountEntries(       
   struct TS*
   );
int TimeRes_Count(              
   struct TS*,
   int ti
   );
int TimeRes_Extract(            
   struct TS*,
   int ti,
   struct TS*
   );
int TimeRes_Extract2C(          
   struct TS*,
   int ti,
   struct TS*
   );
int TimeRes_Par(                
   struct TS* from,
   int ti,
   struct TS* to
   );
int TimeRes_Par2C(              
   struct TS* from,
   int ti,
   struct TS* to
   );
int TimeRes_Prepare(            
   struct TS*
   );
int TS_AssignProfiles(          
   struct TS*
   );
int TS_Check(                   
   struct TS*
   );
int TS_Count(                   
   struct TS*
   );
int TS_CountSubsetEntries(      
   struct TS*
   );
int TS_Par(                     
   struct TS*,
   struct TS*
   );
int TS_Read(                    
   struct TS*
   );
int TS_Statistics(              
   struct TS*,
   int colt,
   int colx
   );
int TS_Sort(                    
   struct TS*
   );
int TS_Subsets(                 
   struct TS*
   );
int TS_TimeShift(               
   struct TS*,
   int colt,
   double shift
   );
int TS_Weights(                 
   struct TS*
   );
int TS_Write(                   
   struct TS*,
   struct TS*,
   int ti
   );
int Win_Generate(               
   struct TS* ts,
   int ti
   );

struct String project;          
int quiet = 0;                  

int t = 0;                      
int x = 1;                      
int* ssraw;                     
int nssc = 0;                   
int* wraw;                      
double* wx;                     
int nwc = 0;                    

double lf = 0;                  
double uf = -1;                 
double ny = .5;                 
int nyscan = 0;                 
double fs;                      
double os = 20;                 
double sl = 5;                  
double csl = 0;                 
int it = -1;                    
double fres = -1;               
int nsp = 0;                    
int nres = 0;                   
int stsp = 1;                   
int stres = 1;                  
int nspr;                       

int makeprof = 0;               
int nprof = 0;                  

int makewin = 0;                

double psl = 0;                 
int lmax;                       

int sockph = -1;                
double sockfill = 0;            
int sockcoord = 0;              
int sockcolmodel = -1;          

int phdistph = -1;              
double phdistfill = 0;          
int phdistcoord = 0;            
int phdistcolmodel = -1;        

int np;                         
int mode = 0;                   
int zeromean = 1;               

double trstart = DBL_MAX;       
double trwidth = -1;            
double trstep = -1;             
int trmode = -1;                
double trpar = DBL_MIN;         
double trph = DBL_MIN;          
double trexp = DBL_MIN;         
int trnum = 1;                  

int simmode = -1;               

int mfl = -1;                   
int nmf = -1;                   
int mfs = 0;                    

int co = 0;                     
int nc = -1;                    
int stc = 1;                    

double aap = 1;                 
int aaa = 1;                    
int aad = 1;                    
double aasl = 5;                
int aan = 1;                    

int cinddef = INT_MIN;          
int ncomp = 0;                  

int dmode = 0;                  
int nmodel = 0;                 
double npar[3];                 
double nnorm = 0;               

int oharm = 0;                  
int h0 = 0;                     
double uff = 0;                 
int nff = 0;                    
int nfh = 0;                    

int msmode = 0;                 
double msacc = .000001;         
double mssens = 1;              
double msrms = .000001;         
int nmst = -1;                  
int stmst = 1;                  
int msp = 0;                    
int nmsp = -1;                  
int stmsp = 1;                  

int nresults = 0;               
int stresults = 1;              

int pdb = -1;                   
double pdw = -1;                
int npd = -1;                   
int stpd = 1;                   
int pdnp = -1;                  
double pdlp = -1;               
double pdup = -1;               

int nph = -1;                   
int stph = 1;                   
int pf = 0;                     

int debug = -1;                 
int main(int argc, char *argv[])
{

   struct TS* ts;               
   struct Profile* prof;        
   struct SigSpec sigspec;      
   struct SigSpec compspec;     
   struct TS* cdata;            
   struct TS data;              

   int ssi;                     
   int ci;                      
   int ri;                      
   int ti;                      
   int fi = -1;                 
   int si;                      
   int pi;                      
   int ni;                      

   int err;                     

   struct String fname;
   FILE* F;

   double dbuf;
   char cbuf[MAXCHAR];
   struct String sbuf;



   if ((argc == 3) && (strcmp(argv[2], "q") == 0)) quiet = 1;

   if (!quiet) Log_Welcome();

   if ((argc < 2) || (argc > 3)) return Log_ErrorMessage(1);
   project.c =
      (int*)calloc(String_SetLength(&project, strlen(argv[1])), sizeof(int));
   if ((err = CmdLine_Scan(argc, argv)) > 0) return Log_ErrorMessage(err);
   if (err == -1) return Help_Display();

   if ((nssc = IniFile_SSCols()) == -4) return Log_ErrorMessage(4);
   else if (nssc == -3) Log_ErrorMessage(3);
   else if (nssc > 0)
   {
      if (debug == 0) printf("calloc ssraw\n");
      ssraw = (int*)calloc(nssc, sizeof(int));
   }
   if ((nwc = IniFile_WCols()) == -6) return Log_ErrorMessage(6);
   else if (nwc == -5) Log_ErrorMessage(5);
   else if (nwc > 0)
   {
      if (debug == 0) printf("calloc wraw\n");
      wraw = (int*)calloc(nwc, sizeof(int));
      if (debug == 0) printf("calloc wx\n");
      wx = (double*)calloc(nwc, sizeof(int));
   }
   if ((err = IniFile_LoadIni()) > 0) return Log_ErrorMessage(err);
   else if (err < 0) Log_ErrorMessage(-err);
   else if ((err = IniFile_CheckIni()) > 0) return Log_ErrorMessage(err);
   if ((mfl >= 0) && ((nmf = MultiFile_Count()) < 0)) return
      Log_ErrorMessage(-nmf);

   if (mfl < 0)
   {
      nmf = 0;
      if (debug == 0) printf("calloc ts\n");
      ts = (struct TS*)calloc(1, sizeof(struct TS));
      ts[0].fi = -1;
      ts[0].ct = INT_MIN;
   }
   else
   {
      nmf -= mfs;
      if (debug == 0) printf("calloc ts\n");
      ts = (struct TS*)calloc(nmf, sizeof(struct TS));
   }
   if ((!quiet) && (nmf > 1)) Log_Int("number of files", nmf);
   for (fi = 0; fi < nmf; fi++) ts[fi].ct = INT_MIN;
   if ((err = IniFile_Cind(ts)) > 0) return Log_ErrorMessage(err);
   else if (err < 0) Log_ErrorMessage(-err);
   if (cinddef == INT_MIN) cinddef = 1;
   for (fi = 0; fi < nmf; fi++) if (ts[fi].ct == INT_MIN) ts[fi].ct = cinddef;
   if ((nmf == 0) && (ts[0].ct == INT_MIN)) ts[0].ct = cinddef;
   fi = 0;
   if (!quiet) Log_Header("loading time series input file(s)");

   do
   {
      ts[fi].it = 0;
      if (ts[fi].ct != 0)
      {
         if (ts[fi].ct == -1) ncomp++;
         if (mfl >= 0) ts[fi].fi = mfs + fi;
         else fi = 0;
         if ((err = TS_Count(&ts[fi])) != 0) return Log_ErrorMessage(err);
         if (debug == 0) printf("calloc ts[%i].cf\n", fi);
         ts[fi].cf = (int*)calloc(ts[fi].c, sizeof(int));
         if (debug == 0) printf("calloc ts[%i].cw\n", fi);
         ts[fi].cw = (int*)calloc(ts[fi].c, sizeof(int));
         if ((err = TS_Check(&ts[fi])) != 0) return Log_ErrorMessage(err);
         if (debug == 0) printf("calloc ts[%i].d\n", fi);
         ts[fi].d = (double**)calloc(ts[fi].n, sizeof(double*));
         if (debug == 0) printf("calloc ts[%i].s\n", fi);
         ts[fi].s = (struct String**)calloc(ts[fi].c - ts[fi].n,
            sizeof(struct String*));
         if (debug == 0) printf("calloc ts[%i].ss\n", fi);
         ts[fi].ss = (int*)calloc(ts[fi].r, sizeof(int));
         if (debug == 0) printf("calloc ts[%i].w\n", fi);
         ts[fi].w = (double*)calloc(ts[fi].r, sizeof(double));
         for (ri = 0; ri < ts[fi].r; ri++) ts[fi].w[ri] = 1;
         for (ci = 0; ci < ts[fi].n; ci++)
         {
            if (debug == 0) printf("calloc ts[%i].d[%i]\n", fi, ci);
            ts[fi].d[ci] = (double*)calloc(ts[fi].r, sizeof(double));
         }
         for (ci = 0; ci < ts[fi].c - ts[fi].n; ci++)
         {
            if (debug == 0) printf("calloc ts[%i].s[%i]\n", fi, ci);
            ts[fi].s[ci] =
               (struct String*)calloc(ts[fi].r, sizeof(struct String));
         }
         for (ci = 0; ci < ts[fi].c; ci++)
            if ((si = Dataset_GetIndex(ci, ts[fi].cf)) < 0)
         {
            si++;
            if (debug == 0) printf("calloc ts[%i].s[%i][%i].c\n", fi, -si,
               ri);
            for (ri = 0; ri < ts[fi].r; ri++) ts[fi].s[-si][ri].c =
               (int*)calloc(ts[fi].s[-si][ri].l = ts[fi].cw[ci],
               sizeof(int));
         }
         if ((err = TS_Read(&ts[fi])) != 0) return Log_ErrorMessage(err);
         if ((err = TS_Sort(&ts[fi])) != 0) return Log_ErrorMessage(err);
         for (ci = 0; ci < ts[fi].c - ts[fi].n; ci++)
            for (ri = 0; ri < ts[fi].r; ri++)
         {
            if (debug == 0) printf("calloc sbuf.c\n");
            sbuf.c = (int*)calloc(String_GetLength(&ts[fi].s[ci][ri]),
               sizeof(int));
            for(si = 0;
               (si < String_GetLength(&ts[fi].s[ci][ri])) &&
               (!isspace(String_GetChar(&ts[fi].s[ci][ri], si))); si++)
               String_SetChar(&sbuf, String_GetChar(&ts[fi].s[ci][ri], si),si);
            if (debug == 0) printf("free ts[%i].s[%i][%i].c\n", fi, ci, ri);
            free(ts[fi].s[ci][ri].c);
            String_SetLength(&sbuf, si);
            String_SetLength(&ts[fi].s[ci][ri], si);
            if (debug == 0) printf("calloc ts[%i].s[%i][%i].c\n", fi, -si,
               ri);
            ts[fi].s[ci][ri].c = (int*)calloc(si, sizeof(int));
            String_Copy(&sbuf, &ts[fi].s[ci][ri], 0);
            if (debug == 0) printf("free sbuf.c\n");
            free(sbuf.c);
         }
         if (((nwc > 0) || (trmode > 0)) && (err = TS_Weights(&ts[fi])) != 0)
            return Log_ErrorMessage(err);
         if ((err = TS_Subsets(&ts[fi])) != 0) return Log_ErrorMessage(err);
         if (debug == 0) printf("calloc ts[%i].nsse\n", fi);
         ts[fi].nsse = (int*)calloc(ts[fi].nss, sizeof(int));
         if ((err = TS_CountSubsetEntries(&ts[fi])) != 0)
            return Log_ErrorMessage(err);
         if ((simmode >= 0) && ((err = Sim_Run(&ts[fi])) != 0))
            return Log_ErrorMessage(err);
         for (ssi = 1; ssi <= ts[fi].nss; ssi++) dbuf =
            Stat_ZeroMean(ts[fi].d[x], ts[fi].w, 0, ts[fi].r - 1, ts[fi].ss,
            ssi, ts[fi].nss, ts[fi].d[x]);
      }
   } while (++fi < nmf);
   if (!quiet) Log_Header("time series properties");
   if (debug == 0) printf("calloc fname.c\n");
   fname.c = (int*)calloc(String_SetLength(&fname, MFDIGITS), sizeof(int));
   if ((!quiet) && (fi <= 1) && ((err = Log_TS(&ts[0])) != 0))
      return Log_ErrorMessage(err);
   else for (fi = 0; fi < nmf; fi++) if (ts[fi].ct != 0) 
   {
      NumFormat_FixDigits(mfs + fi, &fname);
      String_AsChars(&fname, cbuf);
      if (!quiet) printf("MultiFile %s ", cbuf);
      fflush(stdout);
      if ((err = Log_TS(&ts[fi])) != 0) return Log_ErrorMessage(err);
   }
   if (debug == 0) printf("free fname.c\n");
   free(fname.c);
   if (nwc > 0)
   {
      if (debug == 0) printf("free wraw\n");
      free(wraw);
      if (debug == 0) printf("free wx\n");
      free(wx);
   }
   if (nssc > 0)
   {
      if (debug == 0) printf("free ssraw\n");
      free(ssraw);
   }
   if (!quiet) Log_Header("preparing to run SigSpec");
   if (nmf < 1) nmf = 1;
   if ((err = TimeRes_Prepare(ts)) != 0) return Log_ErrorMessage(err);
   if ((err = IniFile_Calculate(ts)) != 0) return Log_ErrorMessage(err);
   if ((np = TS_AssignProfiles(ts)) < 0) return Log_ErrorMessage(-np);
   if ((nmf > 1) && ((makeprof > 0) || (makewin > 0))) Log_Profile(ts);
   if (debug == 0) printf("calloc prof\n");
   prof = (struct Profile*)calloc(np, sizeof(struct Profile));
   for (pi = 0; pi < np; pi++)
   {
      if (debug == 0) printf("calloc prof[%i].a0\n", pi);
      prof[pi].a0 = (double*)calloc(nspr, sizeof(double));
      if (debug == 0) printf("calloc prof[%i].b0\n", pi);
      prof[pi].b0 = (double*)calloc(nspr, sizeof(double));
      if (debug == 0) printf("calloc prof[%i].t0\n", pi);
      prof[pi].t0 = (double*)calloc(nspr, sizeof(double));
   }
   if (debug == 0) printf("calloc sigspec.a\n");
   sigspec.a = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("calloc sigspec.b\n");
   sigspec.b = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("calloc sigspec.sig\n");
   sigspec.sig = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("calloc compspec.a\n");
   compspec.a = (double*)calloc(nspr, sizeof(double));
   if (dmode > 0)
   {
      if (debug == 0) printf("calloc sigspec.ca\n");
      sigspec.ca = (double*)calloc(nspr, sizeof(double));
      if (debug == 0) printf("calloc sigspec.cb\n");
      sigspec.cb = (double*)calloc(nspr, sizeof(double));
   }
   if (dmode == 1)
   {
      if (debug == 0) printf("calloc compspec.b\n");
      compspec.b = (double*)calloc(nspr, sizeof(double));
   }
   ti = 0;
   if (!quiet) Log_Header("running SigSpec");
   do
   {
      if ((!quiet) && (trnum > 1))
         printf("time interval %i of %i\n", ti + 1, trnum);
      fflush(stdout);
      pi = 0;
      if (dmode > 0)
      {
         ni = 0;
         if (debug == 0) printf("calloc cdata\n");
         cdata = (struct TS*)calloc(ncomp, sizeof(struct TS));
         for (fi = 0; fi < nmf; fi++) if (ts[fi].ct == -1)
         {
            if ((err = TimeRes_Par2C(&ts[fi], ti, &cdata[ni])) != 0)
               Log_ErrorMessage(err);
            if (debug == 0) printf("calloc cdata[%i].d\n", ni);
            cdata[ni].d = (double**)calloc(2, sizeof(double*));
            if (debug == 0) printf("calloc cdata[%i].d[0]\n", ni);
            cdata[ni].d[0] = (double*)calloc(cdata[ni].r, sizeof(double));
            if (debug == 0) printf("calloc cdata[%i].d[1]\n", ni);
            cdata[ni].d[1] = (double*)calloc(cdata[ni].r, sizeof(double));
            if (debug == 0) printf("calloc cdata[%i].cf\n", ni);
            cdata[ni].cf = (int*)calloc(2, sizeof(int));
            if (debug == 0) printf("calloc cdata[%i].cf[0]\n", ni);
            cdata[ni].cf[0] = 1;
            if (debug == 0) printf("calloc cdata[%i].cf[1]\n", ni);
            cdata[ni].cf[1] = 1;
            if (debug == 0) printf("calloc cdata[%i].cw\n", ni);
            cdata[ni].cw = (int*)calloc(2, sizeof(int));
            if (debug == 0) printf("calloc cdata[%i].cw[0]\n", ni);
            cdata[ni].cw[0] = 16;
            if (debug == 0) printf("calloc cdata[%i].cw[1]\n", ni);
            cdata[ni].cw[1] = 16;
            if (debug == 0) printf("calloc cdata[%i].w\n", ni);
            cdata[ni].w = (double*)calloc(cdata[ni].r, sizeof(double));
            if (debug == 0) printf("calloc cdata[%i].ss\n", ni);
            cdata[ni].ss = (int*)calloc(cdata[ni].r, sizeof(int));
            if (debug == 0) printf("calloc cdata[%i].nsse\n", ni);
            cdata[ni].nsse = (int*)calloc(cdata[ni].nss, sizeof(int));
            if ((err = TimeRes_Extract2C(&ts[fi], ti, &cdata[ni])) != 0)
               return Log_ErrorMessage(err);
            for (ssi = 1; ssi <= cdata[ni].nss; ssi++)
               Stat_ZeroMean(cdata[ni].d[1], cdata[ni].w, 0, cdata[ni].r - 1,
               cdata[ni].ss, ssi, cdata[ni].nss, cdata[ni].d[1]);
            if ((err = TS_Statistics(&cdata[ni++], 0, 1)) != 0)
               return Log_ErrorMessage(err);
         }
         for (ni = 0; ni < ncomp; ni++) if (((pi == 0) || (cdata[ni].pi > pi)) &
            (makewin > 0) && ((err = Win_Generate(&cdata[ni], ti)) != 0))
            return Log_ErrorMessage(err);
      }
      pi = -1;
      for (fi = 0; fi < nmf; fi++) if (ts[fi].ct == 1)
      {
         if (nmf > 1)
         {
            if (debug == 0) printf("calloc fname.c\n");
            fname.c = (int*)calloc(String_SetLength(&fname, MFDIGITS),
               sizeof(int));
            NumFormat_FixDigits(mfs + fi, &fname);
            String_AsChars(&fname, cbuf);
            if (debug == 0) printf("free fname.c\n");
            free(fname.c);
            if (!quiet)
               printf("\rMultiFile %s                                \n", cbuf);
         }
         if ((err = TimeRes_Par(&ts[fi], ti, &data)) != 0)
            Log_ErrorMessage(err);
         else
         {
            if (debug == 0) printf("calloc data.d\n");
            data.d = (double**)calloc(data.n, sizeof(double*));
            if (debug == 0) printf("calloc data.s\n");
            data.s = (struct String**)calloc(data.c - data.n,
               sizeof(struct String*));
            if (debug == 0) printf("calloc data.cf\n");
            data.cf = (int*)calloc(data.c, sizeof(int));
            if (debug == 0) printf("calloc data.cw\n");
            data.cw = (int*)calloc(data.c, sizeof(int));
            if (debug == 0) printf("calloc data.w\n");
            data.w = (double*)calloc(data.r, sizeof(double));
            for (ri = 0; ri < data.r; ri++) data.w[ri] = 1;
            for (ci = 0; ci < data.n; ci++)
            {
               if (debug == 0) printf("calloc data.d[%i]\n", ci);
               data.d[ci] = (double*)calloc(data.r, sizeof(double));
            }
            for (ci = 0; ci < data.c - data.n; ci++)
            {
               if (debug == 0) printf("calloc data.s[%i]\n", ci);
               data.s[ci] =
                  (struct String*)calloc(data.r, sizeof(struct String));
            }
            for (ci = 0; ci < data.c; ci++)
            {
               data.cf[ci] = ts[fi].cf[ci];
               data.cw[ci] = ts[fi].cw[ci];
               if ((si = Dataset_GetIndex(ci, data.cf)) < 0)
               {
                  si++;
                  if (debug == 0) printf("calloc data.s[%i][%i].c\n", -si,
                     ri);
                  for (ri = 0; ri < data.r; ri++) data.s[-si][ri].c =
                     (int*)calloc(data.s[-si][ri].l = data.cw[ci], sizeof(int));
               }
            }
            if (debug == 0) printf("calloc data.ss\n");
            data.ss = (int*)calloc(data.r, sizeof(int));
            if (debug == 0) printf("calloc data.nsse\n");
            data.nsse = (int*)calloc(data.nss = ts[fi].nss, sizeof(int));
            if ((err = TimeRes_Extract(&ts[fi], ti, &data)) != 0)
               return Log_ErrorMessage(err);
            for (ssi = 1; ssi <= data.nss; ssi++)
            {
               data.nsse[ssi - 1] = ts[fi].nsse[ssi - 1];
               Stat_ZeroMean(data.d[x],
                  data.w, 0, data.r - 1, data.ss, ssi, data.nss, data.d[x]);
            }
            if ((err = TS_Statistics(&data, t, x)) != 0)
               return Log_ErrorMessage(err);
            if (trnum < 2) ti = -1;
            if (dmode == 0) Diff_SpWhite(compspec.a);
            if (data.pi > pi)
            {
               if (makewin > 0) if ((err = Win_Generate(&data, ti)) != 0)
                  return Log_ErrorMessage(err);
               if ((err = Profile_Generate(&data, &prof[data.pi], ti)) != 0)
                  return Log_ErrorMessage(err);
               if ((sockph >= 0) &&
                  ((err = Sock_Generate(&data, &prof[data.pi], ti)) != 0))
                  return Log_ErrorMessage(err);
               if ((phdistph >= 0) &&
                  ((err = PhDist_Generate(&data, &prof[data.pi], ti)) != 0))
                  return Log_ErrorMessage(err);
            }
            pi = data.pi;
            if ((psl > 0) &&
               ((err = Preview_Generate(&data, &prof[pi], &sigspec, cdata,
               &compspec, ti)) < 0)) return Log_ErrorMessage(-err);
            if ((err = SigSpec_Cascade(&data, &prof[pi], cdata, &compspec,
               ti)) != 0) return Log_ErrorMessage(err);
            for (ci = 0; ci < data.c - data.n; ci++)
            {
               for (ri = 0; ri < data.r; ri++)
               {
                  if (debug == 0) printf("free data.s[%i][%i].c\n", ci, ri);
                  free(data.s[ci][ri].c);
               }
               if (debug == 0) printf("free data.s[%i]\n", ci);
               free(data.s[ci]);
            }
            for (ci = 0; ci < data.n; ci++)
            {
               if (debug == 0) printf("free data.d[%i]\n", ci);
               free(data.d[ci]);
            }
            if (debug == 0) printf("free data.ss\n");
            free(data.ss);
            if (debug == 0) printf("free data.nsse\n");
            free(data.nsse);
            if (debug == 0) printf("free data.w\n");
            free(data.w);
            if (debug == 0) printf("free data.d\n");
            free(data.d);
            if (debug == 0) printf("free data.s\n");
            free(data.s);
            if (debug == 0) printf("free data.cf\n");
            free(data.cf);
            if (debug == 0) printf("free data.cw\n");
            free(data.cw);
         }
         if ((!quiet) && (nmf > 1))
         {
            printf("\r                                                       ");
            printf("         \n");
         }
      }
      if ((!quiet) && (trnum > 1))
      {
         printf("\r                                                          ");
         printf("         \n");
      }
      if (dmode > 0)
      {
         for (ni = 0; ni < ncomp; ni++)
         {
            if (debug == 0) printf("free cdata[%i].d[0]\n", ni);
            free(cdata[ni].d[0]);
            if (debug == 0) printf("free cdata[%i].d[1]\n", ni);
            free(cdata[ni].d[1]);
            if (debug == 0) printf("free cdata[%i].d\n", ni);
            free(cdata[ni].d);
            if (debug == 0) printf("free cdata[%i].cf\n", ni);
            free(cdata[ni].cf);
            if (debug == 0) printf("free cdata[%i].cw\n", ni);
            free(cdata[ni].cw);
            if (debug == 0) printf("free cdata[%i].w\n", ni);
            free(cdata[ni].w);
            if (debug == 0) printf("free cdata[%i].ss\n", ni);
            free(cdata[ni].ss);
            if (debug == 0) printf("free cdata[%i].nsse\n", ni);
            free(cdata[ni].nsse);
         }
         free(cdata);
      }
   } while ((trmode >= 0) && ((trnum < 2) || (++ti < trnum)));
   free(project.c);
   for (fi = 0; fi < nmf; fi++)
   {
      for (ci = 0; ci < ts[fi].n; ci++)
      {
        if (debug == 0) printf("free ts[%i].d[%i]\n", fi, ci);
        free(ts[fi].d[ci]);
      }
      for (ci = 0; ci < ts[fi].c - ts[fi].n; ci++)
      {
         for (ri = 0; ri < ts[fi].r; ri++)
         {
           if (debug == 0) printf("free ts[%i].s[%i][%i].c\n", fi, ci, ri);
            free(ts[fi].s[ci][ri].c);
         }
        if (debug == 0) printf("free ts[%i].s[%i]\n", fi, ci);
         free(ts[fi].s[ci]);
      }
      if (debug == 0) printf("free ts[%i].cf\n", fi);
      free(ts[fi].cf);
      if (debug == 0) printf("free ts[%i].cw\n", fi);
      free(ts[fi].cw);
      if (debug == 0) printf("free ts[%i].d\n", fi);
      free(ts[fi].d);
      if (debug == 0) printf("free ts[%i].s\n", fi);
      free(ts[fi].s);
      if (debug == 0) printf("free ts[%i].ss\n", fi);
      free(ts[fi].ss);
      if (debug == 0) printf("free ts[%i].w\n", fi);
      free(ts[fi].w);
      if (debug == 0) printf("free ts[%i].nsse\n", fi);
      free(ts[fi].nsse);
   }
   if (debug == 0) printf("free ts\n");
   free(ts);
   for (pi = 0; pi < np; pi++)
   {
      if (debug == 0) printf("free prof[%i].a0\n", pi);
      free(prof[pi].a0);
      if (debug == 0) printf("free prof[%i].b0\n", pi);
      free(prof[pi].b0);
      if (debug == 0) printf("free prof[%i].t0\n", pi);
      free(prof[pi].t0);
   }
   if (debug == 0) printf("free prof\n");
   free(prof);
   if (debug == 0) printf("free sigspec.a\n");
   free(sigspec.a);
   if (debug == 0) printf("free sigspec.b\n");
   free(sigspec.b);
   if (debug == 0) printf("free sigspec.sig\n");
   free(sigspec.sig);
   if (dmode > 0)
   {
      if (debug == 0) printf("free sigspec.ca\n");
      free(sigspec.ca);
      if (debug == 0) printf("free sigspec.cb\n");
      free(sigspec.cb);
   }
   if (debug == 0) printf("free compspec.a\n");
   free(compspec.a);
   if (dmode == 1)
   {
      if (debug == 0) printf("free compspec.b\n");
      free(compspec.b);
   }
   if (!quiet) Log_Goodbye();
   return 0;
}



int DataFile_ColWidth(
   FILE* F,
   int col
   )

{
   int i;
   int c;
   int len;
   int maxlen = 0;
   rewind(F);
   while ((c = fgetc(F)) != EOF)
   {
      i = 0;
      while ((c != EOF) && (c != '\n'))
      {
         len = 0;
         while ((c != EOF) && (c != '\n') && (isspace(c))) c = fgetc(F);
         while ((c != EOF) && (c != '\n') && (!isspace(c)))
         {
            c = fgetc(F);
            if (i == col) len++;
         }
         if (len > maxlen) maxlen = len;
         i++;
      }
   }
   rewind(F);
   return maxlen;
}



int DataFile_CountCol(
   FILE* F
   )

{
   int i;
   int c;
   int n = 0;
   int r = 0;
   rewind(F);
   while ((c = fgetc(F)) != EOF)
   {
      i = 0;
      r++;
      while ((c != EOF) && (c != '\n'))
      {
         while ((c != EOF) && (c != '\n') && (isspace(c))) c = fgetc(F);
         if (c == '\n') i--;
         while ((c != EOF) && (c != '\n') && (!isspace(c))) c = fgetc(F);
         i++;
      }
      if (n == 0) n = i;
      if (i != n) return -1;
   }
   rewind(F);
   return n;
}



int DataFile_CountRows(
   FILE* F
   )

{
   int c;
   int empty;
   int n = 0;
   rewind(F);
   while ((c = fgetc(F)) != EOF)
   {
      empty = 1;
      while ((c != EOF) && ((c = fgetc(F)) != '\n')) if (!isspace(c))
         empty = 0;
      if (empty == 0) n++;
   }
   rewind(F);
   return n;
}



int DataFile_CountStartString(
   FILE* F,
   struct String* str
   )

{
   int n = 0;
   int c = ' ';
   int i;
   rewind(F);
   while (c != EOF)
   {
      for (i = 0; ((c = fgetc(F)) != EOF) && (c != '\n') && (c != '#') && 
         (i < String_GetLength(str)) && (c == str->c[i]); i++);
      if (i == String_GetLength(str)) n++;
      while ((c != EOF) && (c != '\n')) c = fgetc(F);
   }
   rewind(F);
   return n;
}



int DataFile_Display(
   double** data,
   struct String** str,
   int startcol,
   int endcol,
   int startrow,
   int endrow,
   int* numcol,
   int* cwidth
   )

{   
   int colindex;
   int rowindex;
   int ncolindex;
   for (rowindex = startrow; rowindex <= endrow; rowindex++)
   {
      ncolindex = 0;
      for (colindex = startcol; colindex <= endcol; colindex++)
      {
         if (numcol[colindex - startcol] == 1)
         {
            printf("%24.16lf ", data[ncolindex][rowindex]);
            ncolindex++;
         }
         else String_Display(
               &(str[colindex - ncolindex - startcol][rowindex - startrow]),
               cwidth[colindex - startcol] + 1);
      }
      printf("\n");
   }
   return (endcol - startcol + 1) * (endrow - startrow + 1);
}



int DataFile_NumericCols(
   FILE* F,
   int* col,
   int startcol,
   int endcol,
   int startrow,
   int endrow
   )

{
   int c = ' ';
   int colindex;
   int rowindex;
   int ct = endcol - startcol + 1;
   rewind(F);
   for (colindex = startcol; colindex <= endcol; colindex++)
      col[colindex - startcol] = 1;
   for (rowindex = 0; rowindex < startrow; rowindex++) 
      while (c != '\n') c = fgetc(F);
   for (rowindex = startrow; rowindex <= endrow; rowindex++)
   {
      for (colindex = 0; colindex < startcol; colindex++)
      {
         while (isspace(c)) c = fgetc(F);
         while (!isspace(c)) c = fgetc(F);
      }
      for (colindex = startcol; colindex <= endcol; colindex++)
      {
         while ((c != EOF) && (c != '\n') && (isspace(c)))
            c = fgetc(F);
         if (c == '-') c = fgetc(F);
         while (isdigit(c)) c = fgetc(F);
         if (c == '.') c = fgetc(F);
         while (isdigit(c)) c = fgetc(F);
         if ((col[colindex - startcol] == 1) && (!isspace(c)) && (c != EOF) &&
            (c != '\n'))
         {
            col[colindex - startcol] = 0;
            ct--;
         }
         while ((c != EOF) && (c != '\n') && (!isspace(c))) c = fgetc(F);
      }
      while((c != EOF) && (c != '\n')) c = fgetc(F);
   }
   rewind(F);
   return ct;
}



int DataFile_Read(
   FILE* F,
   double** data,
   struct String** str,
   int startcol,
   int endcol, 
   int startrow,
   int endrow,
   int* numcol
   )

{   
   int c = ' ';
   int err;
   int colindex;
   int rowindex;
   int ncolindex;
   DataFile_NumericCols(F, numcol, startcol, endcol, startrow, endrow);
   for (rowindex = 0; rowindex < startrow; rowindex++) 
      while (c != '\n') c = fgetc(F);
   for (rowindex = startrow; rowindex <= endrow; rowindex++)
   {
      for (colindex = 0; colindex < startcol; colindex++)
      {
         while (isspace(c)) c = fgetc(F);
         while (!isspace(c)) c = fgetc(F);
      }
      ncolindex = 0;
      for (colindex = startcol; colindex <= endcol; colindex++)
      {
         if (numcol[colindex - startcol] == 1)
         {
            err = fscanf(F, "%lf", &data[ncolindex][rowindex]);
            ncolindex++;
         }
         else
         {
            String_Read(
               &(str[colindex - ncolindex - startcol][rowindex - startrow]), F);
         }
      }
      while (isgraph(c));
   }
   rewind(F);
   return (endcol - startcol + 1) * (endrow - startrow + 1);
}



int DataFile_StrLen(
   FILE* F,
   struct String** str,
   int startcol,
   int endcol, 
   int startrow,
   int endrow,
   int* numcol
   )

{   
   int c = ' ';
   int colindex;
   int rowindex;
   int ncolindex;
   DataFile_NumericCols(F, numcol, startcol, endcol, startrow, endrow);
   for (rowindex = 0; rowindex < startrow; rowindex++) 
      while (c != '\n') c = fgetc(F);
   for (rowindex = startrow; rowindex <= endrow; rowindex++)
   {
      for (colindex = 0; colindex < startcol; colindex++)
      {
         while (isspace(c)) c = fgetc(F);
         while (!isspace(c)) c = fgetc(F);
      }
      ncolindex = 0;
      for (colindex = startcol; colindex <= endcol; colindex++)
      {
         if (numcol[colindex - startcol] == 0)
         {
            while (isspace(c)) c = fgetc(F);
            for (str[colindex - ncolindex - startcol][rowindex - startrow].l =
               0; !isspace(c);
               str[colindex - ncolindex - startcol][rowindex - startrow].l++)
               c = fgetc(F);
         }
         else ncolindex++;
      }
      while (isgraph(c));
   }
   rewind(F);
   return (endcol - startcol + 1 - ncolindex) * (endrow - startrow + 1);
}



int DataFile_Write(
   FILE* F,
   double** data,
   struct String** str,
   int startcol,
   int endcol,
   int startrow,
   int endrow,
   int* numcol,
   int* cwidth
   )

{   
   int colindex;
   int rowindex;
   int ncolindex;
   for (rowindex = startrow; rowindex <= endrow; rowindex++)
   {
      ncolindex = 0;
      for (colindex = startcol; colindex <= endcol; colindex++)
      {
         if (numcol[colindex - startcol] == 1)
         {
            fprintf(F, "%24.16lf ", data[ncolindex][rowindex]);
            ncolindex++;
         }
         else String_Write(
            &(str[colindex - ncolindex - startcol][rowindex - startrow]), F,
            cwidth[colindex - startcol] + 1);
      }
      fprintf(F, "\n");
   }
   rewind(F);
   return (endcol - startcol + 1) * (endrow - startrow + 1);
}



int DataFile_WriteDouble(
   FILE* F,
   double** data,
   int startcol,
   int endcol,
   int startrow,
   int endrow
   )

{   
   int colindex;
   int rowindex;
   for (rowindex = startrow; rowindex <= endrow; rowindex++)
   {
      for (colindex = startcol; colindex <= endcol; colindex++)
         fprintf(F, "%24.16lf ", data[colindex][rowindex]);
      fprintf(F, "\n");
   }
   rewind(F);
   return (endcol - startcol + 1) * (endrow - startrow + 1);
}



int Dataset_GetIndex(
   int rawindex,
   int* tscolf
   )

{
   int i;
   int result = 0;
   if (tscolf[rawindex] == 0)
   {
      for (i = 0; i <= rawindex; i++) if (tscolf[i] == 0) result--;
   }
   else for (i = 0; i <= rawindex; i++) if (tscolf[i] > 0) result++;
   return result;
}



int Dataset_NConsistency(
   double** data,
   double** comp,
   int startcol,
   int endcol,
   int startrow,
   int endrow
   )

{
   int ci;
   int ri;
   Log_Header("time series consistency check");
   for (ci = startcol; ci <= endcol; ci++) for (ri = startrow; ri <= endrow; 
      ri++) if (data[ci][ri] != comp[ci][ri]) return -ri;
   return (endcol - startcol + 1) * (endrow - startrow + 1);
}



double Dataset_NSortAsc(
   double** data,
   struct String** str,
   int sortcol, 
   int dstart,
   int dend,
   int sstart,
   int send,
   int startrow,
   int endrow,
   int* width,
   char* message
   )

{
   struct String s;
   double d;
   int xchg = 1;
   double i = 0;
   int ri;
   int ci;
   double perc = 0;
   int* spot;
   if (message != NULL) Log_Percent(message, 0., 100.);
   if (debug == 0) printf("Dataset_NSortAsc: calloc spot\n");
   spot = (int*)calloc(endrow - startrow + 1, sizeof(int));
   for (ri = startrow; ri < endrow; ri++) spot[ri] = 0;
   while(xchg > 0)
   {
      xchg = 0;
      for (ri = startrow; ri < endrow; ri++)
      {
         if(spot[ri] * spot[ri + 1] == 0)
         {
            if (data[sortcol][ri] > data[sortcol][ri + 1])
            {
               i++;
               for (ci = dstart; ci <= dend; ci++)
               {
                  d = data[ci][ri + 1];
                  data[ci][ri + 1] = data[ci][ri];
                  data[ci][ri] = d;
               }
               if (str != NULL) for (ci = sstart; ci <= send; ci++)
               {
                  if (debug == 0) printf("Dataset_NSortAsc: calloc s.c\n");
                  s.c = (int*)calloc(s.l = width[ci], sizeof(int));
                  String_Copy(&(str[ci][ri + 1]), &s, 0);
                  String_Copy(&(str[ci][ri]), &(str[ci][ri + 1]), 0);
                  String_Copy(&s, &(str[ci][ri]), 0);
                  s.l = 0;
                  if (debug == 0) printf("Dataset_NSortAsc: free s.c\n");
                  free(s.c);
               }
               xchg++;
               if ((message != NULL) && ((double)(ri - startrow - xchg) /
                  (endrow - startrow) > perc)) Log_Percent(message, perc =
                  (double)(ri - startrow - xchg) / (endrow - startrow), 1.);
               spot[ri] = 0;
               spot[ri + 1] = 0;
            }
            else
            {
               spot[ri] = 1;
               spot[ri + 1] = 1;
            }
         }
      }
      if ((message != NULL) && ((double)(endrow - startrow - xchg) /
         (endrow - startrow) > perc)) Log_Percent(message, perc =
         (double)(endrow - startrow - xchg) / (endrow - startrow), 1.);
   }
   if (debug == 0) printf("Dataset_NSortAsc: free spot\n");
   free(spot);
   if (message != NULL)
      printf("\r                                                             ");
   return i;
}



double DFT_Amp(
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end, 
   double freq)

{
   return sqrt(DFT_Pow(tdata, data, weight, start, end, freq));
}



int DFT_MaxAmp(
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   double freq,
   double range,
   double* fout,
   double* rout,
   double* phout
   )

{
   int i;
   int j;
   double prev;
   double curr = DFT_Amp(tdata, data, weight, start, end, freq);
   double next;
   double f = freq;
   double r = .5 * range;
   int nextstep = 2;

   if (start < 0) return -1;
   if (end < 0) return -2;
   if (end <= start) return -3;
   for (i = 0; r > pow(10, 1 - DBL_DIG); i++)
   {
      switch (nextstep)
      {
      case -1:
         f -= r;
         next = curr;
         curr = prev;
         break;
      case 0:
         r *= .5;
         break;
      case 1:
         f += r;
         prev = curr;
         curr = next;
         break;
      default:
         break;
      }
      if (nextstep != 1)
      {
         if ((prev = DFT_Amp(tdata, data, weight, start, end, f - r)) ==
            DBL_MAX) return -4;
         if (prev == DBL_MIN) return -5;
      }
      if (nextstep != -1)
      {
         if ((next = DFT_Amp(tdata, data, weight, start, end, f + r)) ==
            DBL_MAX) return -6;
         if (next == DBL_MIN) return -7;
      }
      if (prev == 0)
      {
         *fout = f - r;
         *rout = DFT_Amp(tdata, data, weight, start, end, f - r);
         *phout = DFT_Ph(tdata, data, weight, start, end, f - r);
         return i;
      }
      if (next == 0)
      {
         *fout = f + r;
         *rout = DFT_Amp(tdata, data, weight, start, end, f + r);
         *phout = DFT_Ph(tdata, data, weight, start, end, f + r);
         return i;
      }
      if ((curr >= prev) && (curr >= next)) nextstep = 0;
      else if (prev > next) nextstep = -1;
      else nextstep = 1;
   }
   *fout = f;
   *rout = curr;
   *phout = DFT_Ph(tdata, data, weight, start, end, f);
   return i;
}



double DFT_Ph(
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end, 
   double freq
   )

{
   int i;
   double cossum = 0;
   double sinsum = 0;
   if ((start < 0) || (end < 0)) return DBL_MIN;
   if (end < start) return DBL_MAX;
   for (i = start; i <= end; i++)
   {
      cossum += weight[i] * data[i] * cos(2 * PI * freq * tdata[i]);
      sinsum += weight[i] * data[i] * sin(2 * PI * freq * tdata[i]);
   }
   return atan2(sinsum, cossum);
}



double DFT_Pow(
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   double freq
   )

{
   int i;
   double cossum = 0;
   double sinsum = 0;
   double wsum = 0;
   if ((start < 0) || (end < 0)) return DBL_MIN;
   if (end < start) return DBL_MAX;
   for (i = start; i <= end; i++)
   {
      cossum += weight[i] * data[i] * cos(2 * PI * freq * tdata[i]);
      sinsum += weight[i] * data[i] * sin(2 * PI * freq * tdata[i]);
      wsum += weight[i];
   }
   return 4. * (cossum*cossum + sinsum*sinsum) / pow(wsum, 2);
}



int FFT_AmpSp(
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   double lf,
   double uf,
   int freqs,
   double* fa,
   double* sp,
   double* ph
   )

{
   int t;
   int f;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double df;
   double wsum;
   if (start < 0) return -1;
   if (end < 0) return -2;
   if (end < start) return -3;
   if (uf < lf) return -4;
   if (freqs <= 0) return -5;
   df = (uf - lf) / (freqs - 1.);
   wsum = 0;
   for (f = 0; f < freqs; f++)
   {
      fa[f] = lf + f * df;
      sp[f] = 0;
      ph[f] = 0;
   }
   for (t = start; t <= end; t++) wsum += weight[t];
   for (t = start; t <= end; t++)
   {
      curcos = data[t] * weight[t] / wsum * cos(2. * PI * lf * tdata[t]);
      cursin = data[t] * weight[t] / wsum * sin(2. * PI * lf * tdata[t]);
      dcos = cos(2. * PI * df * tdata[t]);
      dsin = sin(2. * PI * df * tdata[t]);
      for (f = 0; f < freqs; f++)
      {
         sp[f] += curcos;
         ph[f] += cursin;
         dbuf = curcos * dcos - cursin * dsin;
         cursin = cursin * dcos + curcos * dsin;
         curcos = dbuf;
      }
   }
   for (f = 0; f < freqs; f++)
   {
      dbuf = atan2(ph[f], sp[f]);
      sp[f] = 2. * sqrt(pow(sp[f], 2) + pow(ph[f], 2));
      ph[f] = dbuf;
   }
   return freqs;
}



int FFT_AmpSpW(
   double* tdata,
   double* weight,
   int start,
   int end,
   double lf,
   double uf,
   int freqs,
   double* fa,
   double* sp,
   double* ph
   )

{
   int t;
   int f;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double df;
   if (start < 0) return -1;
   if (end < 0) return -2;
   if (end < start) return -3;
   if (uf < lf) return -4;
   if (freqs <= 0) return -5;
   df = (uf - lf) / (freqs - 1.);
   for (f = 0; f < freqs; f++)
   {
      fa[f] = lf + f * df;
      sp[f] = 0;
      ph[f] = 0;
   }
   for (t = start; t <= end; t++)
   {
      curcos = weight[t] * cos(2. * PI * lf * tdata[t]);
      cursin = weight[t] * sin(2. * PI * lf * tdata[t]);
      dcos = cos(2. * PI * df * tdata[t]);
      dsin = sin(2. * PI * df * tdata[t]);
      for (f = 0; f < freqs; f++)
      {
         sp[f] += curcos;
         ph[f] += cursin;
         dbuf = curcos * dcos - cursin * dsin;
         cursin = cursin * dcos + curcos * dsin;
         curcos = dbuf;
      }
   }
   for (f = 0; f < freqs; f++)
   {
      dbuf = atan2(ph[f], sp[f]);
      sp[f] = sqrt(pow(sp[f], 2) + pow(ph[f], 2));
      ph[f] = dbuf;
   }
   return freqs;
}



int NumFormat_FixDigits(
   int val,
   struct String* str
   )

{
   int i;
   char buf[MAXCHAR];
   sprintf(buf, "%i", val);
   for (i = 0; i < String_GetLength(str) - strlen(buf); i++)
      String_SetChar(str, (int)'0', i);
   for (; i < String_GetLength(str); i++)
      String_SetChar(str, (int)buf[i - String_GetLength(str) + strlen(buf)], i);
   return String_GetLength(str);
}



double Stat_AutoCorr(
   double* data,
   double* weight,
   int start,
   int end,
   int lag,
   double llim,
   double ulim,
   int* valid
   )

{
   int i;
   double sum = 0;
   double norm = 0;
   double origin1 =
      Stat_Mean(data, weight, start, end - lag, llim, ulim, valid);
   double origin2 =
      Stat_Mean(data, weight, start + lag, end, llim, ulim, valid);
   if ((start < 0) || (end < 0)) return DBL_MIN;
   if (end < start) return DBL_MAX;
   if (llim >= ulim)
   {
      llim = -DBL_MAX;
      ulim = DBL_MAX;
   }
   *valid = 0;
   for (i = start; i <= end - lag; i++)
      if ((data[i] - origin1 >= llim) && 
      (data[i] - origin1 <= ulim) &&
      (data[i + lag] - origin2 >= llim) && 
      (data[i + lag] - origin2 <= ulim))
   {
      (*valid)++;
      norm += weight[i] * weight[i + lag];
      sum += weight[i] * weight[i + lag] * (data[i] - origin1) *
         (data[i + lag] - origin2);
   }
   if (sum == 0) return 1;
   if (norm <= 0) return DBL_MAX;
   return sum / norm /
      Stat_SDev(data, weight, start, end - lag, llim, ulim, valid) /
      Stat_SDev(data, weight, start + lag, end, llim, ulim, valid);
}



int Stat_Correlogram(
   double* data,
   double* weight,
   int lorder,
   int uorder,
   int start,
   int end,
   FILE* F,
   int* valid
   )

{
   int i;
   int j;
   int l = lorder;
   int u = uorder;
   double sum;
   double norm;
   double var = Stat_Variance(data, weight, start, end, 0, 0, valid);
   double* zm1;
   double* zm2;
   if ((start < 0) || (end < 0)) return -1;
   if (end < start) return -2;
   if (debug == 0) printf("Stat_Correlogram: calloc zm1\n");
   zm1 = (double*)calloc(end - start + 1, sizeof(double));
   if (debug == 0) printf("Stat_Correlogram: calloc zm2\n");
   zm2 = (double*)calloc(end - start + 1, sizeof(double));
   if (u <= l)
   {
      l = 0;
      u = (int)ceil(.5 * (end - start));
   }
   if (weight == NULL)
   {
      for (i = l; i <= u; i++)
      {
         *valid = 0;
         sum = 0;
         Stat_ZeroMean(data, weight, start, end - i, NULL, 0, 0, zm1);
         Stat_ZeroMean(data, weight, start + i, end, NULL, 0, 0, zm2);
         for (j = start; j <= end - i; j++)
         {
            (*valid)++;
            sum += zm1[j] * zm2[j + i];
         }
         if (F != NULL) fprintf(F, "%12i %24.16lf\n", i, sum /
            (double)(*valid) / var);
      }
      free(zm1);
      free(zm2);
      return u - l + 1;
   }
   for (i = l; i <= u; i++)
   {
      *valid = 0;
      norm = 0;
      sum = 0;
      Stat_ZeroMean(data, weight, start, end - i, NULL, 0, 0, zm1);
      Stat_ZeroMean(data, weight, start + i, end, NULL, 0, 0, zm2);
      for (j = start; j <= end - i; j++)
      {
         (*valid)++;
         norm += weight[i] * weight[i + j];
         sum += weight[j] * data[j] * weight[j + i] * data[j + i];
      }
      if (F != NULL) fprintf(F, "%12i %24.16lf\n", i, sum / norm / var);
   }
   if (debug == 0) printf("Stat_Correlogram: free zm1\n");
   free(zm1);
   if (debug == 0) printf("Stat_Correlogram: free zm2\n");
   free(zm2);
   return u - l + 1;
}



double Stat_Mean(
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   )

{
   int i;
   double sum = 0;
   double norm = 0;
   *valid = 0;
   if ((start < 0) || (end < 0)) return DBL_MIN;
   if (end < start) return DBL_MAX;
   if (llim >= ulim)
   {
      llim = -DBL_MAX;
      ulim = DBL_MAX;
   }
   if (weight == NULL)
   {
      for (i = start; i <= end; i++) if ((data[i] >= llim) && (data[i] <= ulim))
      {
         (*valid)++;
         sum += data[i];
      }
      if (sum == 0) return 0;
      if (*valid <= 0) return DBL_MAX;
      return sum / *valid;
   }
   for (i = start; i <= end; i++) if ((data[i] >= llim) && (data[i] <= ulim))
   {
      (*valid)++;
      norm += weight[i];
      sum += weight[i] * data[i];
   }
   if (sum == 0) return 0;
   if (norm <= 0) return DBL_MAX;
   return sum / norm;
}



double Stat_PPScatter(
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   )

{
   int i;
   double sumsq = 0;
   double norm = 0;
   *valid = 0;
   if ((start < 0) || (end < 0)) return -1;
   if (end < start) return -2;
   if (llim >= ulim)
   {
      llim = -DBL_MAX;
      ulim = DBL_MAX;
   }
   if (weight == NULL)
   {
      for (i = start; i < end; i++) if ((fabs(data[i+1] - data[i]) >= llim) && 
         (fabs(data[i+1] - data[i]) <= ulim))
      {
         (*valid)++;
         sumsq += .5 * pow(data[i+1] - data[i], 2);
      }
      if ((fabs(data[0] - data[end]) >= llim) && 
         (fabs(data[0] - data[end]) <= ulim))
      {
         (*valid)++;
         sumsq += .5 * pow(data[0] - data[end], 2);
      }
      if (sumsq == 0) return 0;
      if (*valid <= 0) return DBL_MAX;
      return sqrt(sumsq / *valid);
   }
   for (i = start; i < end; i++) if ((fabs(data[i+1] - data[i]) >= llim) && 
      (fabs(data[i+1] - data[i]) <= ulim))
   {
      (*valid)++;
      norm += weight[i];
      sumsq += .25 * (weight[i] + weight[i+1]) * 
         pow(data[i+1] - data[i], 2);
   }
   if ((fabs(data[0] - data[end]) >= llim) && 
      (fabs(data[0] - data[end]) <= ulim))
   {
      norm += weight[end];
      sumsq += .25 * (weight[start] + weight[end]) *
         pow(data[start] - data[end], 2);
   }
   if (sumsq == 0) return 0;
   if (norm <= 0) return DBL_MAX;
   return sqrt(sumsq / norm);
}



double Stat_SDev(
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   )

{
   double result = Stat_Variance(data, weight, start, end, llim, ulim, valid);
   if (result < 0) return result;
   if (result == DBL_MAX) return DBL_MAX;
   return sqrt(result);
}



double Stat_Variance(
   double* data,
   double* weight,
   int start,
   int end,
   double llim,
   double ulim,
   int* valid
   )

{
   int i;
   double sumsq = 0;
   double norm = 0;
   double mean = Stat_Mean(data, weight, start, end, 0, 0, valid);
   *valid = 0;
   if ((start < 0) || (end < 0)) return -1;
   if (end < start) return -2;
   if (llim >= ulim)
   {
      llim = -DBL_MAX;
      ulim = DBL_MAX;
   }
   if (weight == NULL)
   {
      for (i = start; i <= end; i++)
         if ((fabs(data[i] - mean) >= llim) && (fabs(data[i] - mean) <= ulim))
      {
         (*valid)++;
         sumsq += pow(data[i] - mean, 2);
      }
      if (sumsq == 0) return 0;
      if (*valid <= 0) return DBL_MAX;
      return sumsq / *valid;
   }
   for (i = start; i < end; i++) if ((fabs(data[i+1] - data[i]) >= llim) && 
      (fabs(data[i+1] - data[i]) <= ulim))
   {
      (*valid)++;
      norm += weight[i];
      sumsq += weight[i] * pow(data[i] - mean, 2);
   }
   if ((fabs(data[0] - data[end]) >= llim) && 
      (fabs(data[0] - data[end]) <= ulim))
   {
      norm += weight[end];
      sumsq += pow(data[i] - mean, 2);
   }
   if (sumsq == 0) return 0;
   if (norm <= 0) return DBL_MAX;
   return sumsq / norm;
}



double Stat_ZeroMean(
   double* data,
   double* weight,
   int start,
   int end,
   int* ssid,
   int ss,
   int nss,
   double* zm
   )

{
   int i;
   double sum = 0;
   double norm = 0;
   int valid = 0;
   FILE* log;
   if (debug > 9) log = fopen("Stat_ZeroMean.log", "a");
   if (debug > 9)
   {
      fprintf(log, "**** Stat_ZeroMean ************************************\n");
      fflush(log);
   }
   if ((start < 0) || (end < 0)) return DBL_MIN;
   if (end < start) return DBL_MAX;
   if (weight == NULL)
   {
      for (i = start; i <= end; i++) if ((nss <= 1) || (ssid[i] == ss))
      {
         sum += data[i];
         valid++;
      }
      sum /= (double)valid;
      if (debug > 9)
      {
         fprintf(log, "mean %lg\n", sum);
         fflush(log);
      }
      for (i = start; i <= end; i++) if ((nss <= 1) || (ssid[i] == ss))
         zm[i] = data[i] - sum;
      if (debug > 9)
      {
         fprintf(log, "**** Stat_ZeroMean END ********************************\n");
         fflush(log);
         fclose(log);
      }
      return sum;
   }
   for (i = start; i <= end; i++) if ((nss <= 1) || (ssid[i] == ss))
   {
      valid++;
      sum += data[i] * weight[i];
      norm += weight[i];
   }
   sum /= norm;
   if (debug > 9)
   {
      fprintf(log, "mean %lg\n", sum);
      fflush(log);
   }
   for (i = start; i <= end; i++) if ((nss <= 1) || (ssid[i] == ss))
      zm[i] = data[i] - sum;
   if (debug > 9)
   {
      fprintf(log, "**** Stat_ZeroMean END ********************************\n");
      fflush(log);
      fclose(log);
   }
   return sum;
}



char* String_AsChars(
   struct String* str,
   char* c
   )

{
   int i;
   char cbuf[2];
   sprintf(c, "%c", String_GetChar(str, 0));
   for (i = 1; i < String_GetLength(str); i++)
   {
      sprintf(cbuf, "%c", String_GetChar(str, i));
      strcat(c, cbuf);
   }
   return c;
}



struct String* String_Chars2String(
   char* c,
   struct String* to
   )

{
   int i;
   for (i = 0; i < strlen(c); i++) to->c[i] = (int)c[i];
   return to;
}



int String_Comp(
   struct String* str1,
   struct String* str2
   )

{
   int i;
   if (String_GetLength(str1) != String_GetLength(str2))
      return abs(String_GetLength(str1) - String_GetLength(str2));
   for (i = 0; i < String_GetLength(str2); i++)
      if ((int)str1->c[i] != (int)str2->c[i]) return i + 1;
   return 0;
}



int String_Comp2Char(
   struct String* str,
   char* c
   )

{
   int i;
   if (String_GetLength(str) != strlen(c))
      return (String_GetLength(str) - strlen(c));
   for (i = 0; i < strlen(c); i++) if ((int)str->c[i] != (int)c[i])
      return i + 1;
   return 0;
}



int String_Copy(
   struct String* from,
   struct String* to,
   int at
   )

{
   int i;
   if (String_GetLength(to) < String_GetLength(from) + at)
      for (i = at; i < String_GetLength(to); i++)
      String_SetChar(to, String_GetChar(from, i - at), i);
   else for (i = at; i < String_GetLength(from) + at; i++)
      String_SetChar(to, String_GetChar(from, i - at), i);
   return i;
}



int String_Display(
   struct String* str,
   int len
   )

{
   int i;
   if (len < String_GetLength(str)) len = String_GetLength(str);
   for (i = 0; i < String_GetLength(str); i++)
      printf("%c", String_GetChar(str, i));
   for (; i < len; i++) printf(" ");
   return len;
}



int String_IsEmpty(
   struct String* str
   )

{
   if ((str->c) == NULL) return 1;
   return 0;
}



int String_GetChar(
   struct String* str,
   int pos
   )

{
   return str->c[pos];
}



int String_GetLength(
   struct String* str
   )

{
   return str->l;
}



int String_Read(
   struct String* str,
   FILE* F
   )

{
   int c = ' ';
   int len;
   int i;
   while (isspace(c))
   {
      c = fgetc(F);
   }
   for (len = 0; (len < str->l) && (!isspace(c)); len++)
   {
      String_SetChar(str, c, len);
      c = fgetc(F);
   }
   for (; !isspace(c); len++) c = fgetc(F);
   for (i = len; i < str->l; i++) String_SetChar(str, '\0', i);
   return len;
}



int String_Reset(
   struct String* str
   )

{
   return (str->l = 0);
}



int String_SetChar(
   struct String* str,
   char c,
   int pos
   )

{
   str->c[pos] = c;
   return pos + 1;
}



int String_SetChars(
   struct String* str,
   char* c,
   int pos
   )

{
   int i;
   if (pos + strlen(c) > String_GetLength(str) + 1)
      return String_GetLength(str) - pos - strlen(c) + 1;
   for (i = 0; i < strlen(c); i++) String_SetChar(str, (int)c[i], pos + i);
   return pos + strlen(c);
}



int String_SetLength(
   struct String* str,
   int len
   )

{
   str->l = len;
   return String_GetLength(str);
}



int String_Write(
   struct String* str,
   FILE* F,
   int length
   )

{
   int i;
   if (length < str->l) length = str->l;
   for (i = 0; i < str->l; i++) fprintf(F, "%c", str->c[i]);
   for (; i < length; i++) fprintf(F, " ");
   return length;
}



int Vector_Flush(
   double* data,
   int start,
   int end
  )

{
   int i;
   if ((start < 0) || (end < 0)) return -1;
   if (end < start) return -2;
   for (i = start; i <= end; i++) data[i] = 0;
   return end - start + 1;
}



double Vector_SProd(
   double* v1,
   int start1,
   int end1,
   double* v2,
   int start2,
   int end2
  )

{
   int i;
   int j = start2;
   double result = 0;
   if ((start1 < 0) || (end1 < 0)) return DBL_MIN;
   if (end1 < start1) return DBL_MIN;
   if ((start2 < 0) || (end2 < 0)) return DBL_MAX;
   if (end2 < start2) return DBL_MAX;
   if (end2 - start2 != end1- start1) return 0;
   for (i = start1; i <= end1; i++) result += v1[i] * v2[j++];
   return result;
}



int AntiAlC_Candidates(
   struct TS* orig,
   struct TS* ts,
   struct Profile* prof,
   struct SigSpec* sigspec,
   struct Result** cand,
   int* candi,
   struct TS* ocomp,
   struct TS* cres,
   struct SigSpec* compspec,
   int level
   )

{
   int fi;
   int hi;
   int li;
   int err;
   double r;
   int maxindex = 0;
   double f;
   double a;
   double b;
   double a0;
   double b0;
   double t0;
   struct TS testres;
   struct Result peak;
   if ((testres.d = (double**)calloc(1, sizeof(double*))) == NULL) return -185;
   if ((testres.d[0] = (double*)calloc(ts->r, sizeof(double))) == NULL)
      return -186;
   if ((peak.h = (struct Harmonic*)calloc(oharm + 1,
      sizeof(struct Harmonic))) == NULL) return -187;
   fi = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, fi, orig->nss,
      ts->d[0]);
   while (++fi <= orig->nss);
   peak.sig = SigSpec_SigSpecMax(orig, ts, prof, sigspec, &peak, ocomp, cres,
      compspec);
   peak.rms = Stat_SDev(ts->d[0], orig->w, 0, ts->r - 1, 0, 0, &err);
   peak.ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, ts->r - 1, 0, 0, &err);
   if (aap >= 1)
   {
      Result_Copy(&peak, &cand[level][0]);
      free(testres.d[0]);
      free(testres.d);
      free(peak.h);
      return 1;
   }
   for (fi = 1; fi < nff - 1; fi++)
      if ((sigspec->sig[fi] >= sigspec->sig[fi - 1]) &&
      (sigspec->sig[fi] >= sigspec->sig[fi + 1]) &&
      (((aan >= 0) && (maxindex <= aan)) ||
      ((aap >= 0) && (sigspec->sig[fi] >= peak.sig * aap)) ||
      ((aasl > 0) && (sigspec->sig[fi] >= aasl))))
   {
      err = 0;
      for (li = 0; (li < level) && (err == 0); li++)
         if (((sigspec->sig[fi] > aasl) && (cand[li][candi[li]].sig < aasl)) ||
         ((sigspec->sig[fi] > sl) && (cand[li][candi[li]].sig < sl))) err = 1;
      if (err == 0)
      {
         cand[level][maxindex].f = lf + fi * fs;
         r = fs;
         SigSpec_MaxSig(orig, ts, &(cand[level][maxindex]), &r, ocomp, cres,
            sigspec->norm, sigspec->scale);
         cand[level][maxindex].rms = peak.rms;
         cand[level][maxindex].ppsc = peak.ppsc;
         maxindex++;
      }
   }
   free (testres.d[0]);
   free(testres.d);
   free(peak.h);
   return AntiAlC_Sort(cand[level], maxindex);
}



int AntiAlC_Cascade(
   struct TS* orig,
   struct TS* ts,
   struct Profile* prof,
   struct SigSpec* sigspec,
   struct Result* result,
   struct TS* ocomp,
   struct TS** cdata,
   struct SigSpec* compspec,
   int start
   )

{
   int li;
   int ri;
   int ai;
   int fi;
   int hi;
   struct TS* res;
   struct TS** cres;
   struct SigSpec* sp;
   struct Result** cand;
   int* candi;
   int* candn;
   struct Result* best;
   int err;
   double optrms = DBL_MAX;
   double optppsc = DBL_MAX;
   double sigmax;
   int fin = 0;
   int tot = 1;
   int perc = 0;
   int end = start + aaa + 1;
   int hl = start + aad + 1;
   struct Result* curr;
   struct Result* cresult;
   char dstr[65536];
   if ((res = (struct TS*)calloc(aad + 1, sizeof(struct TS))) == NULL)
      return 157;
   if ((cres = (struct TS**)calloc(aad + 1, sizeof(struct TS*))) == NULL)
      return 158;
   if ((sp = (struct SigSpec*)calloc(aad, sizeof(struct SigSpec))) == NULL)
      return 159;
   if ((cand = (struct Result**)calloc(aad, sizeof(struct Result*))) == NULL)
      return 160;
   if ((curr = (struct Result*)calloc(hl, sizeof(struct Result))) == NULL)
      return 161;
   if ((candi = (int*)calloc(aad, sizeof(int))) == NULL)
      return 162;
   if ((candn = (int*)calloc(aad, sizeof(int))) == NULL)
      return 163;
   if ((best = (struct Result*)calloc(end, sizeof(struct Result))) == NULL)
      return 164;
   if ((cresult = (struct Result*)calloc(hl, sizeof(struct Result))) == NULL)
      return 148;
   if (!quiet)
   {
      printf("\rAntiAlC                                                      ");
      fflush(stdout);
   }
   for (li = 0; li < aad; li++)
   {
      if ((sp[li].a = (double*)calloc(nspr, sizeof(double))) == NULL)
         return 165;
      if ((sp[li].b = (double*)calloc(nspr, sizeof(double))) == NULL)
         return 166;
      if ((sp[li].sig = (double*)calloc(nspr, sizeof(double))) == NULL)
         return 167;
      if (dmode > 0)
      {
         if ((sp[li].ca = (double*)calloc(nspr, sizeof(double))) == NULL)
            return 168;
         if ((sp[li].cb = (double*)calloc(nspr, sizeof(double))) == NULL)
            return 169;
      }
      if ((cand[li] = (struct Result*)calloc((int)floor(.5 * nff),
         sizeof(struct Result))) == NULL) return 170;
      for (ai = 0; ai < (int)floor(.5 * nff); ai++)
         if ((cand[li][ai].h = (struct Harmonic*)calloc(oharm + 1,
         sizeof(struct Harmonic))) == NULL) return 171;
      candn[li] = 0;
      candi[li] = 0;
   }
   for (li = 0; li <= aad; li++)
   {
      res[li].fi = ts[0].fi;
      res[li].it = start + li;
      if ((res[li].d = (double**)calloc(1, sizeof(double*))) == NULL)
         return 172;
      if ((res[li].d[0] = (double*)calloc(ts[0].r, sizeof(double))) == NULL)
         return 173;
      res[li].c = 1;
      res[li].n = 1;
      res[li].r = ts[0].r;
      if ((res[li].cf = (int*)calloc(1, sizeof(int))) == NULL) return 174;
      res[li].cf[0] = 1;
      if ((res[li].cw = (int*)calloc(1, sizeof(int))) == NULL) return 175;
      res[li].cw[0] = 24;
      res[li].nss = ts[0].nss;
      if ((res[li].nsse = (int*)calloc(res[li].nss, sizeof(int))) == NULL)
         return 176;
      for (ri = 0; ri < res[li].nss; ri++) res[li].nsse[ri] = ts[0].nsse[ri];
      res[li].ct = ts[0].ct;
      res[li].pi = ts[0].pi;
   }
   for (ri = 0; ri < ts[0].r; ri++) res[0].d[0][ri] = ts[0].d[0][ri];
   res[0].rms = ts[0].rms;
   res[0].ppsc = ts[0].ppsc;
   for (ai = 0; ai <= aad; ai++)
   {
      if ((cres[ai] = (struct TS*)calloc(ncomp, sizeof(struct TS))) == NULL)
         return 177;
      for (fi = 0; fi < ncomp; fi++)
      {
         cres[ai][fi].fi = cdata[0][fi].fi;
         cres[ai][fi].it = start + ai;
         if ((cres[ai][fi].d = (double**)calloc(1, sizeof(double*))) == NULL)
            return 178;
         if ((cres[ai][fi].d[0] = (double*)calloc(cdata[0][fi].r,
            sizeof(double))) == NULL) return 179;
         cres[ai][fi].c = 1;
         cres[ai][fi].n = 1;
         cres[ai][fi].r = cdata[0][fi].r;
         if ((cres[ai][fi].cf = (int*)calloc(1, sizeof(int))) == NULL)
            return 180;
         cres[ai][fi].cf[0] = 1;
         if ((cres[ai][fi].cw = (int*)calloc(1, sizeof(int))) == NULL)
            return 181;
         cres[ai][fi].cw[0] = 24;
         cres[ai][fi].nss = cdata[0][fi].nss;
         cres[ai][fi].ct = cdata[0][fi].ct;
         cres[ai][fi].pi = cdata[0][fi].pi;
      }
   }
   for (fi = 0; fi < ncomp; fi++)
   {
      for (ri = 0; ri < cdata[0][fi].r; ri++)
         cres[0][fi].d[0][ri] = cdata[0][fi].d[0][ri];
      cres[0][fi].rms = cdata[0][fi].rms;
      cres[0][fi].ppsc = cdata[0][fi].ppsc;
   }
   for (li = 0; li < end; li++)
      if ((best[li].h = (struct Harmonic*)calloc(oharm + 1,
      sizeof(struct Harmonic))) == NULL) return 182;
   for (li = 0; li < hl; li++)
   {
      if ((curr[li].h = (struct Harmonic*)calloc(oharm + 1,
         sizeof(struct Harmonic))) == NULL) return 183;
      if ((cresult[li].h = (struct Harmonic*)calloc(oharm + 1,
         sizeof(struct Harmonic))) == NULL) return 184;
   }
   for (li = 0; li < start; li++) Result_Copy(&result[li], &best[li]);
   for (li = 0; li >= 0; )
   {
      if ((candi[li] == 0) || (candi[li] < candn[li]) || (nsp >= 0))
         switch (dmode)
         {
         case 1:
            if ((err = Diff_SpComp(ocomp, cres[li], compspec)) != 0)
               return err;
            break;
         case 2:
            if ((err = Diff_SpCompAlign(ocomp, cres[li], compspec)) != 0)
               return err;
            break;
         default:
            break;
         }
      if (candi[li] == 0)
      {
         if ((candn[li] = AntiAlC_Candidates(orig, &res[li], prof, &sp[li],
            cand, candi, ocomp, cres[li], compspec, li)) < 0)
            return candn[li];
         if (candn[li] == 0) return 113;
         if (start + li == 0) for (ri = 0; ri < candn[li]; ri++)
            cand[0][ri].csig = cand[0][ri].sig;
         else if (li == 0) for (ri = 0; ri < candn[li]; ri++)
            cand[li][ri].csig =
            SigSpec_CSig(result[start - 1].csig, cand[li][ri].sig);
         else for (ri = 0; ri < candn[li]; ri++) cand[li][ri].csig =
            SigSpec_CSig(cand[li - 1][candi[li - 1]].csig, cand[li][ri].sig);
      }
      for (ri = 0; ri < start; ri++) Result_Copy(&best[ri], &curr[ri]);
      for (ri = 0; ri <= li; ri++) Result_Copy(&cand[ri][candi[ri]],
         &curr[start + ri]);
      Result_Reset(&curr[start + li + 1]);
      curr[start + li + 1].rms = res[li].rms;
      curr[start + li + 1].ppsc = res[li].ppsc;
      if (li == aad - 1)
      {
         fin = 0;
         tot = 1;
         for (ai = aad - 1; ai >= 0; ai--)
         {
            fin += (candi[ai] + 1) * tot;
            tot += candn[ai] * tot;
         }
      }
      if ((li >= 0) && (candi[li] >= candn[li]))
      {
         if (--li >= 0) ++candi[li];
      }
      else if (li < aad - 1)
      {
         for (fi = 0; fi < ncomp; fi++)
         {
            for (ai = 0; ai < start; ai++) Result_Copy(&curr[ai], &cresult[ai]);
            for (ai = start; ai <= li; ai++) 
               Result_Copy(&cand[ai][candi[ai]], &cresult[ai]);
            if ((err = MultiSine_Fit(&ocomp[fi], &cres[li + 1][fi],
               cresult, 0, 1, start + li + 1)) != 0) return err;
            if ((!quiet) && (aad > 1) && (aap >= 1))
            {
               printf("\rAntiAlC: comparison data                            ");
               printf("    %3i%%", perc = (int)floor(100. * fin / tot + .5));
               fflush(stdout);
            }
            ri = 1;
            do Stat_ZeroMean(cres[li + 1][fi].d[0], ocomp[fi].w, 0,
               cres[li + 1][fi].r - 1, ocomp[fi].ss, ri,
               cres[li + 1][fi].nss, cres[li + 1][fi].d[0]);
            while (++ri <= cres[li + 1][fi].nss);
         }
         if ((err = MultiSine_Fit(orig, &res[li + 1], curr, t, x,
            start + li + 1)) != 0) return err;
         if ((!quiet) && (aad > 1) && (aap >= 1))
         {
            printf("\rAntiAlC                                                ");
            printf(" %3i%%", perc = (int)floor(100. * fin / tot + .5));
            fflush(stdout);
         }
         ri = 1;
         candi[++li] = 0;
      }
      else
      {
         for (fi = 0; fi < ncomp; fi++)
         {
            for (ai = 0; ai < start; ai++) Result_Copy(&curr[ai], &cresult[ai]);
            for (ai = start; ai <= li; ai++) 
               Result_Copy(&cand[ai][candi[ai]], &cresult[ai]);
            if ((err = MultiSine_Fit(&ocomp[fi], &cres[li + 1][fi],
               cresult, 0, 1, start + li + 1)) != 0) return err;
            if ((!quiet) && (aad > 1))
            {
               printf("\rAntiAlC                                             ");
               printf("    %3i%%", perc = (int)floor(100. * fin / tot + .5));
               fflush(stdout);
            }
            ri = 1;
            do Stat_ZeroMean(cres[li + 1][fi].d[0], ocomp[fi].w, 0,
               cres[li + 1][fi].r - 1, ocomp[fi].ss, ri,
               cres[li + 1][fi].nss, cres[li + 1][fi].d[0]);
            while (++ri <= cres[li + 1][fi].nss);
         }
         if ((err = MultiSine_Fit(orig, &res[li + 1], curr, t, x,
            start + li + 1)) != 0) return err;
         if ((!quiet) && (aad > 1))
         {
            printf("\rAntiAlC                                                ");
            printf(" %3i%%", perc = (int)floor(100. * fin / tot + .5));
            fflush(stdout);
         }
         if (res[li + 1].rms < optrms)
         {
            optrms = res[li + 1].rms;
            optppsc = res[li + 1].ppsc;
            for (ai = 0; ai < end; ai++) Result_Copy(&curr[ai], &best[ai]);
            for (ai = 0; ai < aaa; ai++) SigSpec_Copy(&sp[ai], &sigspec[ai]);
            if (aaa < aad)
            {
               best[end - 1].rms = curr[end - 1].rms;
               best[end - 1].ppsc = curr[end - 1].ppsc;
            }
            else
            {
               best[end - 1].rms = optrms;
               best[end - 1].ppsc = optppsc;
            }
            TS_Par(&res[0], &ts[0]);
            for (ai = 1; ai <= aaa; ai++)
            {
               TS_Par(&res[ai], &ts[ai]);
               for (ri = 0; ri < orig->r; ri++)
                  for (hi = 0; hi <= oharm; hi++)
                  ts[ai].d[0][ri] = res[ai].d[0][ri];
               for (fi = 0; fi < ncomp; fi++)
                  for (ri = 0; ri < cres[ai][fi].r; ri++)
                  cdata[ai][fi].d[0][ri] = cres[ai][fi].d[0][ri];
            }
         }
         ++candi[li];
      }
   }
   for (li = 0; li < end; li++) Result_Copy(&best[li], &result[li]);
   ts[0].rms = best[end - 1].rms;
   ts[0].ppsc = best[end - 1].ppsc;
   for (li = 0; li <= aad; li++)
   {
      for (fi = 0; fi < ncomp; fi++)
      {
         free(cres[li][fi].d[0]);
         free(cres[li][fi].d);
         free(cres[li][fi].cf);
         free(cres[li][fi].cw);
      }
      free(res[li].d[0]);
      free(res[li].d);
      free(res[li].cf);
      free(res[li].cw);
      free(res[li].nsse);
      free(cres[li]);
   }
   for (li = 0; li < aad; li++)
   {
      free(sp[li].a);
      free(sp[li].b);
      free(sp[li].sig);
      if (dmode > 0)
      {
         free(sp[li].ca);
         free(sp[li].cb);
      }
      for (ai = 0; ai < (int)floor(.5 * nff); ai++) free(cand[li][ai].h);
      free(cand[li]);
   }
   for (li = 0; li < end; li++) free(best[li].h);
   for (li = 0; li < hl; li++)
   {
      free(curr[li].h);
      free(cresult[li].h);
   }
   free(res);
   free(cres);
   free(sp);
   free(cand);
   free(candi);
   free(candn);
   free(curr);
   free(best);
   free(cresult);
   return 0;
}



int AntiAlC_Sort(
   struct Result* result,
   int end
   )

{
   double d;
   int xchg = 1;
   int* spot;
   int ri;
   int ci;
   if ((spot = (int*)calloc(end, sizeof(int))) == NULL) return -188;
   for (ri = 0; ri < end; ri++) spot[ri] = 0;
   while(xchg > 0)
   {
      xchg = 0;
      for (ri = 0; ri < end - 1; ri++)
      {
         if (spot[ri] * spot[ri + 1] == 0)
         {
            if (result[ri].sig < result[ri + 1].sig)
            {
               d = result[ri + 1].f;
               result[ri + 1].f = result[ri].f;
               result[ri].f = d;
               d = result[ri + 1].sig;
               result[ri + 1].sig = result[ri].sig;
               result[ri].sig = d;
               d = result[ri + 1].rms;
               result[ri + 1].rms = result[ri].rms;
               result[ri].rms = d;
               d = result[ri + 1].ppsc;
               result[ri + 1].ppsc = result[ri].ppsc;
               result[ri].ppsc = d;
               xchg++;
               spot[ri] = 0;
               spot[ri + 1] = 0;
            }
            else
            {
               spot[ri] = 1;
               spot[ri + 1] = 1;
            }
         }
      }
   }
   free(spot);
   return end;
}


int CmdLine_Scan(
   int argc,
   char* argv[]
   )

{
   struct String testfile;
   FILE* dummy;
   char cbuf[MAXCHAR];
   if (!quiet)
   {
      Log_Header("start");
      printf("command line interface\n");
   }
   if ((argc < 2) || (argc > 3)) return 1;
   String_Chars2String(argv[1], &project);
   if ((String_Comp2Char(&project, "help") == 0) ||
      (String_Comp2Char(&project, "-?") == 0) ||
      (String_Comp2Char(&project, "-help") == 0) ||
      (String_Comp2Char(&project, "--help") == 0))
      return -1;
   if (!quiet) printf("Checking availability of project directory %s...\n",
      String_AsChars(&project, cbuf));
   testfile.c = (int*)calloc(String_SetLength(&testfile,
      File_NameLength(&project, -1, -1, -1, "dummy", -1, -1, NULL)),
      sizeof(int));
   File_Name(&project, -1, -1, -1, "dummy", -1, -1, NULL, &testfile);
   if ((dummy = fopen(String_AsChars(&testfile, cbuf), "w")) == NULL) return 2;
   free(testfile.c);
   fclose(dummy);
   remove(cbuf);
   if (!quiet) printf("project directory %s ok.\n",
      String_AsChars(&project, cbuf));
   return 0;
}



int Correlogram_Generate(
   struct TS* orig,
   struct TS* ts,
   int ti
   )

{
   int ibuf;
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int corro = co;
   int err;
   double* dbuf;
   if (!quiet)
      printf("\rcorrelogram                                                  ");
   fflush(stdout);
   if (co <= 0) corro = (int)ceil(.5 * ts->r);
   dbuf = (double*)calloc(corro, sizeof(double));
   for (ibuf = 0; ibuf < corro; ibuf++)
   {
      dbuf[ibuf] = Stat_AutoCorr(ts->d[0], orig->w, 0, ts->r - 1, ibuf, 0, 0,
         &err);
      if (!quiet) Log_Percent("correlogram", ibuf, corro);
   }
   if (ts->it < 0)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, ti, -1, "rescorr", -1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "rescorr", -1, ti, "dat",
         &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "c", ts->it, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, ti, -1, "c", ts->it, ti, "dat", &fname);
   }
   if (!quiet)
      printf("\r                                                             ");
   String_AsChars(&fname, cbuf);
   free(fname.c);
   if ((F = fopen(cbuf, "w")) == NULL) return 111;
   for (ibuf = 0; ibuf < corro; ibuf++) fprintf(F, "%12i %24.16lf\n", ibuf,
      dbuf[ibuf]);
   fclose(F);
   free(dbuf);
   return 0;
}



int Debug_AntiAlC(
   int iteration,
   int iterations,
   int level,
   int levels,
   int* candi,
   int* candn,
   struct Result* curr,
   struct Result* best,
   struct Result* result,
   struct TS* orig,
   struct TS* res,
   char* dstr
   )



{
   int ii;
   int hi;
   int err;
   char app[1024];
   sprintf(dstr, "Iteration %i of %i = level %i of %i\n", iteration + 1,
      iterations, level, levels);
   sprintf(app, "<it>h<harm> (<cand>/<cands>) f <curr>/<best>/<result> ");
   strcat(dstr, app);
   sprintf(app, "sig <curr>/<best>/<result> amp <curr>/<best>/<result> ");
   strcat(dstr, app);
   sprintf(app, "rms <curr>/<best>/<result> (<resrms>/<resdata>)\n");
   strcat(dstr, app);
   for (ii = 0; ii < iteration - level; ii++) for (hi = 0; hi <= oharm; hi++)
   {
      sprintf(app, "%ih%i f %lg/%lg/%lg sig %lg/%lg/%lg amp %lg/%lg/%lg ",
         ii + 1, hi, curr[ii].f, best[ii].f, result[ii].f, curr[ii].sig,
         best[ii].sig, result[ii].sig, curr[ii].h[hi].amp, best[ii].h[hi].amp,
         result[ii].h[hi].amp);
      strcat(dstr, app);
      sprintf(app, "rms %lg/%lg/%lg\n", curr[ii].rms, best[ii].rms,
         result[ii].rms);
      strcat(dstr, app);
   }
   for (ii = iteration - level; ii < iterations - 1; ii++)
      for (hi = 0; hi <= oharm; hi++)
   {
      sprintf(app, "%ih%i (%i/%i) f %lg/%lg/%lg sig %lg/%lg/%lg ",
         ii + 1, hi, candi[ii + level - iteration],
         candn[ii + level - iteration], curr[ii].f, 0., result[ii].f,
         curr[ii].sig, 0., result[ii].sig);
      strcat(dstr, app);
      sprintf(app, "amp %lg/%lg/%lg rms %lg/%lg/%lg (%lg/%lg)\n",
         curr[ii].h[hi].amp, 0., result[ii].h[hi].amp,
         curr[ii].rms, 0., result[ii].rms,
         res[ii + level - iteration].rms,
         Stat_SDev(res[ii + level - iteration].d[0], orig->w, 0, orig->r - 1, 0,
         0, &err));
      strcat(dstr, app);
   }
   for (hi = 0; hi <= oharm; hi++)
   {
      sprintf(app, "%ih%i (%i/%i) f %lg/%lg/%lg sig %lg/%lg/%lg ",
         iterations, hi, 0, 0, 0., 0., 0., 0., 0., 0.);
      strcat(dstr, app);
      sprintf(app, "amp %lg/%lg/%lg rms %lg/%lg/%lg (%lg/%lg)\n",
         0., 0., 0., curr[0].rms, 0., result[0].rms,
         res[aad].rms, Stat_SDev(res[aad].d[0], orig->w, 0, orig->r - 1, 0,
         0, &err));
      strcat(dstr, app);
   }
   return 0;
}



int Debug_MultiSine(
   int iteration,
   int signals,
   struct Result* result,
   struct Result* var1,
   struct Result* var2,
   struct TS* orig,
   struct TS* ts,
   char* dstr
   )



{
   int ii;
   int hi;
   int err;
   char app[1024];
   sprintf(dstr, "Iteration %i: %i signals\n", iteration, signals);
   sprintf(app, "<signal>h<harm> f <result>/<d> sig <result> ");
   strcat(dstr, app);
   sprintf(app, "amp <result>/<d> ");
   strcat(dstr, app);
   sprintf(app, "ph <result>/<d> ");
   strcat(dstr, app);
   sprintf(app, "rms <result>/<resdata>/<tsdata>/<orig>\n");
   strcat(dstr, app);
   for (ii = 0; ii < signals; ii++) for (hi = 0; hi <= oharm; hi++)
   {
      sprintf(app,
         "%ih%i f %lg/%lg/%lg sig %lg amp %lg/%lg/%lg ph %lg/%lg/%lg rms %lg\n",
         ii + 1, hi, result[ii].f, var1[ii].f, var2[ii].f, result[ii].sig,
         result[ii].h[hi].amp, var1[ii].h[hi].amp, var2[ii].h[hi].amp,
         result[ii].h[hi].th, var1[ii].h[hi].th, var2[ii].h[hi].th,
         result[ii].rms);
      strcat(dstr, app);
   }
   for (hi = 0; hi <= oharm; hi++)
   {
      sprintf(app, "%ih%i f %lg/%lg/%lg sig %lg amp %lg/%lg/%lg ",
         signals + 1, hi, result[signals].f, 0., 0., result[signals].sig, 0., 0.,
         0.);
      strcat(dstr, app);
      sprintf(app, "ph %lg/%lg/%lg rms %lg/%lg/%lg/%lg\n", 0., 0., 0.,
         result[signals].rms, ts->rms,
         Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &err),
         Stat_SDev(orig->d[1], orig->w, 0, orig->r - 1, 0, 0, &err));
      strcat(dstr, app);
   }
   return 0;
}


int Diff_Comp(
   struct TS* ocomp,
   struct TS* cdata,
   double freq,
   double* ccos,
   double* csin
   )

{
   int fi;
   int ri;
   double f = freq;
   double curcos;
   double cursin;
   *ccos = 0;
   *csin = 0;
   for (fi = 0; fi < ncomp; fi++)
   {
      curcos = 0;
      cursin = 0;
      for (ri = 0; ri < cdata[fi].r; ri++)
      {
         curcos += ocomp[fi].w[ri] * cdata[fi].d[0][ri] *
            cos(2. * PI * f * ocomp[fi].d[0][ri]);
         cursin += ocomp[fi].w[ri] * cdata[fi].d[0][ri] *
            sin(2. * PI * f * ocomp[fi].d[0][ri]);
      }
      *ccos += curcos / cdata[fi].ppsc;
      *csin += cursin / cdata[fi].ppsc;
   }
   *ccos /= nnorm;
   *csin /= nnorm;
   return 0;
}



double Diff_CompAlign(
   struct TS* ocomp,
   struct TS* cdata,
   double freq,
   double* camp
   )

{
   int fi;
   int ri;
   double f = freq;
   double curcos;
   double cursin;
   *camp = 0;
   for (fi = 0; fi < ncomp; fi++)
   {
      curcos = 0;
      cursin = 0;
      for (ri = 0; ri < cdata[fi].r; ri++)
      {
         curcos += ocomp[fi].w[ri] * cdata[fi].d[0][ri] *
            cos(2. * PI * f * ocomp[fi].d[0][ri]);
         cursin += ocomp[fi].w[ri] * cdata[fi].d[0][ri] *
            sin(2. * PI * f * ocomp[fi].d[0][ri]);
      }
      *camp += (pow(curcos, 2) + pow(cursin, 2));
   }
   *camp = 2. * sqrt(*camp / nnorm);
   return 0;
}



int Diff_Init(
   double* noise1,
   double* noise2
   )

{
   int ri;
   for (ri = 0; ri < nspr; ri++)
   {
      noise1[ri] = 0;
      if (noise2 != NULL) noise2[ri] = 0;
   }
   return 0;
}



int Diff_Level(
   struct TS* ocomp,
   struct TS* cdata,
   double freq,
   struct TS* ts,
   double* noise1,
   double* noise2
   )

{
   switch (dmode)
   {
   case 0:
      *noise1 = 0;
      *noise2 = 0;
      return 0;
      break;
   case 1:
      return Diff_Comp(ocomp, cdata, freq, noise1, noise2);
      break;
   case 2:
      return Diff_CompAlign(ocomp, cdata, freq, noise1);
      break;
   default:
      break;
   }
   return 0;
}



int Diff_SpComp(
   struct TS* ocomp,
   struct TS* cdata,
   struct SigSpec* compspec
   )

{
   int ni;
   int ri;
   int si;
   int f;
   int err;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double df = (uf - lf) / (nspr - 1.);
   if (!quiet)
   {
      printf("\rnoise spectrum from comparison data                          ");
      fflush(stdout);
   }
   Diff_Init(compspec->a, compspec->b);
   if (cdata == NULL) for (ni = 0; ni < ncomp; ni++)
   {
      for (ri = 0; ri < ocomp[ni].r; ri++)
      {
         curcos = ocomp[ni].d[1][ri] * cos(2. * PI * lf * ocomp[ni].d[0][ri]) /
            ocomp[ni].r;
         cursin = ocomp[ni].d[1][ri] * sin(2. * PI * lf * ocomp[ni].d[0][ri]) /
            ocomp[ni].r;
         dcos = cos(2. * PI * df * ocomp[ni].d[0][ri]);
         dsin = sin(2. * PI * df * ocomp[ni].d[0][ri]);
         for (si = 0; si < nspr; si++)
         {
            compspec->a[si] += ocomp[ni].w[ri] * curcos;
            compspec->b[si] += ocomp[ni].w[ri] * cursin;
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet) Log_Percent("noise spectrum from comparison data",
              (double)ni * ocomp[ni].r * nspr + ri * nspr + si,
              (double)ncomp * ocomp[ni].r * nspr);
         }
      }
   }
   else for (ni = 0; ni < ncomp; ni++)
   {
      for (ri = 0; ri < ocomp[ni].r; ri++)
      {
         curcos = cdata[ni].d[0][ri] * cos(2. * PI * lf * ocomp[ni].d[0][ri]) /
            cdata[ni].r;
         cursin = cdata[ni].d[0][ri] * sin(2. * PI * lf * ocomp[ni].d[0][ri]) /
            cdata[ni].r;
         dcos = cos(2. * PI * df * ocomp[ni].d[0][ri]);
         dsin = sin(2. * PI * df * ocomp[ni].d[0][ri]);
         for (si = 0; si < nspr; si++)
         {
            compspec->a[si] += ocomp[ni].w[ri] * curcos;
            compspec->b[si] += ocomp[ni].w[ri] * cursin;
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet) Log_Percent("noise spectrum from comparison data",
              (double)ni * cdata[ni].r * nspr + ri * nspr + si,
              (double)ncomp * cdata[ni].r * nspr);
         }
      }
   }
   for (si = 0; si < nspr; si++) nnorm += 4. *
      (pow(compspec->a[si], 2) + pow(compspec->b[si], 2));
   nnorm /= nspr;
   for (si = 0; si < nspr; si++)
   {
      compspec->a[si] /= sqrt(nnorm);
      compspec->b[si] /= sqrt(nnorm);
   }
   return 0;
}



int Diff_SpCompAlign(
   struct TS* ocomp,
   struct TS* cdata,
   struct SigSpec* compspec
   )

{
   int ni;
   int ri;
   int si;
   int f;
   int err;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double df = (uf - lf) / (nspr - 1.);
   double* ccos = (double*)calloc(nspr, sizeof(double));
   double* csin = (double*)calloc(nspr, sizeof(double));
   int rows = 0;
   if (!quiet)
   {
      printf("\rnoise from comparison data (aligned)                         ");
      fflush(stdout);
   }
   compspec->rms = 0;
   Diff_Init(compspec->a, NULL);
   if (cdata == NULL) for (ni = 0; ni < ncomp; ni++)
   {
      compspec->rms += ocomp[ni].r * pow(ocomp[ni].rms, 2);
      rows += ocomp[ni].r;
      Diff_Init(ccos, csin);
      for (ri = 0; ri < ocomp[ni].r; ri++)
      {
         curcos = ocomp[ni].d[1][ri] * cos(2. * PI * lf * ocomp[ni].d[0][ri]);
         cursin = ocomp[ni].d[1][ri] * sin(2. * PI * lf * ocomp[ni].d[0][ri]);
         dcos = cos(2. * PI * df * ocomp[ni].d[0][ri]);
         dsin = sin(2. * PI * df * ocomp[ni].d[0][ri]);
         for (si = 0; si < nspr; si++)
         {
            ccos[si] += ocomp[ni].w[ri] * curcos;
            csin[si] += ocomp[ni].w[ri] * cursin;
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet)
              Log_Percent("noise spectrum from comparison data (aligned)",
              (double)ni * ocomp[ni].r * nspr + ri * nspr + si,
              (double)ncomp * ocomp[ni].r * nspr);
         }
      }
      for (si = 0; si < nspr; si++)
      {
         compspec->a[si] += 4. * (pow(ccos[si], 2) + pow(csin[si], 2));
         nnorm += 4. * (pow(ccos[si], 2) + pow(csin[si], 2));
      }
   }
   else for (ni = 0; ni < ncomp; ni++)
   {
      compspec->rms += ocomp[ni].r * pow(ocomp[ni].rms, 2);
      rows += ocomp[ni].r;
      Diff_Init(ccos, csin);
      for (ri = 0; ri < ocomp[ni].r; ri++)
      {
         curcos = cdata[ni].d[0][ri] * cos(2. * PI * lf * ocomp[ni].d[0][ri]);
         cursin = cdata[ni].d[0][ri] * sin(2. * PI * lf * ocomp[ni].d[0][ri]);
         dcos = cos(2. * PI * df * ocomp[ni].d[0][ri]);
         dsin = sin(2. * PI * df * ocomp[ni].d[0][ri]);
         for (si = 0; si < nspr; si++)
         {
            ccos[si] += ocomp[ni].w[ri] * curcos;
            csin[si] += ocomp[ni].w[ri] * cursin;
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet)
              Log_Percent("noise spectrum from comparison data (aligned)",
              (double)ni * cdata[ni].r * nspr + ri * nspr + si,
              (double)ncomp * cdata[ni].r * nspr);
         }
      }
      for (si = 0; si < nspr; si++)
      {
         compspec->a[si] += 4. * (pow(ccos[si], 2) + pow(csin[si], 2));
         nnorm += 4. * (pow(ccos[si], 2) + pow(csin[si], 2));
      }
   }
   nnorm /= nspr;
   compspec->rms = sqrt(compspec->rms / rows);
   free(ccos);
   free(csin);
   for (si = 0; si < nspr; si++)
      compspec->a[si] = sqrt(compspec->a[si] / nnorm);
   return 0;
}



int Diff_SpWhite(
   double* noise
   )

{
   int ri;
   for (ri = 0; ri < nspr; ri++)
      noise[ri] = 0;
   return 0;
}



int File_Name(
   struct String* path,
   int fi,
   int sep,
   int cpi,
   char* ftype,
   int it,
   int ti,
   char* ext,
   struct String* out
   )

{
   int i;
   int pos = 0;
   struct String mfbuf;
   struct String cpbuf;
   struct String itbuf;
   struct String tibuf;
   char cbuf[MAXCHAR];
   if (path != NULL)
   {
      pos = String_Copy(path, out, pos);
      pos = String_SetChar(out, '/', pos);
   }
   if (fi >= 0)
   {
      mfbuf.c = (int*)calloc(String_SetLength(&mfbuf, MFDIGITS), sizeof(int));
      NumFormat_FixDigits(fi, &mfbuf);
      pos = String_Copy(&mfbuf, out, pos);
      free(mfbuf.c);
      if (cpi >= 0)
      {
         pos = String_SetChar(out, sep, pos);
         cpbuf.c = (int*)calloc(String_SetLength(&mfbuf, MFDIGITS), sizeof(int));
         pos = String_Copy(&cpbuf, out, pos);
         free(cpbuf.c);
      }
      pos = String_SetChar(out, '.', pos);
   }
   pos = String_SetChars(out, ftype, pos);
   if (it >= 0)
   {
      itbuf.c = (int*)calloc(String_SetLength(&itbuf, ITDIGITS), sizeof(int));
      NumFormat_FixDigits(it, &itbuf);
      pos = String_Copy(&itbuf, out, pos);
      free(itbuf.c);
   }
   if (ti >= 0)
   {
      pos = String_SetChar(out, '.', pos);
      tibuf.c = (int*)calloc(String_SetLength(&tibuf, TIDIGITS), sizeof(int));
      NumFormat_FixDigits(ti, &tibuf);
      pos = String_Copy(&tibuf, out, pos);
      free(tibuf.c);
   }
   if (ext != NULL)
   {
      pos = String_SetChar(out, '.', pos);
      pos = String_SetChars(out, ext, pos);
   }
   return 0;
}



int File_NameLength(
   struct String* path,
   int fi,
   int sep,
   int cpi,
   char* ftype,
   int it,
   int ti,
   char* ext
   )

{
   int len = strlen(ftype);
   if (path != NULL) len += String_GetLength(path) + 1;
   if (ext != NULL) len += strlen(ext) + 1;
   if (fi >= 0) len += MFDIGITS + 1;
   if (cpi >= 0) len += MFDIGITS + 1; 
   if (it >= 0) len += ITDIGITS;
   if (ti >= 0) len += TIDIGITS + 1;
   return len;
}



double Fit_Amp(
   struct TS* orig,
   struct TS* ts,
   double f,
   double* ph)

{
   int si;
   int i;
   double xcsum = 0;
   double csum;
   double c2sum = 0;
   double amp = 0;
   double wsum;
   int count;
   FILE* log;
   if (debug >= 0) log = fopen("Fit_Amp.log", "a");
   if (debug > 0)
   {
      fprintf(log, "**** Fit_Amp ******************************************\n");
      fflush(log);
   }
   for (si = 1; si <= orig->nss; si++)
   {
      csum = 0;
      for (i = 0; i < orig->r; i++) if ((orig->nss < 2) || (orig->ss[i] == si))
         csum += ts->d[0][i];
      c2sum += pow(csum, 2);
   }
   if (debug > 0) fprintf(log, "0038 ZeroMean corrected? %lg\n", c2sum);
   for (si = 1; si <= orig->nss; si++)
   {
      csum = 0;
      c2sum = 0;
      count = 0;
      wsum = 0;
      for (i = 0; i < orig->r; i++) if ((orig->nss < 2) || (orig->ss[i] == si))
      {
         wsum += orig->w[i];
         xcsum +=
            orig->w[i] * ts->d[0][i] * cos(2 * PI * f * orig->d[t][i] - *ph);
         csum += orig->w[i] * cos(2 * PI * f * orig->d[t][i] - *ph);
         c2sum += orig->w[i] * pow(cos(2 * PI * f * orig->d[t][i] - *ph), 2);
      }
      amp += c2sum - pow(csum, 2) / wsum;
   }
   if ((amp = xcsum / amp) < 0)
   {
      if (*ph > 0) *ph -= PI;
      else *ph += PI;
      if (debug > 1)
      {
         fprintf(log, "0061    phase: %lg amplitude: %lg.\n", *ph, -amp);
         fflush(log);
      }
      if (debug > 0)
      {
         fprintf(log, "**** Fit_Amp END **************************************\n");
         fflush(log);
      }
      if (debug >= 0) fclose(log);
      return -amp;
   }
   if (debug > 0)
   {
      fprintf(log, "0074 amplitude: %lg.\n", amp);
      fflush(log);
   }
   if (debug > 0)
   {
      fprintf(log, "**** Fit_Amp END **************************************\n");
      fflush(log);
   }
   if (debug >= 0) fclose(log);
   return amp;
}



double Fit_Phase(
   struct TS* orig,
   struct TS* ts,
   double f
   )

{
   int si;
   int i;
   double csum;
   double ssum;
   double xcsum = 0;
   double xssum = 0;
   double c2sum = 0;
   double s2sum = 0;
   double cssum = 0;
   FILE* log;
   double wsum;
   if (debug >= 0) log = fopen("Fit_Phase.log", "a");
   if (debug > 0)
   {
      fprintf(log, "**** Fit_Phase ****************************************\n");
      fflush(log);
   }
   for (si = 1; si <= orig->nss; si++)
   {
      if (debug > 1)
      {
         fprintf(log, "0123    subset %i of %i\n", si, orig->nss);
         fflush(log);
      }
      csum = 0;
      ssum = 0;
      wsum = 0;
      for (i = 0; i < orig->r; i++)
         if ((orig->nss < 2) || (orig->ss[i] == si))
      {
         if (debug > 2)
         {
            fprintf(log, "0134       ");
            fprintf(log, "subset %i of %i - weight %lg - %lg %lg.\n", si,
               orig->nss, orig->w[i], orig->d[t][i], ts->d[0][i]);
            fflush(log);
         }
         wsum += orig->w[i];
         csum += orig->w[i] * cos(2. * PI * f * orig->d[t][i]);
         ssum += orig->w[i] * sin(2. * PI * f * orig->d[t][i]);
         xcsum += ts->d[0][i] * orig->w[i] * cos(2. * PI * f * orig->d[t][i]);
         xssum += ts->d[0][i] * orig->w[i] * sin(2. * PI * f * orig->d[t][i]);
         cssum += .5 * orig->w[i] * sin(4. * PI * f * orig->d[t][i]);
         c2sum += orig->w[i] * pow(cos(2. * PI * f * orig->d[t][i]), 2);
         s2sum += orig->w[i] * pow(sin(2. * PI * f * orig->d[t][i]), 2);
      }
      cssum -= csum * ssum / wsum;
      c2sum -= csum * csum / wsum;
      s2sum -= ssum * ssum / wsum;
      if (debug > 1)
      {
         fprintf(log, "0153    subset %i of %i - phase: %lg.\n", si,
            orig->nss, atan2(cssum * xcsum - c2sum * xssum,
            cssum * xssum - s2sum * xcsum));
         fflush(log);
      }
   }
   if (debug > 0)
   {
      fprintf(log, "0161 phase: %lg.\n",
         atan2(cssum * xcsum - c2sum * xssum, cssum * xssum - s2sum * xcsum));
      fflush(log);
   }
   if (debug > 0)
   {
      fprintf(log, "**** Fit_Phase END ************************************\n");
      fflush(log);
   }
   if (debug >= 0) fclose(log);
   return atan2(cssum * xcsum - c2sum * xssum, cssum * xssum - s2sum * xcsum);
}



int Fit_Prewhitening(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   struct TS* res
   )

{
   int ri;
   int hi;
   int fi;
   FILE* log;
   if (debug >= 0) log = fopen("Fit_Prewhitening.log", "a");
   if (debug > 0)
   {
      fprintf(log, "0200 ");
      fprintf(log, "Setting number of rows in residual time series to %i.\n",
         ts->r);
      fflush(log);
   }
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri, orig->nss,
      ts->d[0]);
   while (++ri <= orig->nss);
   res->r = ts->r;
   if (debug > 0)
   {
      fprintf(log, "0212 !!!Fit_Phase!!!\n");
      fflush(log);
   }
   result->h[0].th = Fit_Phase(orig, ts, result->f);
   if (debug > 0)
   {
      fprintf(log, "0218 Fundamental phase: %lg.\n", result->h[0].th);
      fprintf(log, "0219 !!!Fit_Amp!!!\n");
      fflush(log);
   }
   result->h[0].amp = Fit_Amp(orig, ts, result->f, &(result->h[0].th));
   if (debug > 0)
   {
      fprintf(log, "0225 Fundamental amplitude: %lg.\n", result->h[0].amp);
      fflush(log);
   }
   if (result->h[0].amp < 0)
   {
      if (debug > 1)
      {
         fprintf(log, "0232    Fundamental amplitude < 0: %lg.\n",
            -result->h[0].amp);
         fflush(log);
      }
      result->h[0].amp = -result->h[0].amp;
      if (result->h[0].th < PI)
      {
         if (debug > 2)
         {
            fprintf(log, "0241       Fundamental amplitude < 0: phase %lg.\n",
               result->h[0].th + PI);
            fflush(log);
         }
         result->h[0].th += PI;
      }
      else
      {
         if (debug > 2)
         {
            fprintf(log, "0251       Fundamental amplitude < 0: phase %lg.\n",
               result->h[0].th - PI);
            fflush(log);
         }
         result->h[0].th -= PI;
      }
   }
   for (ri = 0; ri < ts->r; ri++)
   {
      res->d[0][ri] = ts->d[0][ri] - result->h[0].amp * cos(2 * PI * result->f *
         orig->d[t][ri] - result->h[0].th);
      if (debug > 1)
      {
         fprintf(log, "0264    %lg %lg %lg\n", orig->d[t][ri], ts->d[0][ri],
            res->d[0][ri]);
         fflush(log);
      }
   }
   fi = 1;
   if (debug > 0)
   {
      fprintf(log, "0272 ZeroMean correction for residual time series.\n");
      fflush(log);
   }
   do
   {
      if (debug > 1)
      {
         fprintf(log, "0279    ZeroMean correction for residual time series: ");
         fprintf(log, "subset %i of %i.\n", fi, orig->nss);
         fflush(log);
      }
      Stat_ZeroMean(res->d[0], orig->w, 0, orig->r - 1, orig->ss, fi, orig->nss,
         res->d[0]);
   }
   while (++fi <= orig->nss);
   for (hi = 1; hi <= oharm; hi++)
   {
      result->h[hi].th = Fit_Phase(orig, res, (hi + 1) * result->f);
      result->h[hi].amp = Fit_Amp(orig, res, (hi + 1) * result->f,
         &(result->h[hi].th));
      if (result->h[hi].amp < 0)
      {
         result->h[hi].amp = -result->h[hi].amp;
         if (result->h[hi].th < PI) result->h[hi].th += PI;
         else result->h[hi].th -= PI;
      }
      for (ri = 0; ri < ts->r; ri++)
         res->d[0][ri] -= result->h[hi].amp * cos(2 * PI * (hi + 1.) * result->f *
         orig->d[t][ri] - result->h[hi].th);
      fi = 1;
      do Stat_ZeroMean(res->d[0], orig->w, 0, orig->r - 1, orig->ss, fi,
         orig->nss, res->d[0]);
      while (++fi <= orig->nss);
   }
   if (debug >= 0) fclose(log);
   return 0;
}



int Fit_SinglePrewhitening(
   struct TS* orig,
   struct TS* ts,
   double f,
   double *a,
   double *th,
   struct TS* res
   )

{
   int ri;
   int hi;
   int fi;
   FILE* log;
   if (debug >= 0) log = fopen("Fit_SinglePrewhitening.log", "a");
   if (debug > 0) fprintf(log, "0336 !!!Fit_Phase!!!\n");
   *th = Fit_Phase(orig, ts, f);
   if (debug > 0) fprintf(log, "0338 !!!Fit_Amp!!!\n");
   *a = Fit_Amp(orig, ts, f, th);
   if (*a < 0)
   {
      *a = - *a;
      if (*th < PI) *th += PI;
      else *th -= PI;
   }
   for (ri = 0; ri < ts->r; ri++) res->d[0][ri] = ts->d[0][ri] - *a *
      cos(2 * PI * f * orig->d[t][ri] - *th);
   fi = 1;
   do Stat_ZeroMean(res->d[0], orig->w, 0, orig->r - 1, orig->ss, fi,
      orig->nss, res->d[0]);
   while (++fi <= orig->nss);
   if (debug >= 0) fclose(log);
   return 0;
}



int Harmonics_Write(
   struct Result* result,
   struct String* fname
   )

{
   char cbuf[MAXCHAR];
   FILE* F;
   int err;
   int hi;
   if ((F = fopen(String_AsChars(fname, cbuf), "w")) == NULL) return 145;
   for (hi = 0; hi <= oharm; hi++) fprintf(F, "%24.16lf %24.16lf %24.16lf\n",
         result->h[hi].sig, result->h[hi].amp, result->h[hi].th);
   fclose(F);
   return 0;
}


int Help_Display(
   )

{
   printf("\nPlease refer to the SigSpec manual for help.\n");
   printf("An HTML manual is available online at\n");
   printf("http://www.astro.univie.ac.at/SigSpec\n");
   printf("For further information, please contact Piet Reegen\n");
   printf("(reegen@astro.univie.ac.at)\n");
   fflush(stdout);
   return 0;
}



int IniFile_Calculate(
   struct TS* ts
   )

{
   int fi;
   double nybk = DBL_MAX;
   double ufbk = -1;
   int err;
   for (fi = 0; fi < nmf; fi++) if (ts[fi].ct != 0)
   {
      fres = 1. / trwidth;
      if (ny >= 0)
      {
         if (Nyquist_Freq(ts[fi].d[t], 0, ts[fi].r - 1, ny) > ufbk)
            ufbk = Nyquist_Freq(ts[fi].d[t], 0, ts[fi].r - 1, ny);
      }
      else
      {
         if (Nyquist_Coef(ts[fi].d[t], 0, ts[fi].r - 1, uf) < nybk)
            nybk = Nyquist_Coef(ts[fi].d[t], 0, ts[fi].r - 1, uf);
      }
   }
   if (ny >= 0) uf = ufbk;
   else ny = nybk;
   if (os > 0) fs = fres / os;
   else os = fres / fs;
   h0 = (int)floor(lf / fs);
   lf = h0 * fs;
   if (lf == 0)
   {
      lf = fs;
      nspr--;
   }
   nspr = (int)ceil((uf - lf) / fs + .5) * (oharm + 1.) + (h0 - 1) * oharm;
   uf = lf + (nspr - 1.) * fs;
   uff = uf / (oharm + 1.);
   nff = (int)floor((uff - lf) / fs + .5) + 1;
   nfh = (int)floor(uff / fs);
   if (!quiet)
   {
      Log_Double("Rayleigh frequency resolution", fres);
      Log_Double("oversampling ratio", os);
      Log_Double("frequency spacing", fs);
      Log_Double("lower frequency limit", lf);
      Log_Double("upper frequency limit", uf);
      Log_Double("Nyquist coefficient", ny);
      Log_Int("number of frequencies", nspr);
      if (oharm > 0) 
      {
         Log_Double("upper fundamental frequency", uff);
         Log_Int("number of fundamental frequencies", nff);
      }
   }
   if (npd > 0)
   {
      if (pdb < 2) pdb = (int)floor(.5 * ts->r);
      if (pdw <= 0) pdw = 2. / pdb;
   }
   if (pf < 0) pf = ts->r;
   for (fi = 0; fi < nmf; fi++) if (ts[fi].ct != 0) if ((nyscan > 0) &&
      ((err = Nyquist_Scan(&ts[fi], 0, ts[fi].r - 1)) != 0)) return err;
   return 0;
}



int IniFile_CheckIni(
   )

{
   int i;
   if (!quiet)
   {
      printf("checking .ini file entries\n");
      fflush(stdout);
   }
   if ((nmf > 0) && (mfs >= nmf)) return 121;
   if (t == x) return 122;
   for (i = 0; i < nwc; i++) if (wraw[i] == t) return 123;
   for (i = 0; i < nwc; i++) if (wraw[i] == x) return 124;
   if (trwidth * trstep <= 0) return 125;
   if (trmode * trwidth < 0) return 126;
   if (trmode * trstep < 0) return 127;
   if ((trmode == 4) && (trph == DBL_MIN)) return 128;
   if ((trmode == 5) && (trph == DBL_MIN)) return 129;
   if ((trmode == 5) && (trexp == DBL_MIN)) return 130;
   if (it < 0) it = INT_MAX;
   if ((aap >= 0) && (aap <= 1) && (aad < aaa))
   {
      Log_ErrorMessage(131);
      Log_Int("         new depth parameter", aad = (int)ceil(pow(aap, -.5)));
   }
   if (aad <= 1) aan = 1;
   return 0;
}



int IniFile_Cind(
   struct TS* ts
   )

{
   FILE* def;
   char keyword[MAXCHAR];
   int fi = 0;
   int err;
   struct String ini;
   char cbuf[MAXCHAR];
   ini.c = (int*)calloc(String_SetLength(&ini, File_NameLength(NULL, -1, -1,
      -1, String_AsChars(&project, cbuf), -1, -1, "ini")),sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &ini);
   if ((def = fopen(String_AsChars(&ini, cbuf), "r")) == NULL) return -114;
   free(ini.c);
   while ((err = fscanf(def, "%s", keyword)) == 1)
   {
      if (keyword[0] == '#') while (fgetc(def) != '\n');
      else if (strcmp(keyword, "deftype") == 0)
      {
         if ((err = fscanf(def, "%s", keyword)) != 1) Log_ErrorMessage(61);
         else if (strcmp(keyword, "comp") == 0) cinddef = -1;
         else if (strcmp(keyword, "skip") == 0) cinddef = 0;
         else if (strcmp(keyword, "target") == 0) cinddef = 1;
         else Log_ErrorMessage(61);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "target") == 0)
      {
         err = fscanf(def, "%i", &fi);
         if ((fi >= mfs) && (fi < mfs + nmf)) ts[fi - mfs].ct = 1;
         else Log_ErrorMessage(60);
         while (fgetc(def) != '\n');
      } 
      else if (strcmp(keyword, "comp") == 0)
      {
         err = fscanf(def, "%i", &fi);
         if ((fi >= mfs) && (fi < mfs + nmf)) ts[fi - mfs].ct = -1;
         else Log_ErrorMessage(60);
         while (fgetc(def) != '\n');
      } 
      else if (strcmp(keyword, "skip") == 0)
      {
         err = fscanf(def, "%i", &fi);
         if ((fi >= mfs) && (fi < mfs + nmf)) ts[fi - mfs].ct = 0;
         else Log_ErrorMessage(60);
         while (fgetc(def) != '\n');
      } 
      else while (fgetc(def) != '\n');
   }
   fclose(def);
   return 0;
}



int IniFile_LoadIni(
   )

{
   FILE* def;
   int err;
   char keyword[MAXCHAR];
   int wi = 0;
   int ssi = 0;
   struct String ini;
   char cbuf[MAXCHAR];
   int ibuf;
   npar[0] = -DBL_MAX;
   npar[1] = -DBL_MAX;
   npar[2] = -DBL_MAX;
   ini.c = (int*)calloc(String_SetLength(&ini, File_NameLength(NULL, -1, -1,
      -1, String_AsChars(&project, cbuf), -1, -1, "ini")),sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &ini);
   if ((def = fopen(String_AsChars(&ini, cbuf), "r")) == NULL) return -7;
   free(ini.c);
   while ((err = fscanf(def, "%s", keyword)) == 1)
   {
      if (keyword[0] == '#') while (fgetc(def) != '\n');
      else if (strcmp(keyword, "debug") == 0)
      {
         err = fscanf(def, "%i", &debug);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "DFT") == 0)
      {
         mode = 1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "Lomb") == 0)
      {
         mode = 2;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "col:time") == 0)
      {
         err = fscanf(def, "%i", &t);
         if (--t < 0) return 8;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "col:obs") == 0)
      {
         err = fscanf(def, "%i", &x);
         if (--x < 0) return 9;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "col:weights") == 0)
      {
         err = fscanf(def, "%i%lf", &wraw[wi], &wx[wi]);
         if (--wraw[wi++] < 0) return 10;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "col:ssid") == 0)
      {
         err = fscanf(def, "%i", &ssraw[ssi]);
         if (--ssraw[ssi++] < 0) return 11;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "lfreq") == 0)
      {
         err = fscanf(def, "%lf", &lf);
         if (lf < 0) return 12;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "ufreq") == 0)
      {
         err = fscanf(def, "%lf", &uf);
         if (uf <= 0) return 13;
         if ((ny >= 0) && (ny <= 1))
         {
            if (ny != .5) Log_ErrorMessage(14);
            ny = -1;
         }
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "nycoef") == 0)
      {
         err = fscanf(def, "%lf", &ny);
         if ((ny < 0) || (ny > 1)) return 15;
         if (uf > 0)
         {
            Log_ErrorMessage(16);
            uf = 0;
         }
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "nyscan") == 0)
      {
         nyscan = 1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "freqspacing") == 0)
      {
         err = fscanf(def, "%lf", &fs);
         if (fs <= 0) return 17;
         if (os > 0)
         {
            if (os != 20) Log_ErrorMessage(18);
            os = 0;
         }
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "osratio") == 0)
      {
         err = fscanf(def, "%lf", &os);
         if (os <= 0) return 19;
         if (fs > 0)
         {
            Log_ErrorMessage(20);
            fs = 0;
         }
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "siglimit") == 0)
      {
         err = fscanf(def, "%lf", &sl);
         if (sl < 0) return 21;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "csiglimit") == 0)
      {
         err = fscanf(def, "%lf", &csl);
         if (csl <= 0) return 22;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "iterations") == 0)
      {
         err = fscanf(def, "%i", &it);
         if (it <= 0) return 23;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "win") == 0)
      {
         makewin = 1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "preview:siglimit") == 0)
      {
         err = fscanf(def, "%lf", &psl);
         if (psl <= 0) return 24;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phdist:phases") == 0)
      {
         err = fscanf(def, "%i", &phdistph);
         if (phdistph < 0) return 25;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phdist:fill") == 0)
      {
         err = fscanf(def, "%lf", &phdistfill);
         if (phdistph < 0) phdistph = PHDISTPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phdist:cyl") == 0)
      {
         phdistcoord = 0;
         if (phdistph < 0) phdistph = PHDISTPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phdist:cart") == 0)
      {
         phdistcoord = 1;
         if (phdistph < 0) phdistph = PHDISTPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phdist:colmodel:lin") == 0)
      {
         phdistcolmodel = 0;
         if (phdistph < 0) phdistph = PHDISTPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phdist:colmodel:rank") == 0)
      {
         phdistcolmodel = 1;
         if (phdistph < 0) phdistph = PHDISTPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sock:phases") == 0)
      {
         err = fscanf(def, "%i", &sockph);
         if (sockph < 0) return 25;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sock:fill") == 0)
      {
         err = fscanf(def, "%lf", &sockfill);
         if (sockph < 0) sockph = SOCKPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sock:cyl") == 0)
      {
         sockcoord = 0;
         if (sockph < 0) sockph = SOCKPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sock:cart") == 0)
      {
         sockcoord = 1;
         if (sockph < 0) sockph = SOCKPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sock:colmodel:lin") == 0)
      {
         sockcolmodel = 0;
         if (sockph < 0) sockph = SOCKPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sock:colmodel:rank") == 0)
      {
         sockcolmodel = 1;
         if (sockph < 0) sockph = SOCKPH;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "profile") == 0)
      {
         makeprof = 1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "spectra") == 0)
      {
         err = fscanf(def, "%i%i", &nsp, &stsp);
         if (nsp < 0) nsp = (int)pow(10, ITDIGITS);
         if (stsp <= 0) return 139;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "residuals") == 0)
      {
         err = fscanf(def, "%i%i", &nres, &stres);
         if (nres < 0) nres = (int)pow(10, ITDIGITS) - 1;
         if (stres <= 0) return 140;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "results") == 0)
      {
         err = fscanf(def, "%i%i", &nresults, &stresults);
         if (nresults < 0) nresults = (int)pow(10, ITDIGITS) - 1;
         if (stresults <= 0) return 141;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phd") == 0)
      {
         err = fscanf(def, "%i%i", &nph, &stph);
         if (nph < 0) nph = (int)pow(10, ITDIGITS) - 1;
         if (stph <= 0) return 141;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "correlograms") == 0)
      {
         err = fscanf(def, "%i%i%i", &co, &nc, &stc);
         if (co < 0) return 28;
         if (nc < 0) nc = (int)pow(10, ITDIGITS);
         if (stc <= 0) return 133;
         while (fgetc(def) != '\n');
      }         
      else if (strcmp(keyword, "mstracks") == 0)
      {
         err = fscanf(def, "%i%i", &nmst, &stmst);
         if (nmst < 0) nmst = (int)pow(10, ITDIGITS) - 1;
         if (stmst <= 0) return 144;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "msprofs") == 0)
      {
         err = fscanf(def, "%i%i%i", &msp, &nmsp, &stmsp);
         if (nmsp < 0) nmsp = (int)pow(10, ITDIGITS) - 1;
         if (stmsp <= 0) return 189;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "pds") == 0)
      {
         err = fscanf(def, "%i%lf%i%i", &pdb, &pdw, &npd, &stpd);
         if (pdb < 0) return 149;
         if ((pdw < 0) || (pdw >= 1)) return 150;
         if (npd < 0) npd = (int)pow(10, ITDIGITS) - 1;
         if (stpd <= 0) return 143;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "pds:period") == 0)
      {
         err = fscanf(def, "%lf%lf%i", &pdlp, &pdup, &pdnp);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:range") == 0)
      {
         err = fscanf(def, "%lf", &trwidth);
         if (trwidth <= 0) return 29;
         if (trmode < 0) trmode = 0;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:step") == 0)
      {
         err = fscanf(def, "%lf", &trstep);
         if (trstep <= 0) return 30;
         if (trmode < 0) trmode = 0;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:none") == 0)
      {
         trmode = 0;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:ipow") == 0)
      {
         trmode = 1;
         err = fscanf(def, "%lf", &trpar);
         Log_Double("smoothing parameter", trpar);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:gauss") == 0)
      {
         trmode = 2;
         err = fscanf(def, "%lf", &trpar);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:exp") == 0)
      {
         trmode = 3;
         err = fscanf(def, "%lf", &trpar);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:cos") == 0)
      {
         trmode = 4;
         err = fscanf(def, "%lf", &trpar);
         err = fscanf(def, "%lf", &trph);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:cosp") == 0)
      {
         trmode = 5;
         err = fscanf(def, "%lf", &trpar);
         err = fscanf(def, "%lf", &trph);
         err = fscanf(def, "%lf", &trexp);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "timeres:w:damp") == 0)
      {
         trmode = 6;
         err = fscanf(def, "%lf", &trpar);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sim:off") == 0)
      {
         simmode = -1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sim:replace") == 0)
      {
         simmode = 0;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "sim:add") == 0)
      {
         simmode = 1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "multifile") == 0)
      {
         err = fscanf(def, "%i", &mfl);
         if (mfl < 0) mfl = INT_MAX;
         else mfl++;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "mfstart") == 0)
      {
         err = fscanf(def, "%i", &mfs);
         if (mfs < 0) return 120;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "antialc:par") == 0)
      {
         err = fscanf(def, "%lf", &aap);
         if ((aap < 0) || (aap > 1)) return 31;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "antialc:adopt") == 0)
      {
         err = fscanf(def, "%i", &aaa);
         if (aaa < 1) return 32;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "antialc:depth") == 0)
      {
         err = fscanf(def, "%i", &aad);
         if (aad < 1) return 33;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "antialc:siglimit") == 0)
      {
         err = fscanf(def, "%lf", &aasl);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "antialc:candidates") == 0)
      {
         err = fscanf(def, "%i", &aan);
         if (aan <= 0) return 112;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "diff:off") == 0)
      {
         dmode = 0;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "diff:comp") == 0)
      {
         dmode = 1;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "diff:compalign") == 0)
      {
         dmode = 2;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "harmonics") == 0)
      {
         err = fscanf(def, "%i", &oharm);
         if (oharm < 0) return 135;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "multisine:newton") == 0)
      {
         msmode = 0;
         err = fscanf(def, "%lf%lf%lf", &msacc, &mssens, &msrms);
         if (msacc <= 0) return 147;
         if (mssens < 0) return 136;
         if (mssens > 1) Log_ErrorMessage(137);
         if (msrms >= 1) return 190;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "multisine:dwa") == 0)
      {
         msmode = 1;
         err = fscanf(def, "%lf%lf", &msacc, &mssens);
         if (msacc <= 0) return 147;
         if (mssens < 0) return 136;
         if (mssens > 1) Log_ErrorMessage(137);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "multisine:ddwa") == 0)
      {
         msmode = 2;
         err = fscanf(def, "%lf%lf", &msacc, &mssens);
         if (msacc <= 0) return 147;
         if (mssens < 0) return 136;
         if (mssens > 1) Log_ErrorMessage(137);
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "phd:fit") == 0)
      {
         err = fscanf(def, "%i", &pf);
         if (pf == 1) return 154;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "zm:off") == 0)
      {
         zeromean = 0;
         while (fgetc(def) != '\n');
      }
      else if (strcmp(keyword, "zm:on") == 0)
      {
         zeromean = 1;
         while (fgetc(def) != '\n');
      }
      else if ((strcmp(keyword, "sim:serial") != 0) &&
         (strcmp(keyword, "sim:temporal") != 0) &&
         (strcmp(keyword, "sim:signal") != 0) &&
         (strcmp(keyword, "sim:poly") != 0) &&
         (strcmp(keyword, "sim:exp") != 0) &&
         (strcmp(keyword, "sim:zeromean") != 0) &&
         (strcmp(keyword, "sim:rndsteps") != 0) &&
         (strcmp(keyword, "phdist:colour") != 0) &&
         (strcmp(keyword, "sock:colour") != 0) &&
         (strcmp(keyword, "deftype") != 0) &&
         (strcmp(keyword, "target") != 0) &&
         (strcmp(keyword, "comp") != 0) &&
         (strcmp(keyword, "skip") != 0) &&
         (strcmp(keyword, "noisemodel:poly") != 0) &&
         (strcmp(keyword, "noisemodel:exp") != 0) &&
         (strcmp(keyword, "noisemodel:gauss") != 0))
      {
         Log_ErrorMessage(35);
         printf("%s\n\n", keyword);
         while (fgetc(def) != '\n');
      }
      else while (fgetc(def) != '\n');
   }
   fclose(def);
   return 0;
}



int IniFile_PhDistColours(
   )

{
   struct String fname;
   char cbuf[MAXCHAR];
   int result;
   FILE* F;
   fname.c = (int*)calloc(String_SetLength(&fname,
      File_NameLength(NULL, -1, -1, -1, String_AsChars(&project, cbuf), -1, -1,
      "ini")), sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 94;
   free(fname.c);
   fname.c = (int*)calloc(String_SetLength(&fname, 13), sizeof(int));
   String_SetChars(&fname, "phdist:colour", 0);
   if ((result = DataFile_CountStartString(F, &fname)) < 0) return 95;
   free(fname.c);
   fclose(F);
   return result;
}



int IniFile_SockColours(
   )

{
   struct String fname;
   char cbuf[MAXCHAR];
   int result;
   FILE* F;
   fname.c = (int*)calloc(String_SetLength(&fname,
      File_NameLength(NULL, -1, -1, -1, String_AsChars(&project, cbuf), -1, -1,
      "ini")), sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 77;
   free(fname.c);
   fname.c = (int*)calloc(String_SetLength(&fname, 11), sizeof(int));
   String_SetChars(&fname, "sock:colour", 0);
   if ((result = DataFile_CountStartString(F, &fname)) < 0) return 78;
   free(fname.c);
   fclose(F);
   return result;
}



int IniFile_SSCols(
   )

{
   struct String fname;
   char cbuf[MAXCHAR];
   int result;
   FILE* F;
   if (!quiet)
   {
      printf("loading .ini file\n");
      fflush(stdout);
   }
   fname.c = (int*)calloc(String_SetLength(&fname,
      File_NameLength(NULL, -1, -1, -1, String_AsChars(&project, cbuf), -1, -1,
      "ini")), sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return -3;
   free(fname.c);
   fname.c = (int*)calloc(String_SetLength(&fname, 8), sizeof(int));
   String_SetChars(&fname, "col:ssid", 0);
   if ((result = DataFile_CountStartString(F, &fname)) < 0) return -4;
   free(fname.c);
   fclose(F);
   return result;
}



int IniFile_WCols(
   )

{
   struct String fname;
   char cbuf[MAXCHAR];
   int result;
   FILE* F;
   fname.c = (int*)calloc(String_SetLength(&fname,
      File_NameLength(NULL, -1, -1, -1, String_AsChars(&project, cbuf), -1, -1,
      "ini")), sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return -5;
   free(fname.c);
   fname.c = (int*)calloc(String_SetLength(&fname, 11), sizeof(int));
   String_SetChars(&fname, "col:weights", 0);
   if ((result = DataFile_CountStartString(F, &fname)) < 0) return -6;
   free(fname.c);
   fclose(F);
   return result;
}



int Log_Double(
   char* descr,
   double val
   )

{
   int i;
   printf("\r%s", descr);
   for (i = (int)strlen(descr); i < 36; i++) printf(" ");
   printf("%24.16lf\n", val);
   fflush(stdout);
   return (int)strlen(descr);
}



int Log_ErrorMessage(
   int errcode
   )

{
   switch (errcode)
   {
   case 1:
      printf("\nError:   CmdLine_Scan 001\n");
      printf("         Command line syntax.\n");
      printf("         Required syntax: SigSpec <project name> [q]\n\n");
      break;
   case 2:
      printf("\nError:   CmdLine_Scan 002\n");
      printf("         Project directory not found.\n\n");
      break;
   case 3:
      printf("\nWarning: IniFile_SSCols 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 4:
      printf("\nError:   IniFile_SSCols 002\n");
      printf("         Failed to count subset identifier column indices.\n\n");
      break;
   case 5:
      printf("\nWarning: IniFile_WCols 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 6:
      printf("\nError:   IniFile_WCols 002\n");
      printf("         Failed to count weights column indices.\n\n");
      break;
   case 7:
      printf("\nWarning: IniFile_LoadIni 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 8:
      printf("\nError:   IniFile_LoadIni 002\n");
      printf("         Illegal time column specifier, value > 0 required.\n\n");
      break;
   case 9:
      printf("\nError:   IniFile_LoadIni 003\n");
      printf("         Illegal observable column specifier,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 10:
      printf("\nError:   IniFile_LoadIni 004\n");
      printf("         Illegal weights column specifier,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 11:
      printf("\nError:   IniFile_LoadIni 005\n");
      printf("         Illegal subset identifier column specifier,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 12:
      printf("\nError:   IniFile_LoadIni 006\n");
      printf("         Illegal lower frequency limit,\n");
      printf("         value >= 0 required.\n\n");
      break;
   case 13:
      printf("\nError:   IniFile_LoadIni 007\n");
      printf("         Illegal upper frequency limit,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 14:
      printf("\nWarning: IniFile_LoadIni 008\n");
      printf("         Upper frequency limit overriding previously\n");
      printf("         specified Nyquist coefficient.\n\n");
      break;
   case 15:
      printf("\nError:   IniFile_LoadIni 009\n");
      printf("         Illegal Nyquist coefficient,\n");
      printf("         value >= 0 and <= 1 required.\n\n");
      break;
   case 16:
      printf("\nWarning: IniFile_LoadIni 010\n");
      printf("         Nyquist coefficient overriding previously\n");
      printf("         specified upper frequency limit.\n\n");
      break;
   case 17:
      printf("\nError:   IniFile_LoadIni 011\n");
      printf("         Illegal frequency spacing,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 18:
      printf("\nWarning: IniFile_LoadIni 012\n");
      printf("         Frequency spacing overriding previously\n");
      printf("         specified oversampling ratio.\n\n");
      break;
   case 19:
      printf("\nError:   IniFile_LoadIni 013\n");
      printf("         Illegal oversampling ratio,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 20:
      printf("\nWarning: IniFile_LoadIni 014\n");
      printf("         Oversampling ratio overriding previously");
      printf("         specified frequency spacing.\n\n");
      break;
   case 21:
      printf("\nError:   IniFile_LoadIni 015\n");
      printf("         Illegal spectral significance limit,\n");
      printf("         value >= 0 required.\n\n");
      break;
   case 22:
      printf("\nError:   IniFile_LoadIni 016\n");
      printf("         Illegal cumulative spectral significance limit,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 23:
      printf("\nError:   IniFile_LoadIni 017\n");
      printf("         Illegal number of iterations,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 24:
      printf("\nError:   IniFile_LoadIni 018\n");
      printf("         Illegal spectral significance limit for preview,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 25:
      printf("\nError:   IniFile_LoadIni 019\n");
      printf("         Illegal number of phases for Sock Diagram,\n");
      printf("         value >= 0 required.\n\n");
      break;
   case 26:
      printf("\nError:   IniFile_LoadIni 020\n");
      printf("         Illegal number of spectrum files,\n");
      printf("         value >= 0 required.\n\n");
      break;
   case 27:
      printf("\nError:   IniFile_LoadIni 021\n");
      printf("         Illegal number of residuals files,\n");
      printf("         value >= 0 required.\n\n");
      break;
   case 28:
      printf("\nError:   IniFile_LoadIni 022\n");
      printf("         Illegal correlogram order, ");
      printf("value >= 0 required.\n\n");
      break;
   case 29:
      printf("\nError:   IniFile_LoadIni 023\n");
      printf("         Illegal range for time-resolved analysis,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 30:
      printf("\nError:   IniFile_LoadIni 024\n");
      printf("         Illegal step width for time-resolved analysis,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 31:
      printf("\nError:   IniFile_LoadIni 025\n");
      printf("         Illegal AntiAlC parameter,\n");
      printf("         value >= 0 and <= 1 required.\n\n");
      break;
   case 32:
      printf("\nError:   IniFile_LoadIni 026\n");
      printf("         Illegal AntiAlC iterations to adopt,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 33:
      printf("\nError:   IniFile_LoadIni 027\n");
      printf("         Illegal AntiAlC computation depth,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 34:
      printf("\nError:   IniFile_LoadIni 028\n");
      printf("         Illegal AntiAlC spectral significance limit,\n");
      printf("         value > 0 required.\n\n");
      break;
   case 35:
      printf("\nWarning: IniFile_LoadIni 029\n");
      printf("         Unknown keyword ");
      break;
   case 36:
      printf("\nError:   MultiFile_Count 001\n");
      printf("         No time series input file for MultiFile mode found.");
      printf("\n\n");
      break;
   case 37:
      printf("Warning: MultiFile_Count 002\n");
      printf("         MultiFile limit exceeds number of available\n");
      printf("         time series input files, limit re-adjusted to ");
      break;
   case 38:
      printf("\nError:   TS_Count 001\n");
      printf("         Could not open time series input file.\n\n");
      break;
   case 39:
      printf("\nError:   TS_Count 002\n");
      printf("         Column count error.\n\n");
      break;
   case 40:
      printf("\nError:   TS_Check 001\n");
      printf("         Could not open time series input file.\n\n");
      break;
   case 41:
      printf("\nError:   TS_Check 002\n");
      printf("         Time column format error.\n\n");
      break;
   case 42:
      printf("\nError:   TS_Check 003\n");
      printf("         Observable column format error.\n\n");
      break;
   case 43:
      printf("\nError:   TS_Check 004\n");
      printf("         Weights column format error.\n\n");
      break;
   case 44:
      printf("\nError:   TS_StrLen 001\n");
      printf("         Failed to determine length of string in\n");
      printf("         time series input file.\n\n");
      break;
   case 45:
      printf("\nError:   TS_Read 001\n");
      printf("         Failed to open time series input file.\n\n");
      break;
   case 46:
      printf("\nError:   TS_Read 002\n");
      printf("         Failed to read time series input file.\n\n");
      break;
   case 47:
      printf("\nError:   TS_Sort 001\n");
      printf("         Failed to sort time series input file.\n\n");
      break;
   case 48:
      printf("\nError:   TS_Weights 001\n");
      printf("         Unable to normalise weights.\n\n");
      break;
   case 49:
      printf("\nError:   TS_CountSubsetEntries 001\n");
      printf("         Sum of subset entries does not match total number\n");
      printf("         of time series data points.\n\n");
      break;
   case 50:
      printf("\nError:   TimeRes_Prepare 001\n");
      printf("         Illegal number of intervals.\n\n");
      break;
   case 51:
      printf("\nError:   TimeRes_Prepare 002\n");
      printf("         Illegal step width.\n\n");
      break;
   case 52:
      printf("\nError:   Sim_Run 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 53:
      printf("\nError:   Sim_Run 002\n");
      printf("         Simulator error in temporal correlation generator.\n\n");
      break;
   case 54:
      printf("\nError:   Sim_Run 003\n");
      printf("         Simulator error in serial correlation generator.\n\n");
      break;
   case 55:
      printf("\nError:   Sim_Run 004\n");
      printf("         Simulator error in signal generator.\n\n");
      break;
   case 56:
      printf("\nError:   Sim_Run 005\n");
      printf("         Simulator error in polynomial trend generator.\n\n");
      break;
   case 57:
      printf("\nError:   Sim_Run 006\n");
      printf("         Simulator error in exponential trend generator.\n\n");
      break;
   case 58:
      printf("\nError:   Sim_Run 007\n");
      printf("         Simulator error in random steps generator.\n\n");
      break;
   case 59:
      printf("\nError:   Sim_Run 009\n");
      printf("         Failed to write synthetic time series.\n\n");
      break;
   case 60:
      printf("\nWarning: IniFile_Cind 002\n");
      printf("         Illegal MultiFile index. Entry ignored.\n\n");
      break;
   case 61:
      printf("\nWarning: IniFile_Cind 003\n");
      printf("         Illegal default type. Using \"target\" instead.\n\n");
      break;
   case 62:
      printf("\nError:   SigSpec_Freqs 001\n");
      printf("         Upper frequency limit below lower frequency limit.\n\n");
      break;
   case 63:
      printf("\nError:   SigSpec_Freqs 002\n");
      printf("         Illegal number of frequencies.\n\n");
      break;
   case 64:
      printf("\nWarning: TimeRes_Count 001\n");
      printf("         Number of data points in time interval <= 0. Skip.\n\n");
      break;
   case 65:
      printf("\nError:   Log_Profile 001\n");
      printf("         Could not open file assign.log for writing.\n\n");
      break;
   case 66:
      printf("\nError:   Noise_CountFiles 001\n");
      printf("         Missing noise file(s).\n\n");
      break;
   case 67:
      printf("\nWarning: Noise_GetFile 001\n");
      printf("         Parameter nsmooth = 0 not allowed for noise files.\n");
      printf("         Re-adjusting nsmooth.\n\n");
      break;
   case 68:
      printf("\nError:   Noise_GetFile 002\n");
      printf("         Missing noise file.\n\n");
      break;
   case 69:
      printf("\nError:   Win_Generate 001\n");
      printf("         Could not open file for writing spectral window.\n\n");
      break;
   case 70:
      printf("\nError:   Profile_Generate 001\n");
      printf("         Could not open file for writing sampling profile.\n\n");
      break;
   case 71:
      printf("\nError:   Sock_Generate 001\n");
      printf("         Failed to determine coordinate system.\n\n");
      break;
   case 72:
      printf("\nError:   SigSpec_MaxSig 001\n");
      printf("         Significance calculation for precursor failed.\n\n");
      break;
   case 73:
      printf("\nError:   SigSpec_MaxSig 002\n");
      printf("         Significance calculation for current frequency ");
      printf("failed.\n\n");
      break;
   case 74:
      printf("\nError:   SigSpec_MaxSig 003\n");
      printf("         Significance calculation for successor failed.\n\n");
      break;
   case 75:
      printf("\nError:   Preview_Generate 001\n");
      printf("         Could not open file for writing preview.\n\n");
      break;
   case 76:
      printf("\nError:   IniFile_LoadIni 030\n");
      printf("         Illegal number of data points on sock colour bar.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 77:
      printf("\nError:   IniFile_SockColours 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 78:
      printf("\nError:   IniFile_SockColours 002\n");
      printf("         Failed to count sock colour table entries.\n\n");
      break;
   case 79:
      printf("\nError:   Sock_Cylindrical 001\n");
      printf("         Unable to open sock diagram file for writing.\n\n");
      break;
   case 80:
      printf("\nError:   Sock_Cartesian 001\n");
      printf("         Unable to open sock diagram file for writing.\n\n");
      break;
   case 81:
      printf("\nError:   Sock_Colours 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 82:
      printf("\nError:   Sock_Colours 002\n");
      printf("         Incomplete colour table entry.\n");
      printf("         3 values for RGB plus 1 for stretch factor required.\n");
      printf("\n");
      break;
   case 83:
      printf("\nError:   Sock_Colours 003\n");
      printf("         Illegal RGB entry: negative R level.\n\n");
      break;
   case 84:
      printf("\nError:   Sock_Colours 004\n");
      printf("         Illegal RGB entry: negative G level.\n\n");
      break;
   case 85:
      printf("\nError:   Sock_Colours 005\n");
      printf("         Illegal RGB entry: negative B level.\n\n");
      break;
   case 86:
      printf("\nError:   Sock_Colours 006\n");
      printf("         Illegal RGB entry: ");
      printf("scaling factor >= 0 and <= 1 required.\n\n");
      break;
   case 87:
      printf("\nError:   Sock_Colours 007\n");
      printf("         Unable to open sock diagram file for reading.\n\n");
      break;
   case 88:
      printf("\nError:   Sock_Colours 008\n");
      printf("         Invalid row count in sock diagram.\n\n");
      break;
   case 89:
      printf("\nError:   Sock_Colours 009\n");
      printf("         Error reading sock diagram.\n\n");
      break;
   case 90:
      printf("\nError:   Sock_Colours 010\n");
      printf("         Unable to determine coordinate system of sock diagram.\n\n");
      break;
   case 91:
      printf("\nError:   Sock_Colours 011\n");
      printf("         Rank statistics failed.\n\n");
      break;
   case 92:
      printf("\nError:   Sock_Colours 012\n");
      printf("         Unable to open sock diagram file for writing.\n\n");
      break;
   case 93:
      printf("\nError:   Sock_Colours 013\n");
      printf("         Failed to sort RGB colours.\n\n");
      break;
   case 94:
      printf("\nError:   IniFile_PhDistColours 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 95:
      printf("\nError:   IniFile_PhDistColours 002\n");
      printf("         Failed to count phase distribution colour table ");
      printf("entries.\n\n");
      break;
   case 96:
      printf("\nError:   PhDist_Cylindrical 001\n");
      printf("         Could not open file for writing phase distribution diagram.\n\n");
      break;
   case 97:
      printf("\nError:   PhDist_Cartesian 001\n");
      printf("         Could not open file for writing phase distribution diagram.\n\n");
      break;
   case 98:
      printf("\nError:   PhDist_Colours 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 99:
      printf("\nError:   PhDist_Colours 002\n");
      printf("         Incomplete colour table entry.\n");
      printf("         3 values for RGB plus 1 for stretch factor required.\n");
      printf("\n");
      break;
   case 100:
      printf("\nError:   PhDist_Colours 003\n");
      printf("         Illegal RGB entry: negative R level.\n\n");
      break;
   case 101:
      printf("\nError:   PhDist_Colours 004\n");
      printf("         Illegal RGB entry: negative G level.\n\n");
      fflush(stdout);
      break;
   case 102:
      printf("\nError:   PhDist_Colours 005\n");
      printf("         Illegal RGB entry: negative B level.\n\n");
      break;
   case 103:
      printf("\nError:   PhDist_Colours 006\n");
      printf("         Illegal RGB entry: ");
      printf("scaling factor >= 0 and <= 1 required.\n\n");
      break;
   case 104:
      printf("\nError:   PhDist_Colours 007\n");
      printf("         Unable to open phase distribution diagram file for reading.\n\n");
      break;
   case 105:
      printf("\nError:   PhDist_Colours 008\n");
      printf("         Invalid row count in phase distribution diagram.\n\n");
      break;
   case 106:
      printf("\nError:   PhDist_Colours 009\n");
      printf("         Error reading phase distribution diagram.\n\n");
      break;
   case 107:
      printf("\nError:   PhDist_Colours 010\n");
      printf("         Unable to determine coordinate system of phase distribution diagram.\n\n");
      break;
   case 108:
      printf("\nError:   PhDist_Colours 011\n");
      printf("         Rank statistics failed.\n\n");
      break;
   case 109:
      printf("\nError:   PhDist_Colours 012\n");
      printf("         Unable to open phase distribution diagram file for");
      printf("writing.\n\n");
      break;
   case 110:
      printf("\nError:   PhDist_Colours 013\n");
      printf("         Failed to sort RGB colours.\n\n");
      break;
   case 111:
      printf("\nError:   Correlogram_Generate 001\n");
      printf("         Could not open correlogram file for writing.\n\n");
      break;
   case 112:
      printf("\nError:   IniFile_LoadIni 031\n");
      printf("         Illegal maximum number of AntiAlC candidates.\n\n");
      break;
   case 113:
      printf("\nError:   AntiAlC_Cascade 001\n");
      printf("         AntiAlC candidate selection failed.\n\n");
      break;
   case 114:
      printf("\nWarning: IniFile_Cind 001\n");
      printf("         Failed to open .ini file.\n\n");
      break;
   case 115:
      printf("\nError:   TimeRes_Extract 001\n");
      printf("         Unable to open weights file for writing.\n\n");
      break;
   case 116:
      printf("\nError:   Spec_Write 001\n");
      printf("         Unable to open significance spectrum file for writing.");
      printf("\n\n");
      break;
   case 117:
      printf("\nError:   TS_Write 001\n");
      printf("         Unable to open time series file for writing.\n\n");
      break;
   case 118:
      printf("\nError:   TS_Write 002\n");
      printf("         Error writing time series.\n\n");
      break;
   case 119:
      printf("\nError:   TimeRes_Extract2C 001\n");
      printf("         Unable to open weights file for writing.\n\n");
      break;
   case 120:
      printf("\nError:   IniFile_LoadIni 032\n");
      printf("         Illegal MultiFile start index.\n");
      printf("         Value >= 0 required.\n\n");
      break;
   case 121:
      printf("\nError:   IniFile_CheckIni 001\n");
      printf("         MultiFile start index exceeds number of files.\n\n");
      break;
   case 122:
      printf("\nError:   IniFile_CheckIni 002\n");
      printf("         Time and observable index are identical.\n\n");
      break;
   case 123:
      printf("\nError:   IniFile_CheckIni 003\n");
      printf("         Weights column index is time column index.\n\n");
      break;
   case 124:
      printf("\nError:   IniFile_CheckIni 004\n");
      printf("         Weights column index is observable column index.\n\n");
      break;
   case 125:
      printf("\nError:   IniFile_CheckIni 005\n");
      printf("         Illegal combination of interval/step width in\n");
      printf("         time-resolved mode.\n\n");
      break;
   case 126:
      printf("\nError:   IniFile_CheckIni 006\n");
      printf("         Illegal combination of mode id and interval width in\n");
      printf("         time-resolved mode.\n\n");
      break;
   case 127:
      printf("\nError:   IniFile_CheckIni 007\n");
      printf("         Illegal combination of mode id and step width in\n");
      printf("         time-resolved mode.\n\n");
      break;
   case 128:
      printf("\nError:   IniFile_CheckIni 008\n");
      printf("         Illegal value for parameter 2 of timeres:w:cos.\n\n");
      break;
   case 129:
      printf("\nError:   IniFile_CheckIni 009\n");
      printf("         Illegal value for parameter 2 of timeres:w:cosp.\n\n");
      break;
   case 130:
      printf("\nError:   IniFile_CheckIni 010\n");
      printf("         Illegal value for parameter 3 of timeres:w:cosp.\n\n");
      break;
   case 131:
      printf("\nWarning: IniFile_CheckIni 011\n");
      printf("         Illegal value for AntiAlC depth parameter.\n");
      break;
   case 132:
      printf("\nWarning: IniFile_CheckIni 012\n");
      printf("         AntiAlC significance limit below overall\n");
      printf("         significance limit\n");
      break;
   case 133:
      printf("\nError:   IniFile_LoadIni 033\n");
      printf("         Illegal step width for correlogram output files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 134:
      printf("\nError:   TS_AssignProfiles 001\n");
      printf("         Could not open profile assignment file for writing.\n");
      break;
   case 135:
      printf("\nError:   IniFile_LoadIni 034\n");
      printf("         Illegal harmonic order.\n");
      printf("         Value >= 0 required.\n\n");
      break;
   case 136:
      printf("\nError:   IniFile_LoadIni 035\n");
      printf("         Illegal MultiSine sensitivity.\n");
      printf("         Value >= 0 required.\n\n");
      break;
   case 137:
      printf("\nWarning: IniFile_LoadIni 036\n");
      printf("         MultiSine sensitivity > 1.\n");
      printf("         Does this make sense?\n\n");
      break;
   case 138:
      printf("\nError:   IniFile_LoadIni 037\n");
      printf("         Illegal number of result files.\n");
      printf("         Value >= 0 required.\n\n");
      break;
   case 139:
      printf("\nError:   IniFile_LoadIni 038\n");
      printf("         Illegal step width for spectrum output files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 140:
      printf("\nError:   IniFile_LoadIni 039\n");
      printf("         Illegal step width for residual output files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 141:
      printf("\nError:   IniFile_LoadIni 040\n");
      printf("         Illegal step width for result files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 142:
      printf("\nError:   MultiSine_WriteTracks 001\n");
      printf("         Failed to open MultiSine track output file.\n\n");
      break;
   case 143:
      printf("\nError:   IniFile_LoadIni 041\n");
      printf("         Illegal step width for phase dispersion files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 144:
      printf("\nError:   IniFile_LoadIni 042\n");
      printf("         Illegal step width for MultiSine track output files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 145:
      printf("\nError:   Harmonics_Write 001\n");
      printf("         Unable to open harmonics output file for writing.\n\n");
      break;
   case 146:
      printf("\nError:   Result_Write 001\n");
      printf("         Unable to open result file for writing.\n\n");
      break;
   case 147:
      printf("\nError:   IniFile_LoadIni 043\n");
      printf("         Illegal MultiSine accuracy.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 148:
      printf("\nError:   AntiAlC_Cascade 030\n");
      printf("         Memory allocation for comparison result failed.\n\n");
      break;
   case 149:
      printf("\nError:   IniFile_LoadIni 045\n");
      printf("         Illegal number of bins for phase dispersion.\n");
      printf("         Value >= 0 required.\n\n");
      break;
   case 150:
      printf("\nError:   IniFile_LoadIni 046\n");
      printf("         Illegal bin width for phase dispersion.\n");
      printf("         Value >= 0 and < 1 required.\n\n");
      break;
   case 151:
      printf("\nError:   IniFile_LoadIni 044\n");
      printf("         Illegal step width for phase diagram files.\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 152:
      printf("\nError:   PhaseDiagram_Generate 001\n");
      printf("         Unable to open phase diagram file for writing.\n\n");
      break;
   case 153:
      printf("\nError:   PhaseDiagram_Generate 002\n");
      printf("         Unable to open phase fit file for writing.\n\n");
      break;
   case 154:
      printf("\nError:   IniFile_LoadIni 048\n");
      printf("         Illegal parameter for phase fit.\n\n");
      printf("         Value < 0 or > 1 required.\n\n");
      break;
   case 155:
      printf("\nError:   Nyquist_Scan 001\n");
      printf("         Unable to open nyquist coefficients file for writing.");
      printf("\n\n");
      break;
   case 156:
      printf("\nWarning: Rnd_Write 001\n");
      printf("         Unable to open file 'rnd.log' for writing.");
      printf("\n\n");
      break;
   case 157:
      printf("\nError:   AntiAlC_Cascade 002\n");
      printf("         Memory allocation for levels of residual time series ");
      printf("failed.\n\n");
      break;
   case 158:
      printf("\nError:   AntiAlC_Cascade 003\n");
      printf("         Memory allocation for residual comparison time series ");
      printf("failed.\n\n");
      break;
   case 159:
      printf("\nError:   AntiAlC_Cascade 004\n");
      printf("         Memory allocation for residual spectra failed.\n\n");
      break;
   case 160:
      printf("\nError:   AntiAlC_Cascade 005\n");
      printf("         Memory allocation for candidate levels failed.\n\n");
      break;
   case 161:
      printf("\nError:   AntiAlC_Cascade 006\n");
      printf("         Memory allocation for current result failed.\n\n");
      break;
   case 162:
      printf("\nError:   AntiAlC_Cascade 007\n");
      printf("         Memory allocation for candidate indices failed.\n\n");
      break;
   case 163:
      printf("\nError:   AntiAlC_Cascade 008\n");
      printf("         Memory allocation for numbers of candidates ");
      printf("failed.\n\n");
      break;
   case 164:
      printf("\nError:   AntiAlC_Cascade 009\n");
      printf("         Memory allocation for best result failed.\n\n");
      break;
   case 165:
      printf("\nError:   AntiAlC_Cascade 010\n");
      printf("         Memory allocation for Fourier cos coefficients ");
      printf("failed.\n\n");
      break;
   case 166:
      printf("\nError:   AntiAlC_Cascade 011\n");
      printf("         Memory allocation for Fourier sin coefficients ");
      printf("failed.\n\n");
      break;
   case 167:
      printf("\nError:   AntiAlC_Cascade 012\n");
      printf("         Memory allocation for sigs in spectra failed.\n\n");
      break;
   case 168:
      printf("\nError:   AntiAlC_Cascade 013\n");
      printf("         Memory allocation for Fourier cos coefficients ");
      printf("in comparison spectra failed.\n\n");
      break;
   case 169:
      printf("\nError:   AntiAlC_Cascade 014\n");
      printf("         Memory allocation for Fourier sin coefficients ");
      printf("in comparison spectra failed.\n\n");
      break;
   case 170:
      printf("\nError:   AntiAlC_Cascade 015\n");
      printf("         Memory allocation for candidates failed.\n\n");
      break;
   case 171:
      printf("\nError:   AntiAlC_Cascade 016\n");
      printf("         Memory allocation for candidate harmonics failed.\n\n");
      break;
   case 172:
      printf("\nError:   AntiAlC_Cascade 017\n");
      printf("         Memory allocation for observable column in residual ");
      printf("time series failed.\n\n");
      break;
   case 173:
      printf("\nError:   AntiAlC_Cascade 018\n");
      printf("         Memory allocation for observables in residual time ");
      printf("series failed.\n\n");
      break;
   case 174:
      printf("\nError:   AntiAlC_Cascade 019\n");
      printf("         Memory allocation for column format string in ");
      printf("residual time series\n         failed.\n\n");
      break;
   case 175:
      printf("\nError:   AntiAlC_Cascade 020\n");
      printf("         Memory allocation for column widths in residual time ");
      printf("series failed.\n\n");
      break;
   case 176:
      printf("\nError:   AntiAlC_Cascade 021\n");
      printf("         Memory allocation for numbers of subset entries in ");
      printf("residual time series\n         failed.\n\n");
      break;
   case 177:
      printf("\nError:   AntiAlC_Cascade 022\n");
      printf("         Memory allocation for residual comparison time series ");
      printf("failed.\n\n");
      break;
   case 178:
      printf("\nError:   AntiAlC_Cascade 023\n");
      printf("         Memory allocation for observable column in residual ");
      printf("comparison time series\n         failed.\n\n");
      break;
   case 179:
      printf("\nError:   AntiAlC_Cascade 024\n");
      printf("         Memory allocation for observables in residual ");
      printf("comparison time series\n         failed.\n\n");
      break;
   case 180:
      printf("\nError:   AntiAlC_Cascade 025\n");
      printf("         Memory allocation for column format string in ");
      printf("residual comparison\n         time series failed.\n\n");
      break;
   case 181:
      printf("\nError:   AntiAlC_Cascade 026\n");
      printf("         Memory allocation for column widths in residual ");
      printf("comparison time\n         series failed.\n\n");
      break;
   case 182:
      printf("\nError:   AntiAlC_Cascade 027\n");
      printf("         Memory allocation for harmonics in best result ");
      printf("failed.\n\n");
      break;
   case 183:
      printf("\nError:   AntiAlC_Cascade 028\n");
      printf("         Memory allocation for harmonics in current result ");
      printf("failed.\n\n");
      break;
   case 184:
      printf("\nError:   AntiAlC_Cascade 029\n");
      printf("         Memory allocation for harmonics in comparison result ");
      printf("failed.\n\n");
      break;
   case 185:
      printf("\nError:   AntiAlC_Candidates 001\n");
      printf("         Memory allocation for residual observable column ");
      printf("failed.\n\n");
      break;
   case 186:
      printf("\nError:   AntiAlC_Candidates 002\n");
      printf("         Memory allocation for residual observables failed.\n\n");
      break;
   case 187:
      printf("\nError:   AntiAlC_Candidates 003\n");
      printf("         Memory allocation for candidate harmonics failed.\n\n");
      break;
   case 188:
      printf("\nError:   AntiAlC_Sort 001\n");
      printf("         Memory allocation for re-ordered indices failed.\n\n");
      break;
   case 189:
      printf("\nError:   IniFile_LoadIni 049\n");
      printf("         Illegal step width for MultiSine profile files.\n\n");
      printf("         Value > 0 required.\n\n");
      break;
   case 190:
      printf("\nError:   IniFile_LoadIni 050\n");
      printf("         Illegal value for MultiSine rms error improvement.\n\n");
      printf("         Value < 1 required.\n\n");
      break;
   case 191:
      printf("\nError:   Sim_Run 008\n");
      printf("         Failed to open synthetic time series file for ");
      printf("writing.\n\n");
      break;
   default:
      printf("\nError:   Unknown (Code %i).\n\n", errcode);
      break;
   }
   fflush(stdout);
   return errcode;
}



int Log_Goodbye(
   )

{
   printf("\r                                                                ");
   printf("\n\nFinished.\n\n");
   printf("************************************************************\n\n");
   printf("Thank you for using SigSpec!\n");
   printf("Questions or comments?\n");
   printf("Please contact Piet Reegen (reegen@astro.univie.ac.at)\n");
   printf("Bye!\n\n");
   fflush(stdout);
   return 0;
}



int Log_Header(
   char* hdr
   )

{
   int i;
   printf("\n*** ");
   printf("%s ", hdr);
   for (i = (int)strlen(hdr) + 5; i < 60; i++) printf("*");
   printf("\n\n");
   fflush(stdout);
   return (int)strlen(hdr);
}



int Log_Int(
   char* descr,
   int val
   )

{
   int i;
   printf("\r%s", descr);
   for (i = (int)strlen(descr); i < 35; i++) printf(" ");
   printf("%8i\n", val);
   fflush(stdout);
   return (int)strlen(descr);
}



int Log_Percent(
   char* descr,
   double val1,
   double val2
   )

{
   int i;
   double perc;
   if ((perc = floor(100. * val1 / val2 + .5)) ==
      floor(100. * (val1 - 1) / val2 + .5)) return 0;
   printf("\r%s", descr);
   for (i = (int)strlen(descr); i < 56; i++) printf(" ");
   printf("%3.0lf%%", perc);
   fflush(stdout);
   return (int)strlen(descr);
}



int Log_Profile(
   struct TS* ts
   )

{
   int fi;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project, -1,
      -1, -1, "assign", -1, -1, "log")), sizeof(int));
   File_Name(&project, -1, -1, -1, "assign", -1, -1, "log", &fname);
   String_AsChars(&fname, cbuf);
   free(fname.c);
   if ((F = fopen(cbuf, "w")) == NULL) return 65;
   fprintf(F, "time series input file           ");
   fprintf(F, "profile and spectral window     \n\n");
   fname.c = (int*)calloc(String_SetLength(&fname, MFDIGITS),
      sizeof(int));
   for (fi = 0; fi < nmf; fi++) if (ts[fi].ct == 1)
   {
      NumFormat_FixDigits(mfs + fi, &fname);
      String_AsChars(&fname, cbuf);
      fprintf(F, "%16s ", cbuf);
      NumFormat_FixDigits(mfs + ts[fi].pi, &fname);
      String_AsChars(&fname, cbuf);
      fprintf(F, "%32s\n", cbuf);
   }
   free(fname.c);
   fflush(F);
   fclose(F);
   return 0;
}



int Log_TS(
   struct TS* ts
   )

{
   int si;
   printf("points %i, time base %lg, rms dev %lg\n", ts->r,
      ts->d[t][ts->r - 1] - ts->d[t][0],
      Stat_SDev(ts->d[x], ts->w, 0, ts->r - 1, 0, 0, &si));
   if (ts->nss > 1) for (si = 0; si < ts->nss; si++) if (!quiet)
      printf("subset %i of %i: %i entries\n", si + 1, ts->nss, ts->nsse[si]);
   fflush(stdout);
   return 0;
}



void Log_Welcome(
   )

{
   printf(" SSSSSS  ii          SSSSSS                          \n");
   printf("SS    SS            SS    SS                         \n");
   printf("SS       ii  gggg g SS       p pppp   eeeee   ccccc  \n");
   printf("SS       ii gg   gg SS       pp   pp ee   ee cc   cc \n");
   printf(" SSSSSS  ii gg   gg  SSSSSS  pp   pp ee   ee cc      \n");
   printf("      SS ii gg   gg       SS pp   pp eeeeeee cc      \n");
   printf("      SS ii gg   gg       SS pp   pp ee      cc      \n");
   printf("SS    SS ii gg   gg SS    SS pp   pp ee   ee cc   cc \n");
   printf(" SSSSSS  ii  gggggg  SSSSSS  pppppp   eeeee   ccccc  \n");
   printf("                 gg          pp                      \n");
   printf("            gg   gg          pp                      \n");
   printf("             ggggg           pp                      \n\n\n");

   printf("SIGnificance SPECtrum\n");
   printf("Version 2.0\n");
   printf("************************************************************\n");
   printf("by Piet Reegen\n");
   printf("Institute of Astronomy\n");
   printf("University of Vienna\n");
   printf("Tuerkenschanzstrasse 17\n1180 Vienna, Austria\n");
   printf("Release date: August 18, 2009\n\n");
   fflush(stdout);
	
   return;
}



int MultiFile_Count(
   )

{
   struct String testfile;
   FILE* dummy;
   char cbuf[MAXCHAR];
   if (!quiet) Log_Header("MultiFile count");
   testfile.c = (int*)calloc(String_SetLength(&testfile,
      File_NameLength(NULL, mfs, -1, -1, String_AsChars(&project, cbuf), -1, -1,
      "dat")), sizeof(int));
   File_Name(NULL, mfs, -1, -1, cbuf, -1, -1, "dat", &testfile);
   for (nmf = mfs + 1; (nmf < mfl) &&
     (dummy = fopen(String_AsChars(&testfile, cbuf), "r")) != NULL; nmf++)
   {
      free(testfile.c);
      fclose(dummy);
      testfile.c = (int*)calloc(String_SetLength(&testfile,
         File_NameLength(NULL, nmf, -1, -1, cbuf, -1, -1, "dat")), sizeof(int));
      File_Name(NULL, nmf + 1, -1, -1, String_AsChars(&project, cbuf), -1, -1,
         "dat", &testfile);
   }
   free(testfile.c);
   if (nmf < 1) return -36;
   if (mfl == INT_MAX) mfl = nmf - 1;
   if (mfl > nmf)
   {
      Log_ErrorMessage(37);
      printf("%i.\n\n", mfl = nmf - 1);
   }
   return nmf;
}



int MultiSine_ClearTracks(
   struct TS* ts,
   int ti
   )

{
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int err;
   int ri = 1;
   FILE* log;
   if (debug >= 0) log = fopen("MultiSine_ClearTracks.log", "a");
   if (debug > 0)
   {
      fprintf(log, "**** MultiSine_ClearTracks ****************************\n");
      fflush(log);
   }
   do
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "m", ri, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "m", ri++, ti, "dat", &fname);
      err = remove(String_AsChars(&fname, cbuf));
      free(fname.c);
   } while (err == 0);
   if (debug >= 0) fclose(log);
   return ri;
}



int MultiSine_DDWA(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   int oi,
   int end
   )

{
   int ii;
   int ri;
   int hi;
   int si;
   int ci;
   int ai;
   double sine;
   double cosine;
   double msine;
   int count = 0;
   int kall;
   char dstr[65536];
   double* var1;
   double* var2;
   if (end == 0) return 0;
   var1 = (double*)calloc(end + 1, sizeof(double));
   var2 = (double*)calloc(end + 1, sizeof(double));
   for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] = orig->d[oi][ri];
   for (ai = 0; ai < end - 1; ai++) for (hi = 0; hi <= oharm; hi++)
      for (ri = 0; ri < orig->r; ri++)
         ts->d[0][ri] -= result[ai].h[hi].amp * cos(2. * PI * (hi + 1.) *
         result[ai].f * orig->d[t][ri] - result[ai].h[hi].th);
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
      orig->nss, ts->d[0]);
   while (++ri <= orig->nss);
   ts->rms = Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   for (hi = 0; hi <= oharm; hi++)
   {
      result[end - 1].h[hi].th =
         Fit_Phase(orig, ts, (hi + 1.) * result[end - 1].f);
      result[end - 1].h[hi].amp = Fit_Amp(orig, ts,
         (hi + 1.) * result[end - 1].f, &result[end - 1].h[hi].th);
      for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
         result[end - 1].h[hi].amp * cos(2. * PI * (hi + 1.) *
         result[end - 1].f * orig->d[t][ri] - result[end - 1].h[hi].th);
   }
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
      orig->nss, ts->d[0]);
   while (++ri <= orig->nss);
   if (end > 0) do
   {
      kall = 1;
      count++;
      result[end].rms = ts->rms;
      result[end].ppsc = ts->ppsc;
      for (ii = 0; ii < end; ii++)
      {
         var1[ii] = 0;
         var2[ii] = 0;
         for (si = 1; si <= orig->nss; si++)
         {
            msine = 0;
            for (ri = 0; ri < orig->r; ri++)
               if ((orig->nss < 2) || (orig->ss[ri] == si))
            {
               sine = 0;
               cosine = 0;
               for (hi = 0; hi <= oharm; hi++)
               {
                  sine += (hi + 1.) * orig->d[t][ri] * result[ii].h[hi].amp *
                     sin(2. * PI * (hi + 1.) * result[ii].f * orig->d[t][ri] -
                     result[ii].h[hi].th);
                  cosine += pow((hi + 1.) * orig->d[t][ri], 2) *
                     result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
                     result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
               }
               msine += sine;
               var1[ii] += ts->d[0][ri] * sine;
               var2[ii] += ts->d[0][ri] * cosine + pow(sine, 2);
            }
            var2[ii] -= pow(msine, 2) / orig->nsse[si - 1];
         }
         var1[ii] *= 4. * PI / orig->r;
         var2[ii] *= 8. * PI * PI / orig->r;
         for (hi = 0; hi <= oharm; hi++) for (ri = 0; ri < orig->r; ri++)
            ts->d[0][ri] += result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
            result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
         result[ii].f -= var1[ii] / var2[ii];
         for (hi = 0; hi <= oharm; hi++)
         {
            result[ii].h[hi].th =
               Fit_Phase(orig, ts, (hi + 1.) * result[ii].f);
            result[ii].h[hi].amp = Fit_Amp(orig, ts,
               (hi + 1.) * result[ii].f, &result[ii].h[hi].th);
            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
               result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
               result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
         }
         ri = 1;
         do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
            orig->nss, ts->d[0]);
         while (++ri <= orig->nss);
         ts->rms = Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
         if (fabs(var1[ii] / var2[ii]) > msacc *
            pow(result[ii].sig, -.5 * mssens) / (orig->d[t][orig->r - 1] -
            orig->d[t][0])) kall = 0;
      }
      ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   } while (kall == 0);
   result[end].rms = ts->rms;
   result[end].ppsc = ts->ppsc;
   free(var1);
   free(var2);
   return 0;
}



int MultiSine_Derive(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   struct Result* var1,
   struct Result* var2,
   int oi,
   int end
   )

{
   int ii;
   int ri;
   int hi;
   int si;
   int ci;
   int ai;
   double sine;
   double cosine;
   double hsin;
   double hcos;
   double tsin;
   double rcos;
   int count = 0;
   int kall;
   char dstr[65536];
   double wsum;
   ri = 1;
   for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] = orig->d[x][ri];
   for (ii = 0; ii < end; ii++) for (hi = 0; hi <= oharm; hi++)
      for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
      result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
      result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
      orig->nss, ts->d[0]);
   while (++ri <= orig->nss);
   for (ii = 0; ii < end; ii++)
   {
      Result_Reset(&var1[ii]);
      Result_Reset(&var2[ii]);
      for (si = 1; si <= orig->nss; si++)
      {
         tsin = 0;
         wsum = 0;
         for (hi = 0; hi <= oharm; hi++)
         {
            var2[ii].h[hi].a0 = 0;
            var2[ii].h[hi].b0 = 0;
         }
         for (ri = 0; ri < orig->r; ri++)
            if ((orig->nss < 2) || (orig->ss[ri] == si))
         {
            hsin = 0;
            hcos = 0;
            wsum += orig->w[ri];
            for (hi = 0; hi <= oharm; hi++)
            {
               sine = orig->w[ri] * sin(2. * PI * (hi + 1.) * result[ii].f *
                  orig->d[t][ri] - result[ii].h[hi].th);
               cosine = orig->w[ri] * cos(2. * PI * (hi + 1.) * result[ii].f *
                  orig->d[t][ri] - result[ii].h[hi].th);
               hsin += (hi + 1.) * result[ii].h[hi].amp * sine;
               hcos += pow(hi + 1., 2) * result[ii].h[hi].amp * cosine;
               rcos = ts->d[0][ri] * cosine;
               var1[ii].h[hi].th -= result[ii].h[hi].amp * ts->d[0][ri] *
                  sine;
               var1[ii].h[hi].amp -= rcos;
               var2[ii].h[hi].a0 += cosine;
               var2[ii].h[hi].b0 += sine;
               var2[ii].h[hi].amp += pow(cosine, 2);
               var2[ii].h[hi].th += pow(result[ii].h[hi].amp * sine, 2) +
                  result[ii].h[hi].amp * rcos;
            }
            var1[ii].f += ts->d[0][ri] * orig->d[t][ri] * hsin;
            var2[ii].f += pow(orig->d[t][ri], 2) * (pow(hsin, 2) +
               ts->d[0][ri] * hcos);
            tsin += orig->d[t][ri] * hsin;
         }
         var2[ii].f -= pow(tsin, 2) / wsum;
         for (hi = 0; hi <= oharm; hi++)
         {
            var2[ii].h[hi].amp -=
               pow(var2[ii].h[hi].a0, 2) / wsum;
            var2[ii].h[hi].th -= pow(result[ii].h[hi].amp *
               var2[ii].h[hi].b0, 2) / wsum;
         }
      }
      var1[ii].f *= 4. * PI / orig->r;
      var2[ii].f *= 8. * PI * PI / orig->r;
      for (hi = 0; hi <= oharm; hi++)
      {
         var1[ii].h[hi].amp *= 2. / orig->r;
         var1[ii].h[hi].th *= 2. / orig->r;
         var2[ii].h[hi].amp *= 2. / orig->r;
         var2[ii].h[hi].th *= 2. / orig->r;
      }
   }
   return 0;
}



int MultiSine_DWA(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   int oi,
   int end
   )

{
   int ii;
   int ri;
   int hi;
   int si;
   int ci;
   int ai;
   double sine;
   double cosine;
   double msine;
   int count = 0;
   int kall;
   char dstr[65536];
   double* var1;
   double* var2;
   if (end == 0) return 0;
   var1 = (double*)calloc(end + 1, sizeof(double));
   var2 = (double*)calloc(end + 1, sizeof(double));
   for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] = orig->d[oi][ri];
   for (ai = 0; ai < end - 1; ai++) for (hi = 0; hi <= oharm; hi++)
      for (ri = 0; ri < orig->r; ri++)
         ts->d[0][ri] -= result[ai].h[hi].amp * cos(2. * PI * (hi + 1.) *
         result[ai].f * orig->d[t][ri] - result[ai].h[hi].th);
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
      orig->nss, ts->d[0]);
   while (++ri <= orig->nss);
   ts->rms = Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   for (hi = 0; hi <= oharm; hi++)
   {
      result[end - 1].h[hi].th =
         Fit_Phase(orig, ts, (hi + 1.) * result[end - 1].f);
      result[end - 1].h[hi].amp = Fit_Amp(orig, ts,
         (hi + 1.) * result[end - 1].f, &result[end - 1].h[hi].th);
      for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
         result[end - 1].h[hi].amp * cos(2. * PI * (hi + 1.) *
         result[end - 1].f * orig->d[t][ri] - result[end - 1].h[hi].th);
   }
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
      orig->nss, ts->d[0]);
   while (++ri <= orig->nss);
   if (end > 0) do
   {
      kall = 1;
      count++;
      result[end].rms = ts->rms;
      result[end].ppsc = ts->ppsc;
      for (ii = 0; ii < end; ii++)
      {
         var1[ii] = 0;
         var2[ii] = 0;
         for (si = 1; si <= orig->nss; si++)
         {
            msine = 0;
            for (ri = 0; ri < orig->r; ri++)
               if ((orig->nss < 2) || (orig->ss[ri] == si))
            {
               sine = 0;
               cosine = 0;
               for (hi = 0; hi <= oharm; hi++)
               {
                  sine += (hi + 1.) * orig->d[t][ri] * result[ii].h[hi].amp *
                     sin(2. * PI * (hi + 1.) * result[ii].f * orig->d[t][ri] -
                     result[ii].h[hi].th);
                  cosine += pow((hi + 1.) * orig->d[t][ri], 2) *
                     result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
                     result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
               }
               msine += sine;
               var1[ii] += ts->d[0][ri] * sine;
               var2[ii] += ts->d[0][ri] * cosine + pow(sine, 2);
            }
            var2[ii] -= pow(msine, 2) / orig->nsse[si - 1];
         }
         var1[ii] *= 4. * PI / orig->r;
         var2[ii] *= 8. * PI * PI / orig->r;
         for (hi = 0; hi <= oharm; hi++) for (ri = 0; ri < orig->r; ri++)
            ts->d[0][ri] += result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
            result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
         result[ii].f -= var1[ii] / var2[ii];
         for (hi = 0; hi <= oharm; hi++)
         {
            result[ii].h[hi].th =
               Fit_Phase(orig, ts, (hi + 1.) * result[ii].f);
            result[ii].h[hi].amp = Fit_Amp(orig, ts,
               (hi + 1.) * result[ii].f, &result[ii].h[hi].th);
            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
               result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
               result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
         }
         ri = 1;
         do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
            orig->nss, ts->d[0]);
         while (++ri <= orig->nss);
         ts->rms = Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
         if (fabs(var1[ii] / var2[ii]) > msacc *
            pow(result[ii].sig, -.5 * mssens) / (orig->d[t][orig->r - 1] -
            orig->d[t][0])) kall = 0;
      }
      ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   } while (kall == 0);
   result[end].rms = ts->rms;
   result[end].ppsc = ts->ppsc;
   free(var1);
   free(var2);
   return 0;
}



int MultiSine_Fit(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   int ot,
   int oi,
   int end
   )

{
   int ii;
   int err;
   TS_TimeShift(orig, ot, - orig->meantime);
   for (ii = 0; ii < end; ii++) Result_TimeShift(&result[ii], - orig->meantime);
   switch(msmode)
   {
   case 0:
      err = MultiSine_Newton(orig, ts, result, oi, end);
      break;
   case 1:
      err = MultiSine_DWA(orig, ts, result, oi, end);
      break;
   case 2:
      err = MultiSine_DDWA(orig, ts, result, oi, end);
      break;
   default:
      break;
   }
   TS_TimeShift(orig, ot, orig->meantime);
   for (ii = 0; ii < end; ii++) Result_TimeShift(&result[ii], orig->meantime);
   return err;
}



int MultiSine_Newton(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   int oi,
   int end
   )

{
   struct String fname;
   FILE* F;
   int ii;
   int ri;
   int hi;
   int si;
   int ci;
   int ai;
   int count = 0;
   int kall;
   char dstr[65536];
   struct Result* var1;
   struct Result* var2;
   FILE* log;
   int err;
   double oldrms = orig->rms;
   if (end == 0) return 0;
   if (debug >= 0) log = fopen("MultiSine_Newton.log", "a");
   var1 = (struct Result*)calloc(end, sizeof(struct Result));
   var2 = (struct Result*)calloc(end, sizeof(struct Result));
   for (ii = 0; ii < end; ii++)
   {
      var1[ii].h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
      var2[ii].h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   }
   ts->rms = orig->rms;
   if (end > 0) do
   {
      kall = 1;
      count++;
      if ((err = MultiSine_Derive(orig, ts, result, var1, var2, oi, end)) != 0)
         return err;
      for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] = orig->d[x][ri];
      for (ii = 0; ii < end; ii++)
      {
         result[ii].f -= var1[ii].f / var2[ii].f;
         for (hi = 0; hi <= oharm; hi++)
         {
            result[ii].h[hi].amp -= var1[ii].h[hi].amp / var2[ii].h[hi].amp;
            if (result[ii].h[hi].amp < 0)
            {
               result[ii].h[hi].amp = -result[ii].h[hi].amp;
               result[ii].h[hi].th -= PI;
            }
            result[ii].h[hi].th -= var1[ii].h[hi].th / var2[ii].h[hi].th;
            while (result[ii].h[hi].th < 0) result[ii].h[hi].th += 2. * PI;
            while (result[ii].h[hi].th >= 2. * PI) result[ii].h[hi].th -= 2. *
               PI;


            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
               result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
               result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
         }
         if (fabs(var1[ii].f / var2[ii].f) > msacc *
            pow(result[ii].sig, -.5 * mssens) / (orig->d[t][orig->r - 1] -
            orig->d[t][0])) kall = 0;

         
      }
      ri = 1;
      do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
         orig->nss, ts->d[0]);
      while (++ri <= orig->nss);
      while ((kall == 0) &&
         (Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri) > ts->rms))
      {
         kall = 1;
         for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] = orig->d[x][ri];
         for (ii = 0; ii < end; ii++)
         {
            var1[ii].f *= .5;
            result[ii].f += var1[ii].f / var2[ii].f;
            for (hi = 0; hi <= oharm; hi++)
            {
               var1[ii].h[hi].amp *= .5;
               result[ii].h[hi].amp += var1[ii].h[hi].amp / var2[ii].h[hi].amp;
               if (result[ii].h[hi].amp < 0)
               {
                  result[ii].h[hi].amp = -result[ii].h[hi].amp;
                  result[ii].h[hi].th -= PI;
               }
               var1[ii].h[hi].th *= .5;
               result[ii].h[hi].th += var1[ii].h[hi].th / var2[ii].h[hi].th;
               while (result[ii].h[hi].th < 0) result[ii].h[hi].th += 2. * PI;
               while (result[ii].h[hi].th >= 2. * PI) result[ii].h[hi].th -=
                  2. * PI;


               for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
                  result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) *
                  result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
            }
            if (fabs(var1[ii].f / var2[ii].f) > msacc *
               pow(result[ii].sig, -.5 * mssens) / (orig->d[t][orig->r - 1] -
               orig->d[t][0])) kall = 0;

            
         }
         ri = 1;
         do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
            orig->nss, ts->d[0]);
         while (++ri <= orig->nss);
      }
      oldrms = ts->rms;
      ts->rms = Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);

      if (1 - ts->rms / oldrms < msrms) kall = 1;
      ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
      if (debug > 1)
      {
         Debug_MultiSine(count, end, result, var1, var2, orig, ts, dstr);
         fprintf(log, "%s", dstr);
         fflush(log);
      }
      if ((!quiet) && (aad < 2))
      {
         printf("\rMultiSine fit: iteration %i, rms res %lg                  ",
            count, ts->rms);
         fflush(stdout);
      }
   } while (kall == 0);
   result[end].rms = ts->rms;
   result[end].ppsc = ts->ppsc;
   for (ii = 0; ii < end; ii++)
   {
      free(var1[ii].h);
      free(var2[ii].h);
   }
   free(var1);
   free(var2);
   if (debug >= 0) fclose(log);
   return 0;
}



int MultiSine_WriteProfiles(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   int ti,
   int end
   )

{
   int ii;
   int ri;
   int hi;
   int si;
   int ci;
   int ai;
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int err;
   FILE* log;
   double f;
   double th;
   double amp;
   int count;
   double d1;
   double d2;
   struct Result* var1 = (struct Result*)calloc(end, sizeof(struct Result));
   struct Result* var2 = (struct Result*)calloc(end, sizeof(struct Result));
   for (ii = 0; ii < end; ii++)
   {
      var1[ii].h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
      var2[ii].h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   }
   if (debug >= 0) log = fopen("MultiSine_WriteProfiles.log", "a");
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
      ts->fi, -1, -1, "f", end, ti, "dat")), sizeof(int));
   File_Name(&project, ts->fi, -1, -1, "f", end, ti, "dat", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 190;
   free(fname.c);
   if ((err = MultiSine_Derive(orig, ts, result, var1, var2, x, end)) != 0)
      return err;
   for (ii = 0; ii < end; ii++)
   {
      fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %8i %8i\n",
         result[ii].f, Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
         ts->rms, ts->rms,
         Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai), ii + 1,
         0);
      d1 = .5 * var1[ii].f / ts->rms;
      d2 = .5 * (var2[ii].f * ts->rms - d1 * var1[ii].f) / pow(ts->rms, 2);
      for (ri = 0; ri < orig->r; ri++) for (hi = 0; hi <= oharm; hi++)
         ts->d[0][ri] += result[ii].h[hi].amp * cos(2. * PI * (oharm + 1) *
         result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
      count = 0;
      for (f = result[ii].f - .5 / (orig->d[t][orig->r - 1] - orig->d[t][0]) /
         sqrt(result[ii].sig); f <= result[ii].f + .5 /
         (orig->d[t][orig->r - 1] - orig->d[t][0]) / sqrt(result[ii].sig); f +=
         1. / (orig->d[t][orig->r - 1] - orig->d[t][0]) / sqrt(result[ii].sig) /
         (msp - 1.))
      {
         for (hi = 0; hi <= oharm; hi++) for (ri = 0; ri < orig->r; ri++)
            ts->d[0][ri] -= result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) * f *
            orig->d[t][ri] - result[ii].h[hi].th);
         ri = 1;
         do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
            orig->nss, ts->d[0]);
         while (++ri <= orig->nss);
         fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %8i %8i\n", f,
            Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
            ts->rms + d1 * (f - result[ii].f),
            ts->rms + d1 * (f - result[ii].f) +
            .5 * d2 * pow(f - result[ii].f, 2),
            Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
            ii + 1, 0);
         for (ri = 0; ri < orig->r; ri++) for (hi = 0; hi <= oharm; hi++)
            ts->d[0][ri] += result[ii].h[hi].amp * cos(2. * PI * (oharm + 1) *
            f * orig->d[t][ri] - result[ii].h[hi].th);
         if (!quiet)
            Log_Percent("MultiSine profile (frequency)", msp * ii + (++count),
            (2. * oharm + 3.) * msp * end);
      }
      for (ri = 0; ri < orig->r; ri++) for (hi = 0; hi <= oharm; hi++)
         ts->d[0][ri] -= result[ii].h[hi].amp * cos(2. * PI * (oharm + 1) *
         result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
      ri = 1;
      do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
         orig->nss, ts->d[0]);
      while (++ri <= orig->nss);
   }
   fclose(F);
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
      ts->fi, -1, -1, "a", end, ti, "dat")), sizeof(int));
   File_Name(&project, ts->fi, -1, -1, "a", end, ti, "dat", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 190;
   free(fname.c);
   for (ii = 0; ii < end; ii++)
   {
      count = 0;
      for (hi = 0; hi <= oharm; hi++)
      {
         fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %8i %8i\n",
            result[ii].h[hi].amp,
            Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
            ts->rms, ts->rms,
            Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai), ii + 1,
            hi);
         d1 = .5 * var1[ii].h[hi].amp / ts->rms;
         d2 = .5 * (var2[ii].h[hi].amp * ts->rms - d1 * var1[ii].h[hi].amp) /
            pow(ts->rms, 2);
         for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] += result[ii].h[hi].amp *
            cos(2. * PI * (oharm + 1) * result[ii].f * orig->d[t][ri] -
            result[ii].h[hi].th);
         for (amp = 0; amp <= 2.* result[ii].h[hi].amp;
            amp += 2. * result[ii].h[hi].amp / (msp - 1.))
         {
            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -= amp * cos(2. * PI *
               (hi + 1.) * result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
            ri = 1;
            do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
               orig->nss, ts->d[0]);
            while (++ri <= orig->nss);
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %8i %8i\n",
               amp, Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
               ts->rms + d1 * (amp - result[ii].h[hi].amp),
               ts->rms + d1 * (amp - result[ii].h[hi].amp) +
               .5 * d2 * pow(amp - result[ii].h[hi].amp, 2),
               Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
               ii + 1, hi);
            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] += amp * cos(2. * PI *
               (hi + 1.) * result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
            if (!quiet)
               Log_Percent("MultiSine profile (amplitude)", msp * end +
               (oharm + 1.) * msp * ii + (++count), (2. * oharm + 3.) * msp *
               end);
         }
         for (ri = 0; ri < orig->r; ri++) for (hi = 0; hi <= oharm; hi++)
            ts->d[0][ri] -= result[ii].h[hi].amp * cos(2. * PI * (oharm + 1) *
            result[ii].f * orig->d[t][ri] - result[ii].h[hi].th);
         ri = 1;
         do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
            orig->nss, ts->d[0]);
         while (++ri <= orig->nss);
      }
   }
   fclose(F);
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
      ts->fi, -1, -1, "p", end, ti, "dat")), sizeof(int));
   File_Name(&project, ts->fi, -1, -1, "p", end, ti, "dat", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 190;
   free(fname.c);
   for (ii = 0; ii < end; ii++)
   {
      count = 0;
      for (hi = 0; hi <= oharm; hi++)
      {
         fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %8i %8i\n",
            result[ii].h[hi].th,
            Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai), ts->rms,
            ts->rms,
            Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
            ii + 1, hi);
         d1 = .5 * var1[ii].h[hi].th / ts->rms;
         d2 = .5 * (var2[ii].h[hi].th * ts->rms - d1 * var1[ii].h[hi].th) /
            pow(ts->rms, 2);
         for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] += result[ii].h[hi].amp *
            cos(2. * PI * (oharm + 1) * result[ii].f * orig->d[t][ri] -
            result[ii].h[hi].th);
         for (th = result[ii].h[hi].th - PI; th <= result[ii].h[hi].th + PI;
            th += 2. * PI / (msp - 1.))
         {
            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
               result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) * result[ii].f *
               orig->d[t][ri] - th);
            ri = 1;
            do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
               orig->nss, ts->d[0]);
            while (++ri <= orig->nss);
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %8i %8i\n", th,
               Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
               ts->rms + d1 * (th - result[ii].h[hi].th),
               ts->rms + d1 * (th - result[ii].h[hi].th) +
               .5 * d2 * pow(th - result[ii].h[hi].th, 2),
               Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ai),
               ii + 1, hi);
            for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] +=
               result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) * result[ii].f *
               orig->d[t][ri] - th);
            if (!quiet)
               Log_Percent("MultiSine profile (phase)", (oharm + 2.) * msp *
               end + (oharm + 1.) * msp * ii + (++count), (2. * oharm + 3.) *
               msp * end);
         }
         for (ri = 0; ri < orig->r; ri++) ts->d[0][ri] -=
            result[ii].h[hi].amp * cos(2. * PI * (hi + 1.) * result[ii].f *
            orig->d[t][ri] - result[ii].h[hi].th);
         ri = 1;
         do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri,
            orig->nss, ts->d[0]);
         while (++ri <= orig->nss);
      }
   }
   fclose(F);
   for (ii = 0; ii < end; ii++)
   {
      free(var1[ii].h);
      free(var2[ii].h);
   }
   free(var1);
   free(var2);
   if (debug >= 0) fclose(log);
   return 0;
}



int MultiSine_WriteTracks(
   struct TS* ts,
   struct Result* result,
   int ti,
   int rows
   )

{
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int err;
   int ri;
   FILE* log;
   for (ri = 0; ri < rows; ri++)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "m", ri + 1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "m", ri + 1, ti, "dat", &fname);
      if ((F = fopen(String_AsChars(&fname, cbuf), "a")) == NULL) return 142;
      free(fname.c);
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n", result[ri].f,
         result[ri].h[0].amp, result[ri].h[0].th);
      fclose(F);
   }
   return 0;
}



double Nyquist_Coef(
   double* tdata,
   int start,
   int end,
   double nf
   )

{
   int i;
   int count = 0;
   double l = DBL_MAX;
   double u = DBL_MIN;
   if (start < 0) return -1;
   if (end < 0) return -2;
   if (end <= start) return -3;
   if (nf < 0) return -4;
   if (nf == 0) return -5;
   for (i = start; i <= end; i++)
   {
      if (tdata[i] < l) l = tdata[i];
      if (tdata[i] > u) u = tdata[i];
   }
   if (u <= l) return -6;
   for (i = start; i < end; i++)
   {
      if (tdata[i+1] < tdata[i]) return 0;
      if (tdata[i+1] - tdata[i] < .5 / nf) count++;
   }
   return (double)count / (end - start);
}



double Nyquist_Freq(
   double* tdata,
   int start,
   int end,
   double nc
   )

{
   int i;
   double step = 0;
   double test = 0;
   int count = 0;
   double l = DBL_MAX;
   double u = DBL_MIN;
   if (start < 0) return -1;
   if (end < 0) return -2;
   if (end <= start) return -3;
   if (nc < 0) return -4;
   if (nc == 0) return -5;
   for (i = start; i <= end; i++)
   {
      if (tdata[i] < l) l = tdata[i];
      if (tdata[i] > u) u = tdata[i];
   }
   if (u <= l) return -6;
   do
   {
      test = u - l;
      count = 0;
      for (i = start; i < end; i++)
      {
         if (tdata[i+1] < tdata[i]) return 0;
         if ((tdata[i+1] - tdata[i] < test) && (tdata[i+1] - tdata[i] > step))
            test = tdata[i+1] - tdata[i];
      }
      for (i = start; i < end; i++) if (tdata[i+1] - tdata[i] <= test) count++;
      step = test;
   } while (count < (end - start) * (1. - nc));
   return .5 / step;
}



double Nyquist_Scan(
   struct TS* ts,
   int start,
   int end
   )

{
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int ri;
   int fi;
   int count;
   if (start < 0) return -1;
   if (end < 0) return -2;
   if (end <= start) return -3;
   if (!quiet)
   {
      printf("\rscanning Nyquist Coefficients                                ");
      fflush(stdout);
   }
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
      ts->fi, -1, -1, "nycoef", -1, -1, "dat")), sizeof(int));
   File_Name(&project, ts->fi, -1, -1, "nycoef", -1, -1, "dat", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 155;
   free(fname.c);
   for (fi = 0; fi < nspr; fi++)
   {
      count = 0;
      for (ri = start; ri < end; ri++)
      {
         if (ts->d[t][ri+1] < ts->d[t][ri]) return 0;
         if (ts->d[t][ri+1] - ts->d[t][ri] > .5 / (lf + fi * fs)) count++;
         Log_Percent("scanning Nyquist coefficients",
            (double)fi * (end - start) + ri, (double)nspr * (end - start));
         fflush(stdout);
      }
      fprintf(F, "%24.16lf %24.16lf\n", lf + fi * fs,
         (double)count / (end - start));
   }
   fclose(F);
   if (!quiet)
   {
      printf("\r                                                             ");
      fflush(stdout);
   }
   return 0;
}



int PhaseDiagram_Generate(
   struct TS* orig,
   struct TS* ts,
   struct TS* pw,
   struct Result* result,
   int ti,
   int rows
   )

{
   int ri;
   int fi;
   int hi;
   struct String fname;
   struct String pname;
   struct String pbuf;
   char cbuf[MAXCHAR];
   FILE* F;
   int pos;
   double val;
   if (!quiet)
      printf("\rphase diagram                                                ");
   fflush(stdout);
   if (rows <= 0)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, pos = 
         File_NameLength(&project,
         ts->fi, ti, -1, "result", -1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "result", -1, ti, "dat", &fname);
      rows *= -1;
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, pos = 
         File_NameLength(&project,
         ts->fi, -1, -1, "r", rows, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "r", rows, ti, "dat", &fname);
   }
   for (fi = 0; fi < rows; fi++)
   {
      pbuf.c = (int*)calloc(String_SetLength(&pbuf, ITDIGITS), sizeof(int));
      NumFormat_FixDigits(fi + 1, &pbuf);
      pname.c = (int*)calloc(String_SetLength(&pname, pos + 2 + ITDIGITS),
         sizeof(int));
      pos = String_Copy(&fname, &pname, 0);
      pos = String_SetChar(&pname, '-', pos - 4);
      pos = String_SetChar(&pname, 'p', pos);
      pos = String_Copy(&pbuf, &pname, pos);
      pos = String_SetChar(&pname, '.', pos);
      pos = String_SetChar(&pname, 'd', pos);
      pos = String_SetChar(&pname, 'a', pos);
      pos = String_SetChar(&pname, 't', pos);
      free(pbuf.c);
      if ((F = fopen(String_AsChars(&pname, cbuf), "w")) == NULL) return 152;
      free(pname.c);
      for (ri = 0; ri < ts->r; ri++) fprintf(F, "%24.16lf %24.16lf\n",
         fmod(orig->d[t][ri] * result[fi].f, 1.), ts->d[0][ri]);
      fclose(F);
      if (pf >= 2)
      {
         pbuf.c = (int*)calloc(String_SetLength(&pbuf, ITDIGITS), sizeof(int));
         NumFormat_FixDigits(fi + 1, &pbuf);
         pname.c = (int*)calloc(String_SetLength(&pname, pos + 2 + ITDIGITS),
            sizeof(int));
         pos = String_Copy(&fname, &pname, 0);
         pos = String_SetChar(&pname, '-', pos - 4);
         pos = String_SetChar(&pname, 'f', pos);
         pos = String_Copy(&pbuf, &pname, pos);
         pos = String_SetChar(&pname, '.', pos);
         pos = String_SetChar(&pname, 'd', pos);
         pos = String_SetChar(&pname, 'a', pos);
         pos = String_SetChar(&pname, 't', pos);
         free(pbuf.c);
         if ((F = fopen(String_AsChars(&pname, cbuf), "w")) == NULL) return 153;
         free(pname.c);
         for (ri = 0; ri < pf; ri++)
         {
            val = 0;
            for (hi = 0; hi <= oharm; hi++) val += result[fi].h[hi].amp *
               cos(2. * PI * (hi + 1.) * ri / (pf - 1.) -
               result[fi].h[hi].th);
            fprintf(F, "%24.16lf %24.16lf\n", ri / (pf - 1.), val);
         }
         fclose(F);
      }
      if (!quiet) Log_Percent("phase diagram", (double)ri, (double)rows);
   }
   if (!quiet)
      printf("\r                                                             ");
   fflush(stdout);
   free(fname.c);
   return 0;
}



int PhD_Generate(
   struct TS* orig,
   struct TS* ts,
   int ti
   )

{
   int fi;
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   if (!quiet)
   {
      printf("\rphase dispersion spectrum                                    ");
      fflush(stdout);
   }
   if (pdnp <= 0) pdnp = nff;
   if (pdlp <= 0) pdlp = 1. / uf;
   if (pdup <= 0)
   {
      if (lf > 0) pdup = 1. / lf;
      else pdup = 1. / (orig->d[t][orig->r-1] - orig->d[t][0]);
   }
   if (ts->it < 0)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, ti, -1, "respds", -1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "respds", -1, ti, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "d", ts->it, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "d", ts->it, ti, "dat", &fname);
   }
   String_AsChars(&fname, cbuf);
   free(fname.c);
   if ((F = fopen(cbuf, "w")) == NULL) return 69;
   for (fi = 0; fi < pdnp; fi++)
   {
      fprintf(F, "%24.16lf %24.16lf\n", pdlp + fi * (pdup - pdlp) / (pdnp - 1.),
         PhD_SinglePeriod(orig, ts, pdlp + fi * (pdup - pdlp) / (pdnp - 1.)));
      if (!quiet) Log_Percent("phase dispersion spectrum", (double)fi + 1,
         (double)nff);
   }
   fclose(F);
   if (!quiet)
   {
      printf("\r                                                             ");
      fflush(stdout);
   }
   return 0;
}



double PhD_SinglePeriod(
   struct TS* orig,
   struct TS* ts,
   double period
   )

{
   int ti;
   int bi;
   double bincount;
   double binmean;
   double binvar;
   double result = 0;
   double ph;
   if (pdb < 2) pdb = (int)floor(sqrt(ts->r + 1.) * .5);
   if (pdb < 2) return -1;
   if (period == 0) return 0;
   ts->rms = Stat_SDev(ts->d[0], orig->w, 0, ts->r - 1, 0, 0, &bi);
   ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, ts->r - 1, 0, 0, &bi);
   for (bi = 0; bi < pdb; bi++)
   {
      bincount = 0;
      binmean = 0;
      binvar = 0;
      for (ti = 0; ti < ts->r; ti++)
      {
         if (((ph = fmod(orig->d[t][ti] / period, 1.)) >=
            1. * bi / pdb - .5 * pdw) && (ph < 1. * bi / pdb + .5 * pdw))
         {
            bincount += orig->w[ti];
            binmean += orig->w[ti] * ts->d[0][ti];
            binvar += orig->w[ti] * pow(ts->d[0][ti], 2);
         }
         if ((ph + 1. >= bi / pdb - .5 * pdw) && (ph + 1. < bi / pdb + .5 * pdw))
         {
            bincount += orig->w[ti];
            binmean += orig->w[ti] * ts->d[0][ti];
            binvar += orig->w[ti] * pow(ts->d[0][ti], 2);
         }
         if ((ph - 1. >= bi / pdb - .5 * pdw) && (ph - 1. < bi / pdb + .5 * pdw))
         {
            bincount += orig->w[ti];
            binmean += orig->w[ti] * ts->d[0][ti];
            binvar += orig->w[ti] * pow(ts->d[0][ti], 2);
         }
      }
      if (bincount > 0) result += binvar / bincount - pow(binmean / bincount, 2);
   }
   return pow(ts->rms, 2) * pdb / result - 1;
}



int PhDist_Cartesian(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int fi;
   int ph;
   int fill;
   int i;
   double dbuf;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "phdist", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "phdist", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "phdist", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "phdist", -1, -1, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 97;
   free(fname.c);
   if (lf == 0)
   {
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         0., 0., 0., 0., PI, 0.);
      fflush(F);
   }
   else for (ph = 0; ph < ceil(phdistph / prof->a0[0]); ph++)
   {
      dbuf = .5 / PI * prof->a0[0] * prof->b0[0] /
         (pow(prof->b0[0] * cos(PI * ph / phdistph * prof->a0[0]), 2) +
         pow(prof->a0[0] * sin(PI * ph / phdistph * prof->a0[0]), 2));
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         lf, - dbuf * cos(PI * ph / phdistph * prof->a0[0]),
         - dbuf * sin(PI * ph / phdistph * prof->a0[0]),
         lf, dbuf * cos(PI * ph / phdistph * prof->a0[0]),
         dbuf * sin(PI * ph / phdistph * prof->a0[0]));
      fflush(F);
   }
   for (fi = 1; fi < nspr; fi++)
   {
      fill = (int)ceil(fabs(1. / prof->a0[fi] - 1. / prof->a0[fi - 1]) *
         phdistfill);
      for (i = 1; i < fill; i++)
      {
         if (lf + (uf - lf) * (fi - 1.) / (nspr - 1.) +
            (uf - lf) / (nspr - 1.) * i / fill == 0)
         {
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               0., 0., 0., 0., PI, 0.);
            fflush(F);
         }
         else for (ph = 0; ph < ceil(phdistph * (1. / prof->a0[fi - 1] +
            i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))); ph++)
         {
            dbuf = .5 / PI * (prof->a0[fi - 1] +
               (prof->a0[fi] - prof->a0[fi - 1]) * i / fill) *
               (prof->b0[fi - 1] +
               (prof->b0[fi] - prof->b0[fi - 1]) * i / fill) /
               (pow((prof->b0[fi - 1] +
               (prof->b0[fi] - prof->b0[fi - 1]) * i / fill) *
               cos(PI * ph / phdistph / (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))), 2) +
               pow((prof->a0[fi - 1] +
               (prof->a0[fi] - prof->a0[fi - 1]) * i / fill) *
               sin(PI * ph / phdistph / (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))), 2));
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               lf + (uf - lf) * (fi - 1.) / (nspr - 1.) +
               (uf - lf) / (nspr - 1.) * i / fill,
               - dbuf * cos(PI * ph / phdistph /
               (1. / prof->a0[fi - 1] + i / fill *
               fabs((1. / prof->a0[fi] - 1. / prof->a0[fi - 1])))),
               - dbuf * sin(PI * ph / phdistph /
               (1. / prof->a0[fi - 1] + i / fill *
               fabs((1. / prof->a0[fi] - 1. / prof->a0[fi - 1])))),
               lf + (uf - lf) * (fi - 1.) / (nspr - 1.) +
               (uf - lf) / (nspr - 1.) * i / fill,
               dbuf * cos(PI * ph / phdistph /
               (1. / prof->a0[fi - 1] + i / fill *
               fabs((1. / prof->a0[fi] - 1. / prof->a0[fi - 1])))),
               dbuf * sin(PI * ph / phdistph /
               (1. / prof->a0[fi - 1] + i / fill *
               fabs((1. / prof->a0[fi] - 1. / prof->a0[fi - 1])))));
            if (!quiet) Log_Percent("phase distribution diagram",
               (double)fi * fill * ceil(phdistph * (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))) +
               i * ceil(phdistph * (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))) + ph,
               (double)nff * fill * ceil(phdistph * (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))));
            fflush(F);
         }
      }
      if (lf + (uf - lf) * fi / (nspr - 1.) == 0)
      {
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            0., 0., 0., 0., PI, 0.);
         fflush(F);
      }
      else for (ph = 0; ph < ceil(phdistph / prof->a0[fi]); ph++)
      {
         dbuf = .5 / PI * prof->a0[fi] * prof->b0[fi] /
            (pow(prof->b0[fi] *
            cos(PI * ph / phdistph * prof->a0[fi]), 2) +
            pow(prof->a0[fi] *
            sin(PI * ph / phdistph * prof->a0[fi]), 2));
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            lf + (uf - lf) * fi / (nspr - 1.),
            - dbuf * cos(PI * ph / phdistph * prof->a0[fi]),
            - dbuf * sin(PI * ph / phdistph * prof->a0[fi]),
            lf + (uf - lf) * fi / (nspr - 1.),
            dbuf * cos(PI * ph / phdistph * prof->a0[fi]),
            dbuf * sin(PI * ph / phdistph * prof->a0[fi]));
         fflush(F);
      }
      if (!quiet) Log_Percent("phase distribution diagram", (double)fi,
         (double)nspr);
   }
   fclose(F);
   return 0;
}



int PhDist_Colours(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   FILE* F;
   struct String fname;
   char cbuf[MAXCHAR];
   int ibuf;
   double* rgb[4];
   double* data[4];
   int ri;
   int ci = 0;
   int rows;
   int rgbs = IniFile_PhDistColours();
   double lower = DBL_MAX;
   double upper = -DBL_MAX;
   double red;
   double green;
   double blue;
   if (rgbs < 0) return 0;
   if (!quiet)
   {
      printf("\rphase distribution diagram: colours                          ");
      fflush(stdout);
   }
   rgb[0] = (double*)calloc(rgbs, sizeof(double));
   rgb[1] = (double*)calloc(rgbs, sizeof(double));
   rgb[2] = (double*)calloc(rgbs, sizeof(double));
   rgb[3] = (double*)calloc(rgbs, sizeof(double));
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(NULL, -1, -1,
      -1, String_AsChars(&project, cbuf), -1, -1, "ini")),sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 98;
   free(fname.c);
   while (fscanf(F, "%s", cbuf) == 1)
   {
      if (strcmp(cbuf, "phdist:colour") == 0)
      {
         if (fscanf(F, "%lf%lf%lf%lf", &rgb[0][ci], &rgb[1][ci],
            &rgb[2][ci], &rgb[3][ci]) != 4) return 99;
         if (rgb[0][ci] < 0) return 100;
         if (rgb[1][ci] < 0) return 101;
         if (rgb[2][ci] < 0) return 102;
         if (rgb[3][ci] < 0) return 103;
         while (fgetc(F) != '\n');
         ci++;
      }
   }
   fclose(F);
   if (Dataset_NSortAsc(rgb, NULL, 3, 0, 3, -1, -1, 0, rgbs - 1, NULL,
      "phase distribution diagram: initialise") < 0) return 110;
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "phdist", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "phdist", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "phdist", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "phdist", -1, -1, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 104;
   free(fname.c);
   if ((rows = DataFile_CountRows(F)) < 1) return 105;
   rewind(F);
   data[0] = (double*)calloc(rows, sizeof(double));
   data[1] = (double*)calloc(rows, sizeof(double));
   data[2] = (double*)calloc(rows, sizeof(double));
   data[3] = (double*)calloc(rows, sizeof(double));
   for (ri = 0; ri < rows; ri++)
   {
      if (fscanf(F, "%lf%lf%lf", &data[0][ri], &data[1][ri], &data[2][ri]) != 3)
         return 106;
      while (((ibuf = fgetc(F)) != '\n') && (ibuf != EOF));
   }
   fclose(F);
   switch (phdistcoord)
   {
   case 0:
      for (ri = 0; ri < rows; ri++)
      {
         data[3][ri] = data[2][ri];
         if (data[3][ri] < lower) lower = data[3][ri];
         if (data[3][ri] > upper) upper = data[3][ri];
      }
      break;
   case 1:
      for (ri = 0; ri < rows; ri++)
      {
         data[3][ri] = sqrt(pow(data[1][ri], 2) + pow(data[2][ri], 2));
         if (data[3][ri] < lower) lower = data[3][ri];
         if (data[3][ri] > upper) upper = data[3][ri];
      }
      break;
   default:
      return 107;
      break;
   }
   if (phdistcolmodel == 1)
   {
      if (Dataset_NSortAsc(data, NULL, 3, 0, 3, -1, -1, 0, rows - 1, NULL,
         "phase distribution diagram: rank") < 0) return 108;
      for (ri = 0; ri < rows; ri++) data[3][ri] = 1. * ri / rows;
      lower = 0;
      upper = rows - 1;
   }
   if ((F = fopen(cbuf, "w")) == NULL) return 109;
   for (ri = 0; ri < rows; ri++)
   {
      if (data[3][ri] <= rgb[3][0])
      {
         red = rgb[0][0];
         green = rgb[1][0];
         blue = rgb[2][0];
      }
      else if (data[3][ri] >= rgb[3][rgbs - 1])
      {
         red = rgb[0][rgbs - 1];
         green = rgb[1][rgbs - 1];
         blue = rgb[2][rgbs - 1];
      }
      else
      {
         for (ci = 1; (ci < rgbs) &&
            (data[3][ri] > rgb[3][ci]); ci++);
         red = rgb[0][ci - 1] + (rgb[0][ci] - rgb[0][ci - 1]) * 
            (data[3][ri] - rgb[3][ci - 1]) / (rgb[3][ci] - rgb[3][ci - 1]);
         green = rgb[1][ci - 1] + (rgb[1][ci] - rgb[1][ci - 1]) * 
            (data[3][ri] - rgb[3][ci - 1]) / (rgb[3][ci] - rgb[3][ci - 1]);
         blue = rgb[2][ci - 1] + (rgb[2][ci] - rgb[2][ci - 1]) * 
            (data[3][ri] - rgb[3][ci - 1]) / (rgb[3][ci] - rgb[3][ci - 1]);
      }
      fprintf(F, "%24.16lf %24.16lf %24.16lf %8.0lf %24.16lf %3.0lf %3.0lf %3.0lf\n", data[0][ri], data[1][ri],
         data[2][ri], floor(red + .5) + 256. * floor(green + .5) + 
         02936. * floor(blue + .5), data[3][ri], floor(red + .5), floor(green + .5), floor(blue + .5));
      fflush(F);
      if (!quiet) Log_Percent("phase distribution diagram: colours", ri, rows);
   }
   fclose(F);
   free(data[0]);
   free(data[1]);
   free(data[2]);
   free(data[3]);
   free(rgb[0]);
   free(rgb[1]);
   free(rgb[2]);
   free(rgb[3]);
   return 0;
}



int PhDist_Cylindrical(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int fi;
   int ph;
   int fill;
   int i;
   double dbuf;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "phdist", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "phdist", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "phdist", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "phdist", -1, -1, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 96;
   free(fname.c);
   if (lf == 0)
   {
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         0., 0., 0., 0., PI, 0.);
      fflush(F);
   }
   else for (ph = 0; ph < ceil(phdistph / prof->a0[0]); ph++)
   {
      dbuf = .5 / PI * prof->a0[0] * prof->b0[0] /
         (pow(prof->b0[0] * cos(PI * ph / phdistph * prof->a0[0]), 2) +
         pow(prof->a0[0] * sin(PI * ph / phdistph * prof->a0[0]), 2));
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         lf, PI * ph / phdistph * prof->a0[0] - PI, dbuf, lf,
         PI * ph / phdistph * prof->a0[0], dbuf);
      fflush(F);
   }
   for (fi = 1; fi < nspr; fi++)
   {
      fill = (int)ceil(fabs(1. / prof->a0[fi] - 1. / prof->a0[fi - 1]) *
         phdistfill);
      for (i = 1; i < fill; i++)
      {
         if (lf + (uf - lf) * (fi - 1.) / (nspr - 1.) +
            (uf - lf) / (nspr - 1.) * i / fill == 0)
         {
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               0., 0., 0., 0., PI, 0.);
            fflush(F);
         }
         else for (ph = 0; ph < ceil(phdistph * (1. / prof->a0[fi - 1] +
            i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))); ph++)
         {
            dbuf = .5 / PI * (prof->a0[fi - 1] +
               (prof->a0[fi] - prof->a0[fi - 1]) * i / fill) *
               (prof->b0[fi - 1] +
               (prof->b0[fi] - prof->b0[fi - 1]) * i / fill) /
               (pow((prof->b0[fi - 1] +
               (prof->b0[fi] - prof->b0[fi - 1]) * i / fill) *
               cos(PI * ph / phdistph / (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] -
               1. / prof->a0[fi - 1]))), 2) +
               pow((prof->a0[fi - 1] +
               (prof->a0[fi] - prof->a0[fi - 1]) * i / fill) *
               sin(PI * ph / phdistph / (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] -
               1. / prof->a0[fi - 1]))), 2));
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               lf + (uf - lf) * (fi - 1.) / (nspr - 1.) +
               (uf - lf) / (nspr - 1.) * i / fill,
               PI * ph / phdistph / (1. / prof->a0[fi - 1] + i / fill *
               fabs((1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))) - PI,
               dbuf, lf + (uf - lf) * (fi - 1.) / (nspr - 1.) +
               (uf - lf) / (nspr - 1.) * i / fill,
               PI * ph / phdistph / (1. / prof->a0[fi - 1] + i / fill *
               fabs((1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))), dbuf);
            fflush(F);
            if (!quiet) Log_Percent("phase distribution diagram",
               (double)fi * fill * ceil(phdistph * (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))) +
               i * ceil(phdistph * (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))) + ph,
               (double)nff * fill * ceil(phdistph * (1. / prof->a0[fi - 1] +
               i / fill * (1. / prof->a0[fi] - 1. / prof->a0[fi - 1]))));
         }
      }
      if (lf + (uf - lf) * fi / (nspr - 1.) == 0)
      {
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            0., 0., 0., 0., PI, 0.);
         fflush(F);
      }
      else for (ph = 0; ph < ceil(phdistph / prof->a0[fi]); ph++)
      {
         dbuf = .5 / PI * prof->a0[fi] * prof->b0[fi] /
            (pow(prof->b0[fi] *
            cos(PI * ph / phdistph * prof->a0[fi]), 2) +
            pow(prof->a0[fi] *
            sin(PI * ph / phdistph * prof->a0[fi]), 2));
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            lf + (uf - lf) * fi / (nspr - 1.),
            PI * ph / phdistph * prof->a0[fi] - PI, dbuf,
            lf + (uf - lf) * fi / (nspr - 1.),
            PI * ph / phdistph * prof->a0[fi], dbuf);
         fflush(F);
      }
      if (!quiet) Log_Percent("phase distribution diagram", (double)fi,
         (double)nspr);
   }
   fclose(F);
   return 0;
}



int PhDist_Generate(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int err;
   if (!quiet)
   {
      printf("\rphase distribution diagram                                   ");
      fflush(stdout);
   }
   switch (phdistcoord)
   {
   case 0:
      if ((err = PhDist_Cylindrical(ts, prof, ti)) != 0) return err;
      break;
   case 1:
      if ((err = PhDist_Cartesian(ts, prof, ti)) != 0) return err;
      break;
   default:
      return 98;
   }
   if ((phdistcolmodel >= 0) && ((err = PhDist_Colours(ts, prof, ti)) != 0))
      return err;
   return 0;
}


int Preview_Generate(
   struct TS* ts,
   struct Profile* prof,
   struct SigSpec* sigspec,
   struct TS* ocomp,
   struct SigSpec* compspec,
   int ti
   )

{
   int fi;
   int ri;
   double prevsig;
   double currsig;
   double nextsig;
   double sig;
   double r;
   int maxindex = 0;
   struct String fname;
   char cbuf[MAXCHAR];
   struct TS tc;
   struct TS* cdata = (struct TS*)calloc(ncomp, sizeof(struct TS));
   FILE* F;
   int err;
   struct Result testresult;
   testresult.h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   if (!quiet)
      printf("\rpreview                                                      ");
   fflush(stdout);
   tc.d = (double**)calloc(1, sizeof(double*));
   tc.d[0] = (double*)calloc(ts->r, sizeof(double));
   for (fi = 0; fi < ncomp; fi++)
   {
      cdata[fi].fi = cdata[fi].fi;
      cdata[fi].it = 0;
      cdata[fi].d = (double**)calloc(1, sizeof(double*));
      cdata[fi].d[0] = (double*)calloc(ocomp[fi].r, sizeof(double));
      cdata[fi].c = 1;
      cdata[fi].n = 1;
      cdata[fi].r = ocomp[fi].r;
      cdata[fi].cf = (int*)calloc(1, sizeof(int));
      cdata[fi].cf[0] = 1;
      cdata[fi].cw = (int*)calloc(1, sizeof(int));
      cdata[fi].cw[0] = 16;
      cdata[fi].nss = ocomp[fi].nss;
      cdata[fi].ct = ocomp[fi].ct;
      cdata[fi].pi = ocomp[fi].pi;
   }
   for (fi = 0; fi < ncomp; fi++)
   {
      for (ri = 0; ri < ocomp[fi].r; ri++)
         cdata[fi].d[0][ri] = ocomp[fi].d[1][ri];
      cdata[fi].rms = ocomp[fi].rms;
      cdata[fi].ppsc = ocomp[fi].ppsc;
   }
   for (fi = 0; fi < ts->r; fi++) tc.d[0][fi] = ts->d[x][fi];
   tc.rms = ts->rms;
   switch (dmode)
   {
   case 0:
      if ((err = Diff_SpWhite(compspec->a)) != 0)
         return Log_ErrorMessage(err);
      break;
   case 1:
      if ((err = Diff_SpComp(cdata, NULL, compspec)) != 0)
         return Log_ErrorMessage(err);
      for (fi = 0; fi < ocomp[fi].r; fi++)
         cdata[fi].d[0][fi] = ocomp[fi].d[1][fi];
      break;
   case 2:
      if ((err = Diff_SpCompAlign(cdata, NULL, compspec)) != 0)
         return Log_ErrorMessage(err);
      for (fi = 0; fi < ocomp[fi].r; fi++)
         cdata[fi].d[0][fi] = ocomp[fi].d[1][fi];
      break;
   default:
      break;
   }
   SigSpec_SigSpec(ts, &tc, prof, sigspec, compspec);
   currsig = sigspec->sig[0];
   nextsig = sigspec->sig[1];
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "preview", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "preview", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "preview", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "preview", -1, -1, "dat", &fname);
   }
   String_AsChars(&fname, cbuf);
   free(fname.c);
   if ((F = fopen(cbuf, "w")) == NULL) return -75;
   for (fi = 2; fi < nff; fi++)
   {
      testresult.f = lf + fs * fi;
      prevsig = currsig;
      currsig = nextsig;
      nextsig = sigspec->sig[fi];
      if ((currsig >= prevsig) && (currsig >= nextsig))
      {
         r = fs;
         if ((sig = SigSpec_MaxSig(ts, &tc, &testresult, &r, ocomp, cdata,
            sigspec->norm, sigspec->scale)) < 0) return -(int)sig;
         if (sig >= psl)
         {
            maxindex++;
            fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n",
               testresult.f, testresult.sig, testresult.h[0].amp,
               testresult.h[0].th, testresult.h[0].t0);
         }
      }
   }
   fclose(F);
   free(tc.d[0]);
   free(tc.d);
   free(testresult.h);
   for (fi = 0; fi < ncomp; fi++)
   {
      free(cdata[fi].d[0]);
      free(cdata[fi].d);
      free(cdata[fi].cf);
      free(cdata[fi].cw);
   }
   free(cdata);
   return maxindex;
}



int Profile_DFT(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int fi;
   for (fi = 0; fi < nspr; fi++)
   {
      prof->a0[fi] = 1;
      prof->b0[fi] = 1;
      prof->t0[fi] = 0;
      if (!quiet)
         Log_Percent("sampling profile (DFT)", (double)fi, (double)nspr);
   }
   return 0;
}



int Profile_Generate(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   switch (mode)
   {
   case 1:
      return Profile_DFT(ts, prof, ti);
      break;
   case 2:
      return Profile_Lomb(ts, prof, ti);
      break;
   default:
      return Profile_SigSpec(ts, prof, ti);
      break;
   }
   return 0;
}



int Profile_Lomb(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int ri;
   int fi;
   int si;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double df = (uf - lf) / (nspr - 1.);
   double* sinth0;
   double* costh0;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   int perc = 0;
   if (debug == 0) printf("Profile_Lomb: calloc sinth0\n");
   sinth0 = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("Profile_Lomb: calloc costh0\n");
   costh0 = (double*)calloc(nspr, sizeof(double));
   if (!quiet)
   {
      printf("\rsampling profile (L-S): initialise arrays                    ");
      fflush(stdout);
   }
   for (fi = 0; fi < nspr; fi++)
   {
      sinth0[fi] = 0;
      costh0[fi] = 0;
   }
   if (!quiet)
   {
      printf("\rsampling profile (L-S): orientation of rms error ellipse     ");
      fflush(stdout);
   }
   for (si = 1; si <= ts->nss; si++)
   {
      for (ri = 0; ri < ts->r; ri++)
         if ((ts->nss < 2) || (ts->ss[ri] == si))
      {
         curcos = cos(4. * PI * lf * ts->d[t][ri]);
         cursin = sin(4. * PI * lf * ts->d[t][ri]);
         dcos = cos(4. * PI * df * ts->d[t][ri]);
         dsin = sin(4. * PI * df * ts->d[t][ri]);
         for (fi = 0; fi < nspr; fi++)
         {
            costh0[fi] += ts->w[ri] * curcos;
            sinth0[fi] += ts->w[ri] * cursin;
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet) Log_Percent
               ("sampling profile (L-S): orientation of rms error ellipse",
               (double)++perc, (double)nspr * ts->r);
         }
      }
   }
   if (!quiet)
   {
      printf("\rsampling profile (L-S): axes of rms error ellipse            ");
      fflush(stdout);
   }
   for (fi = 0; fi < nspr; fi++)
   {
      prof->t0[fi] = .5 * atan2(sinth0[fi], costh0[fi]);
      while (prof->t0[fi] < 0) prof->t0[fi] += PI;
      sinth0[fi] = 0;
      costh0[fi] = 0;
   }
   perc = 0;
   for (si = 1; si <= ts->nss; si++)
   {
      for (ri = 0; ri < ts->r; ri++)
         if ((ts->nss < 2) || (ts->ss[ri] == si))
      {
         curcos = cos(2. * PI * lf * ts->d[t][ri]);
         cursin = sin(2. * PI * lf * ts->d[t][ri]);
         dcos = cos(2. * PI * df * ts->d[t][ri]);
         dsin = sin(2. * PI * df * ts->d[t][ri]);
         for (fi = 0; fi < nspr; fi++)
         {
            dbuf = curcos * cos(prof->t0[fi]) + cursin * sin(prof->t0[fi]);
            prof->a0[fi] += ts->w[ri] * pow(dbuf, 2);
            dbuf = cursin * cos(prof->t0[fi]) - curcos * sin(prof->t0[fi]);
            prof->b0[fi] += ts->w[ri] * pow(dbuf, 2);
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet) Log_Percent
               ("sampling profile (L-S): axes of rms error ellipse",
               (double)++perc, (double)nspr * ts->r);
         }
      }
   }
   if (!quiet)
   {
      printf("\rsampling profile (L-S): finalise                             ");
      fflush(stdout);
   }
   for (fi = 0; fi < nspr; fi++)
   {
      if (lf + fs * fi == 0)
      {
         prof->a0[fi] = 0;
         prof->b0[fi] = 0;
         prof->t0[fi] = 0;
      }
      else if (prof->a0[fi] > prof->b0[fi])
      {
         dbuf = sqrt(2. * prof->a0[fi] / ts->r);
         prof->a0[fi] = sqrt(2. * prof->b0[fi] / ts->r);
         prof->b0[fi] = dbuf;
         prof->t0[fi] += .5 * PI;
      }
      else
      {
         prof->a0[fi] = sqrt(2. * prof->a0[fi] / ts->r);
         prof->b0[fi] = sqrt(2. * prof->b0[fi] / ts->r);
      }
      while (prof->t0[fi] < 0) prof->t0[fi] += PI;
      while (prof->t0[fi] > PI) prof->t0[fi] -= PI;
      if (!quiet) Log_Percent("sampling profile (L-S): finalise",
         (double)fi + 1, (double)nspr);
   }
   if (makeprof > 0)
   {
      if (trnum > 1)
      {
         if (debug == 0) printf("Profile_Lomb: calloc fname.c\n");
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, ts->fi, -1, -1, "profile", ti, -1,
            "dat")), sizeof(int));
         File_Name(&project, ts->fi, -1, -1, "profile", ti, -1, "dat", &fname);
      }
      else
      {
         if (debug == 0) printf("Profile_Lomb: calloc fname.c\n");
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, ts->fi, -1, -1, "profile", -1, -1,
            "dat")), sizeof(int));
         File_Name(&project, ts->fi, -1, -1, "profile", -1, -1, "dat", &fname);
      }
      String_AsChars(&fname, cbuf);
      if (debug == 0) printf("Profile_Lomb: free fname.c\n");
      free(fname.c);
      if ((F = fopen(cbuf, "w")) == NULL) return 70;
      for (fi = 0; fi < nspr; fi++)
         fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf\n",
         lf + fi * df, prof->a0[fi], prof->b0[fi], prof->t0[fi]);
      fclose(F);
   }
   if (debug == 0) printf("Profile_Lomb: free costh0\n");
   free(costh0);
   if (debug == 0) printf("Profile_Lomb: free sinth0\n");
   free(sinth0);
   return 0;
}



int Profile_SigSpec(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int ri;
   int fi;
   int si;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double df = (uf - lf) / (nspr - 1.);
   double* axis1;
   double* axis2;
   double* sinth0;
   double* costh0;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   int perc = 0;
   if (debug == 0) printf("Profile_SigSpec: calloc axis1\n");
   axis1 = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("Profile_SigSpec: calloc axis2\n");
   axis2 = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("Profile_SigSpec: calloc sinth0\n");
   sinth0 = (double*)calloc(nspr, sizeof(double));
   if (debug == 0) printf("Profile_SigSpec: calloc costh0\n");
   costh0 = (double*)calloc(nspr, sizeof(double));
   if (!quiet)
   {
      printf("\rsampling profile: initialise arrays                          ");
      fflush(stdout);
   }
   for (fi = 0; fi < nspr; fi++)
   {
      sinth0[fi] = 0;
      costh0[fi] = 0;
   }
   if (!quiet)
   {
      printf("\rsampling profile: orientation of rms error ellipse           ");
      fflush(stdout);
   }
   for (si = 1; si <= ts->nss; si++)
   {
      for (fi = 0; fi < nspr; fi++)
      {
         prof->t0[fi] = 0;
         prof->b0[fi] = 0;
         axis1[fi] = 0;
         axis2[fi] = 0;
      }
      for (ri = 0; ri < ts->r; ri++)
         if ((ts->nss < 2) || (ts->ss[ri] == si))
      {
         curcos = cos(2. * PI * lf * ts->d[t][ri]);
         cursin = sin(2. * PI * lf * ts->d[t][ri]);
         dcos = cos(2. * PI * df * ts->d[t][ri]);
         dsin = sin(2. * PI * df * ts->d[t][ri]);
         for (fi = 0; fi < nspr; fi++)
         {
            axis1[fi] += ts->w[ri] * curcos;
            axis2[fi] += ts->w[ri] * cursin;
            prof->t0[fi] += ts->w[ri] * (pow(curcos, 2) - pow(cursin, 2));
            prof->b0[fi] += ts->w[ri] * curcos * cursin;
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet)
               Log_Percent("sampling profile: orientation of rms error ellipse",
               (double)++perc, (double)nspr * ts->r);
         }
      }
      for (fi = 0; fi < nspr; fi++)
      {
         sinth0[fi] +=
            2. * (ts->nsse[si - 1] * prof->b0[fi] - axis1[fi] * axis2[fi]);
         costh0[fi] += ts->nsse[si - 1] * prof->t0[fi] - pow(axis1[fi], 2) +
            pow(axis2[fi], 2);
      }
   }
   if (!quiet)
   {
      printf("\rsampling profile: axes of rms error ellipse                  ");
      fflush(stdout);
   }
   for (fi = 0; fi < nspr; fi++)
   {
      prof->t0[fi] = .5 * atan2(sinth0[fi], costh0[fi]);
      if (prof->t0[fi] < 0) prof->t0[fi] += PI;
      sinth0[fi] = 0;
      costh0[fi] = 0;
   }
   perc = 0;
   for (si = 1; si <= ts->nss; si++)
   {
      for (fi = 0; fi < nspr; fi++)
      {
         axis1[fi] = 0;
         axis2[fi] = 0;
         prof->b0[fi] = 0;
      }
      for (ri = 0; ri < ts->r; ri++)
         if ((ts->nss < 2) || (ts->ss[ri] == si))
      {
         curcos = cos(2. * PI * lf * ts->d[t][ri]);
         cursin = sin(2. * PI * lf * ts->d[t][ri]);
         dcos = cos(2. * PI * df * ts->d[t][ri]);
         dsin = sin(2. * PI * df * ts->d[t][ri]);
         for (fi = 0; fi < nspr; fi++)
         {
            dbuf = curcos * cos(prof->t0[fi]) + cursin * sin(prof->t0[fi]);
            axis1[fi] += ts->w[ri] * dbuf;
            axis2[fi] += ts->w[ri] *
               (cursin * cos(prof->t0[fi]) - curcos * sin(prof->t0[fi]));
            prof->b0[fi] += ts->w[ri] * pow(dbuf, 2);
            dbuf = curcos * dcos - cursin * dsin;
            cursin = cursin * dcos + curcos * dsin;
            curcos = dbuf;
            if (!quiet)
               Log_Percent("sampling profile: axes of rms error ellipse",
               (double)++perc, (double)nspr * ts->r);
         }
      }
      for (fi = 0; fi < nspr; fi++)
      {
         costh0[fi] += prof->b0[fi] - pow(axis1[fi], 2) / ts->nsse[si - 1];
         sinth0[fi] += ts->nsse[si - 1] -
            prof->b0[fi] - pow(axis2[fi], 2) / ts->nsse[si - 1];
      }
   }
   if (!quiet)
   {
      printf("\rsampling profile: finalise                                   ");
      fflush(stdout);
   }
   for (fi = 0; fi < nspr; fi++)
   {
      if (lf + fs * fi == 0)
      {
         prof->a0[fi] = 0;
         prof->b0[fi] = 0;
         prof->t0[fi] = 0;
      }
      else if (fabs(costh0[fi]) > fabs(sinth0[fi]))
      {
         prof->a0[fi] = sqrt(2. * fabs(sinth0[fi]) / ts->r);
         prof->b0[fi] = sqrt(2. * fabs(costh0[fi]) / ts->r);
         prof->t0[fi] += .5 * PI;
      }
      else
      {
         prof->a0[fi] = sqrt(2. * fabs(costh0[fi]) / ts->r);
         prof->b0[fi] = sqrt(2. * fabs(sinth0[fi]) / ts->r);
      }
      while (prof->t0[fi] < 0) prof->t0[fi] += PI;
      while (prof->t0[fi] > PI) prof->t0[fi] -= PI;
      if (!quiet) Log_Percent("sampling profile: finalise", (double)fi + 1,
         (double)nspr);
   }
   if (makeprof > 0)
   {
      if (trnum > 1)
      {
         if (debug == 0) printf("Profile_SigSpec: calloc fname.c\n");
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, ts->fi, -1, -1, "profile", ti, -1,
            "dat")), sizeof(int));
         File_Name(&project, ts->fi, -1, -1, "profile", ti, -1, "dat", &fname);
      }
      else
      {
         if (debug == 0) printf("Profile_SigSpec: calloc fname.c\n");
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, ts->fi, -1, -1, "profile", -1, -1,
            "dat")), sizeof(int));
         File_Name(&project, ts->fi, -1, -1, "profile", -1, -1, "dat", &fname);
      }
      String_AsChars(&fname, cbuf);
      if (debug == 0) printf("Profile_SigSpec: free fname.c\n");
      free(fname.c);
      if ((F = fopen(cbuf, "w")) == NULL) return 70;
      for (fi = 0; fi < nspr; fi++)
         fprintf(F, "%24.16lf %24.16lf %24.16lf %24.16lf\n",
         lf + fi * df, prof->a0[fi], prof->b0[fi], prof->t0[fi]);
      fclose(F);
   }
   if (debug == 0) printf("Profile_SigSpec: free axis1\n");
   free(axis1);
   if (debug == 0) printf("Profile_SigSpec: free axis2\n");
   free(axis2);
   if (debug == 0) printf("Profile_SigSpec: free costh0\n");
   free(costh0);
   if (debug == 0) printf("Profile_SigSpec: free sinth0\n");
   free(sinth0);
   return 0;
}


int Result_Copy(
   struct Result* from,
   struct Result* to
   )

{
   int hi;
   to->f = from->f;
   to->sig = from->sig;
   to->csig = from->csig;
   to->rms = from->rms;
   to->ppsc = from->ppsc;
   for (hi = 0; hi <= oharm; hi++)
   {
      to->h[hi].sig = from->h[hi].sig;
      to->h[hi].amp = from->h[hi].amp;
      to->h[hi].th = from->h[hi].th;
      to->h[hi].a0 = from->h[hi].a0;
      to->h[hi].b0 = from->h[hi].b0;
      to->h[hi].t0 = from->h[hi].t0;
   }
   return 0;
}



int Result_Display(
   struct Result* result
   )

{
   printf("\nf %lg sig %lg rms %lg", result->f, result->sig, result->rms);
   fflush(stdout);
   return 0;
}



int Result_Reset(
   struct Result* result
   )

{
   int hi;
   result->f = 0;
   result->sig = 0;
   result->csig = 0;
   result->rms = 0;
   result->ppsc = 0;
   for (hi = 0; hi <= oharm; hi++)
   {
      result->h[hi].sig = 0;
      result->h[hi].amp = 0;
      result->h[hi].th = 0;
      result->h[hi].a0 = 0;
      result->h[hi].b0 = 0;
      result->h[hi].t0 = 0;
   }
   return 0;
}



int Result_TimeShift(
   struct Result* result,
   double shift
   )

{
   int hi;
   for (hi = 0; hi <= oharm; hi++)
   {
      result->h[hi].th += 2. * PI * (hi + 1.) * result->f * shift;
      while (result->h[hi].th < 0) result->h[hi].th += 2. * PI;
      while (result->h[hi].th >= 2. * PI) result->h[hi].th -= 2. * PI;
   }
   return 0;
}



int Result_Write(
   struct TS* ts,
   struct Result* result,
   int ti,
   int rows
   )

{
   struct String fname;
   struct String hbuf;
   struct String hname;
   FILE* F;
   char cbuf[MAXCHAR];
   int err;
   int ri;
   int pos;
   if (ts->it < 0)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, pos = 
         File_NameLength(&project,
         ts->fi, -1, -1, "result", -1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "result", -1, ti, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, pos = 
         File_NameLength(&project,
         ts->fi, -1, -1, "r", rows, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "r", rows, ti, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return -146;
   for (ri = 0; (ri < rows) && ((it < 0) || (ri < it)) &&
      ((sl < 0) || (result[ri].sig >= sl)) &&
      ((csl < 0) || (result[ri].csig >= csl)); ri++)
   {
      fprintf(F,
         "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n",
         result[ri].f, result[ri].sig / (oharm + 1.), result[ri].h[0].amp,
         result[ri].h[0].th, result[ri].rms, result[ri].ppsc,
         result[ri].csig / (oharm + 1.), result[ri].h[0].t0);
      if (oharm > 0)
      {
         hbuf.c = (int*)calloc(String_SetLength(&hbuf, ITDIGITS), sizeof(int));
         NumFormat_FixDigits(ri + 1, &hbuf);
         hname.c = (int*)calloc(String_SetLength(&hname, pos + 2 + ITDIGITS),
            sizeof(int));
         pos = String_Copy(&fname, &hname, 0);
         pos = String_SetChar(&hname, '-', pos - 4);
         pos = String_SetChar(&hname, 'h', pos);
         pos = String_Copy(&hbuf, &hname, pos);
         pos = String_SetChar(&hname, '.', pos);
         pos = String_SetChar(&hname, 'd', pos);
         pos = String_SetChar(&hname, 'a', pos);
         pos = String_SetChar(&hname, 't', pos);
         free(hbuf.c);
         Harmonics_Write(&result[ri], &hname);
         free(hname.c);
      }
   }
   free(fname.c);
   if (result[ri].sig > 0) fprintf(F,
      "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n",
      0., result[ri].sig, 0., 0., result[ri].rms, result[ri].ppsc,
      result[ri].csig / (oharm + 1.), 0.);
   fclose(F);
   return ri;
}



int Rnd_Init(
   int* s
   )

{
   FILE* F;
   char* pch;
   int err;
   struct String fname;
   char cbuf[MAXCHAR];
   String_AsChars(&project, cbuf);
   pch = strtok(cbuf, "/");
   while (pch != NULL)
   {
      sprintf (cbuf, "%s", pch);
      pch = strtok(NULL, "/");
   }
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project, -1,
      -1, -1, cbuf, -1, -1, "rnd")),sizeof(int));
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(NULL, -1, -1,
      -1, String_AsChars(&project, cbuf), -1, -1, "rnd")),sizeof(int));
   File_Name(NULL, -1, -1, -1, String_AsChars(&project, cbuf), -1, -1, "rnd",
      &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL)
   {
      free(fname.c);
      if (!quiet)
         printf("random number generator: system time initialisation\n");
      *s = (int)time(NULL);
   }
   else
   {
      free(fname.c);
      if (!quiet) printf("random number generator: file %s\n", cbuf);
      err = fscanf(F, "%i", s);
      fclose(F);
   }
   return 0;
}



int Rnd_Write(
   int* s
   )

{
   FILE* F;
   struct String fname;
   char cbuf[MAXCHAR];
   char* pch;
   String_AsChars(&project, cbuf);
   pch = strtok(cbuf, "/");
   while (pch != NULL)
   {
      sprintf (cbuf, "%s", pch);
      pch = strtok(NULL, "/");
   }
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project, -1,
      -1, -1, cbuf, -1, -1, "rnd")),sizeof(int));
   File_Name(&project, -1, -1, -1, cbuf, -1, -1, "rnd", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL)
   {
      free(fname.c);
      Log_ErrorMessage(156);
   }
   else
   {
      free(fname.c);
      fprintf(F, "%i", *s);
      fclose(F);
   }
   return 0;
}
int SigSpec_Cascade(
   struct TS* ts,
   struct Profile* prof,
   struct TS* cdata,
   struct SigSpec* compspec,
   int ti
   )

{
   int ii;                      
   int ai;                      
   int ri;                      
   int fi;
   int err;
   double sig = DBL_MAX;
   double csig = DBL_MAX;
   int count;
   int rows;
   struct SigSpec lastsp;
   struct TS lastout;
   struct Result* bk;
   struct TS* res =
      (struct TS*)calloc(aaa + 1, sizeof(struct TS));
   struct TS** cres =
      (struct TS**)calloc(aaa + 1, sizeof(struct TS*));
   struct SigSpec* sp =
      (struct SigSpec*)calloc(aaa + 1, sizeof(struct SigSpec));
   struct Result* result =
      (struct Result*)calloc(aaa + 1, sizeof(struct Result));
   struct Result* opt =
      (struct Result*)calloc(1, sizeof(struct Result));
   MultiSine_ClearTracks(ts, ti);
   lastout.fi = ts->fi;
   lastout.it = -1;
   lastout.d = (double**)calloc(1, sizeof(double*));
   lastout.d[0] = (double*)calloc(ts->r, sizeof(double));
   lastout.c = 1;
   lastout.n = 1;
   lastout.r = ts->r;
   lastout.cf = (int*)calloc(1, sizeof(int));
   lastout.cf[0] = 1;
   lastout.cw = (int*)calloc(1, sizeof(int));
   lastout.cw[0] = 16;
   lastout.nss = ts->nss;
   lastout.nsse = (int*)calloc(ts->nss, sizeof(int));
   for (ri = 0; ri < ts->nss; ri++) lastout.nsse[ri] = ts->nsse[ri];
   lastout.ct = ts->ct;
   lastout.pi = ts->pi;
   for (ai = 0; ai <= aaa; ai++)
   {
      res[ai].fi = ts->fi;
      res[ai].it = ai;
      res[ai].d = (double**)calloc(1, sizeof(double*));
      res[ai].d[0] = (double*)calloc(ts->r, sizeof(double));
      res[ai].c = 1;
      res[ai].n = 1;
      res[ai].r = ts->r;
      res[ai].cf = (int*)calloc(1, sizeof(int));
      res[ai].cf[0] = 1;
      res[ai].cw = (int*)calloc(1, sizeof(int));
      res[ai].cw[0] = 16;
      res[ai].nss = ts->nss;
      res[ai].nsse = (int*)calloc(ts->nss, sizeof(int));
      for (ri = 0; ri < ts->nss; ri++) res[ai].nsse[ri] = ts->nsse[ri];
      res[ai].ct = ts->ct;
      res[ai].pi = ts->pi;
      result[ai].h =
         (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   }
   opt[0].h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   for (ri = 0; ri < ts->r; ri++) res[0].d[0][ri] = ts->d[x][ri];
   res[0].rms = ts->rms;
   res[0].ppsc = ts->ppsc;
   for (ai = 0; ai <= aaa; ai++)
   {
      sp[ai].a = (double*)calloc(nspr, sizeof(double));
      sp[ai].b = (double*)calloc(nspr, sizeof(double));
      sp[ai].sig = (double*)calloc(nspr, sizeof(double));
      if (dmode > 0)
      {
         sp[ai].ca = (double*)calloc(nspr, sizeof(double));
         sp[ai].cb = (double*)calloc(nspr, sizeof(double));
      }
      cres[ai] = (struct TS*)calloc(ncomp, sizeof(struct TS));
      for (fi = 0; fi < ncomp; fi++)
      {
         cres[ai][fi].fi = cdata[fi].fi;
         cres[ai][fi].it = ai;
         cres[ai][fi].d = (double**)calloc(1, sizeof(double*));
         cres[ai][fi].d[0] = (double*)calloc(cdata[fi].r, sizeof(double));
         cres[ai][fi].c = 1;
         cres[ai][fi].n = 1;
         cres[ai][fi].r = cdata[fi].r;
         cres[ai][fi].cf = (int*)calloc(1, sizeof(int));
         cres[ai][fi].cf[0] = 1;
         cres[ai][fi].cw = (int*)calloc(1, sizeof(int));
         cres[ai][fi].cw[0] = 16;
         cres[ai][fi].nss = cdata[fi].nss;
         cres[ai][fi].ct = cdata[fi].ct;
         cres[ai][fi].pi = cdata[fi].pi;
      }
   }
   lastsp.a = (double*)calloc(nspr, sizeof(double));
   lastsp.b = (double*)calloc(nspr, sizeof(double));
   lastsp.sig = (double*)calloc(nspr, sizeof(double));
   if (dmode > 0)
   {
      lastsp.ca = (double*)calloc(nspr, sizeof(double));
      lastsp.cb = (double*)calloc(nspr, sizeof(double));
   }
   for (fi = 0; fi < ncomp; fi++)
   {
      for (ri = 0; ri < cdata[fi].r; ri++)
         cres[0][fi].d[0][ri] = cdata[fi].d[1][ri];
      cres[0][fi].rms = cdata[fi].rms;
      cres[0][fi].ppsc = cdata[fi].ppsc;
   }
   if ((trmode >= 0) && ((err = TS_Write(ts, &res[0], ti)) != 0))
      return err;
   for (ii = 0; ((it < 0) || (ii < it)) && ((sl < 0) ||
      (sig / (oharm + 1.) >= sl)) && ((csl < 0) ||
      (csig / (oharm + 1.) >= csl)); ii += aaa)
   {
      if ((err = AntiAlC_Cascade(ts, res, prof, sp, result, cdata, cres,
         compspec, ii)) != 0) return err;
      if (!quiet) for (ai = 0; (ai < aaa) && ((it < 0) || (ii + ai - 1 < it)) &&
         ((sl < 0) || (ii + ai == 0) ||
         (result[ii + ai - 1].sig / (oharm + 1.) >= sl)) &&
         ((csl < 0) || (ii + ai == 0) ||
         (result[ii + ai - 1].csig / (oharm + 1.) >= csl)); ai++)
      {
         printf("\r%4i freq %lg  sig %lg  rms %lg  csig %lg                \n",
            ii + ai + 1, result[ii + ai].f,
            result[ii + ai].sig / (oharm + 1.),
            result[ii + ai].rms, result[ii + ai].csig / (oharm + 1.));
         fflush(stdout);
      }
      rows = 0;
      for (ai = 0; (ai < aaa) && ((it < 0) || (ii + ai < it)) &&
         ((sl < 0) || (ii + ai == 0) ||
         (result[ii + ai].sig / (oharm + 1.) >= sl)) &&
         ((csl < 0) || (ii + ai == 0) ||
         (result[ii + ai].csig / (oharm + 1.) >= csl)); ai++)
      {
         if ((err = MultiSine_Fit(ts, &res[ai + 1], result, t, x,
            ii + ai + 1)) != 0) return err;
         if ((ii >= 0) && (ii + ai < nresults) && (1. * (ii + ai + 1) /
            stresults == floor(1. * (ii + ai + 1) / stresults)))
         {
            if ((rows = Result_Write(ts, result, ti, ii + ai + 1)) < 0)
               return -rows;
            rows += 1 - ii;
         }
         if ((ii >= 0) && (ii + ai <= nsp) &&
            (1. * (ii + ai) / stsp == floor(1. * (ii + ai) / stsp)) &&
            ((err = Spec_Write(&res[ai], prof, &sp[ai], ti)) != 0))
            return err;
         if ((ii >= 0) && (ii + ai <= npd) &&
            (1. * (ii + ai) / stpd == floor(1. * (ii + ai) / stpd)) &&
            ((err = PhD_Generate(ts, &res[ai], ti)) != 0))
            return err;
         if ((ii >= 0) && (ii + ai <= nc) && (co >= 0) &&
            (1. * (ii + ai) / stc == floor(1. * (ii + ai) / stc)) &&
            ((err = Correlogram_Generate(ts, &res[ai], ti)) != 0))
            return err;
         if ((ii >= 0) && (ii + ai <= nmst) && (1. * (ii + ai) /
            stmst == floor(1. * (ii + ai) / stmst)) &&
            ((err = MultiSine_WriteTracks(ts, result, ti, ii + ai + 1)) != 0))
            return err;
         if ((ii >= 0) && (ii + ai <= nmsp) && (1. * (ii + ai + 1) /
            stmsp == floor(1. * (ii + ai + 1) / stmsp)) &&
            ((err = MultiSine_WriteProfiles(ts, &res[ai + 1], result, ti,
            ii + ai + 1)) != 0)) return err;
         if ((ii >= 0) && (ii + ai < nres) &&
            (1. * (ii + ai + 1) / stres == floor(1. * (ii + ai + 1) / stres)) &&
            ((err = TS_Write(ts, &res[ai + 1], ti)) != 0))
            return err;
         if ((ii >= 0) && (ii + ai < nph) && (1. * (ii + ai + 1) /
            stph == floor(1. * (ii + ai + 1) / stph)) &&
            (PhaseDiagram_Generate(ts, &res[ai], &res[ai + 1], result, ti,
            ii + ai + 1) != 0)) return err;
         for (fi = 0; fi < ncomp; fi++) for (ri = 0; ri < cres[0][fi].r; ri++)
            cres[0][fi].d[0][ri] = cres[aaa][fi].d[0][ri];
         for (ri = 0; ri <= ii; ri++) free(opt[ri].h);
         free(opt);
         opt = (struct Result*)calloc(ii + aaa + 1, sizeof(struct Result));
         for (ri = 0; ri <= ii + aaa; ri++)
         {
            opt[ri].h =
               (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
            Result_Copy(&result[ri], &opt[ri]);
         }
         for (ri = 0; ri < ts->r; ri++) lastout.d[0][ri] = res[ai + 1].d[0][ri];
      }
      for (ri = 0; ri < ts->r; ri++)
         res[0].d[0][ri] = res[aaa].d[0][ri];
      sig = result[ii + aaa - 1].sig;
      csig = result[ii + aaa - 1].csig;
      bk = (struct Result*)calloc(ii + aaa + 1, sizeof(struct Result));
      for (ai = 0; ai <= ii + aaa; ai++)
      {
         bk[ai].h =
            (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
         Result_Copy(&result[ai], &bk[ai]);
      }
      for (ai = 0; ai <= ii + aaa; ai++) free(result[ai].h);
      free(result);
      result = (struct Result*)calloc((count = ii + 2 * aaa + 1),
         sizeof(struct Result));
      for (ai = 0; ai < count; ai++) result[ai].h =
            (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
      for (ri = 0; ri < ii + aaa; ri++) Result_Copy(&bk[ri], &result[ri]);
      for (ai = 0; ai <= ii + aaa; ai++) free(bk[ai].h);
      free(bk);
   }
   if ((it >= 0) && (ii >= it)) rows = 0;
   if (nsp >= 0)
   {
      for (ri = 0; ri < nspr; ri++)
      {
         lastsp.a[ri] = sp[rows].a[ri];
         lastsp.b[ri] = sp[rows].b[ri];
         lastsp.sig[ri] = sp[rows].sig[ri];
      }
      if ((err = Spec_Write(&lastout, prof, &lastsp, ti)) != 0)
         return err;
   }
   if ((nres >= 0) && ((err = TS_Write(ts, &lastout, ti)) != 0))
      return err; 
   if ((nc >= 0) && (co >= 0) && (err = Correlogram_Generate(ts, &lastout,
      ti)) != 0) return err;
   opt[ii + rows - 1].sig = result[ii + rows - 1].sig;
   Result_Write(&lastout, opt, ti, ii + rows);
   for (ai = 0; ai <= aaa; ai++)
   {
      free(res[ai].d[0]);
      free(res[ai].d);
      free(res[ai].cf);
      free(res[ai].cw);
      free(res[ai].nsse);
      for (fi = 0; fi < ncomp; fi++)
      {
         free(cres[ai][fi].d[0]);
         free(cres[ai][fi].d);
         free(cres[ai][fi].cf);
         free(cres[ai][fi].cw);
      }
      free(cres[ai]);
      free(sp[ai].a);
      free(sp[ai].b);
      free(sp[ai].sig);
      if (dmode > 0)
      {
         free(sp[ai].ca);
         free(sp[ai].cb);
      }
   }
   for (ai = 0; ai <= ii + aaa; ai++) free(result[ai].h);
   for (ai = 0; (ai == 0) || (ai <= ii - aaa); ai++) free(opt[ai].h);
   free(res);
   free(cres);
   free(sp);
   free(result);
   free(opt);
   free(lastout.d[0]);
   free(lastout.d);
   free(lastout.cf);
   free(lastout.cw);
   free(lastout.nsse);
   free(lastsp.a);
   free(lastsp.b);
   free(lastsp.sig);
   if (dmode > 0)
   {
      free(lastsp.ca);
      free(lastsp.cb);
   }
fflush(stdout);
   return 0;
}



double SigSpec_Copy(
   struct SigSpec* from,
   struct SigSpec* to
   )

{
   int i;
   for (i = 0; i < nspr; i++)
   {
      to->a[i] = from->a[i];
      to->b[i] = from->b[i];
      to->sig[i] = from->sig[i];
      if (dmode > 0)
      {
         to->ca[i] = from->ca[i];
         to->cb[i] = from->cb[i];
      }
   }
   return 0;
}



double SigSpec_CSig(
   double csig,
   double sig
   )

{
   if (csig <= 0) return sig;
   if (log10((1. - pow(10, - csig)) * (1. - pow(10, - sig))) != 0)
      return - log10(1. - (1. - pow(10, - csig)) * (1. - pow(10, - sig)));
   if (csig < sig) return csig;
   return sig;
}



double SigSpec_DFT(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   struct TS* ocomp,
   struct TS* cdata,
   double norm,
   double scale
   )

{
   int hi;
   int ri;
   double cossum;
   double cos2sum;
   double cossum2;
   double sinsum;
   double sin2sum;
   double sinsum2;
   double xcossum;
   double xsinsum;
   double axis1;
   double axis2;
   double smaj;
   double smin;
   double n1;
   double n2;
   double a;
   double b;
   result->sig = 0;
   for (hi = 0; hi <= oharm; hi++)
   {
      cossum = 0;
      cos2sum = 0;
      cossum2 = 0;
      sinsum = 0;
      sin2sum = 0;
      sinsum2 = 0;
      xcossum = 0;
      xsinsum = 0;
      a = 0;
      b = 0;
      for (ri = 0; ri < orig->r; ri++)
      {
         xcossum += orig->w[ri] * ts->d[0][ri] *
            cos(2. * PI * (hi + 1.) * result->f * orig->d[t][ri]);
         xsinsum += orig->w[ri] * ts->d[0][ri] *
            sin(2. * PI * (hi + 1.) * result->f * orig->d[t][ri]);
         if ((!quiet) && (aad < 2))
            Log_Percent("DFT amplitude spectrum: Fourier Coefficients",
            (double)ri, (double)orig->r);
      }
      a = xcossum / orig->r;
      b = xsinsum / orig->r;
      result->h[hi].amp = 2. * sqrt(pow(a, 2) + pow(b, 2));
      result->h[hi].th = atan2(b, a);
      result->h[hi].t0 = 0;
      result->h[hi].a0 = 1;
      result->h[hi].b0 = 1;
      Diff_Level(ocomp, cdata, (hi + 1.) * result->f, ts, &n1, &n2);
      if (dmode > 1)
      {
         n2 = .5 * n1 * sin(result->h[hi].th) * sqrt(norm);
         n1 = .5 * n1 * cos(result->h[hi].th) * sqrt(norm);
      }
      else if (dmode > 0)
      {
         n1 *= sqrt(norm);
         n2 *= sqrt(norm);
      }
      if ((dmode == 2) && (pow(result->h[hi].amp, 2) <
         4. * (pow(n1, 2) + pow(n2, 2)))) result->h[hi].sig = 0;
      else if (ncomp == 0) result->h[hi].sig = orig->r * LG_E /
         pow(ts->rms, 2) * norm / scale *
         (pow((a - n1) + (b - n2) * sin(result->h[hi].t0), 2) / result->h[hi].a0 +
         pow((a - n1) * sin(result->h[hi].t0) - (b - n2) *
         cos(result->h[hi].t0), 2) / result->h[hi].b0);
      else result->h[hi].sig = orig->r * LG_E / pow(ts->rms, 2) *
         norm / scale * (pow(a - n1, 2) + pow(b - n2, 2)) / (1. + 1. / ncomp);
      result->sig += result->h[hi].sig;
   }
   return result->sig;
}



double SigSpec_Lomb(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   struct TS* ocomp,
   struct TS* cdata,
   double norm,
   double scale
   )

{
   int hi;
   int ri;
   double cossum;
   double cos2sum;
   double cossum2;
   double sinsum;
   double sin2sum;
   double sinsum2;
   double xcossum;
   double xsinsum;
   double axis1;
   double axis2;
   double smaj;
   double smin;
   double n1;
   double n2;
   double a;
   double b;
   result->sig = 0;
   for (hi = 0; hi <= oharm; hi++)
   {
      cossum = 0;
      cos2sum = 0;
      cossum2 = 0;
      sinsum = 0;
      sin2sum = 0;
      sinsum2 = 0;
      xcossum = 0;
      xsinsum = 0;
      a = 0;
      b = 0;
      for (ri = 0; ri < orig->r; ri++)
      {
         cossum2 += orig->w[ri] * cos(4. * PI * (hi + 1.) * result->f *
            orig->d[t][ri]);
         sinsum2 += orig->w[ri] * sin(4. * PI * (hi + 1.) * result->f *
            orig->d[t][ri]);
         xcossum += orig->w[ri] * ts->d[0][ri] *
            cos(2. * PI * (hi + 1.) * result->f * orig->d[t][ri]);
         xsinsum += orig->w[ri] * ts->d[0][ri] *
            sin(2. * PI * (hi + 1.) * result->f * orig->d[t][ri]);
         if ((!quiet) && (aad < 2))
            Log_Percent("LS periodogram: Fourier Coefficients",
            (double)ri, (double)orig->r);
      }
      a = xcossum / orig->r;
      b = xsinsum / orig->r;
      result->h[hi].amp = 2. * sqrt(pow(a, 2) + pow(b, 2));
      result->h[hi].th = atan2(b, a);
      result->h[hi].t0 = .5 * atan2(sinsum2, cossum2);
      if (result->h[hi].t0 < 0) result->h[hi].t0 += PI;
      cossum = 0;
      cos2sum = 0;
      sinsum = 0;
      for (ri = 0; ri < orig->r; ri++) cos2sum += orig->w[ri] *
         pow(cos(2. * PI * (hi + 1.) * result->f * orig->d[t][ri] -
         result->h[hi].t0), 2);
      sin2sum = orig->r - cos2sum;
      axis1 = 2. * fabs(orig->r * cos2sum);
      axis2 = 2. * fabs(orig->r * sin2sum);
      if (axis1 > axis2)
      {
         result->h[hi].a0 = axis2;
         result->h[hi].b0 = axis1;
         if (result->h[hi].t0 < .5 * PI) result->h[hi].t0 += .5 * PI;
         else result->h[hi].t0 -= .5 * PI;
      }
      else
      {
         result->h[hi].a0 = axis1;
         result->h[hi].b0 = axis2;
      }
      if (result->f == 0) result->h[hi].sig = 0;
      Diff_Level(ocomp, cdata, (hi + 1.) * result->f, ts, &n1, &n2);
      if (dmode > 1)
      {
         n2 = .5 * n1 * sin(result->h[hi].th) * sqrt(norm);
         n1 = .5 * n1 * cos(result->h[hi].th) * sqrt(norm);
      }
      else if (dmode > 0)
      {
         n1 *= sqrt(norm);
         n2 *= sqrt(norm);
      }
      if ((dmode == 2) && (pow(result->h[hi].amp, 2) <
         4. * (pow(n1, 2) + pow(n2, 2)))) result->h[hi].sig = 0;
      else if (ncomp == 0) result->h[hi].sig = orig->r * LG_E /
         pow(ts->rms, 2) * norm / scale * (pow(a - n1, 2) + pow(b - n2, 2));
      else result->h[hi].sig = orig->r * LG_E / pow(ts->rms, 2) *
         norm / scale * (pow((a - n1) * cos(result->h[hi].t0) +
         (b - n2) * sin(result->h[hi].t0), 2) / result->h[hi].a0 +
         pow((a - n1) * sin(result->h[hi].t0) -
         (b - n2) * cos(result->h[hi].t0), 2) / result->h[hi].b0) /
         (1. + 1. / ncomp);
      result->sig += result->h[hi].sig;
   }
   return result->sig;
}



double SigSpec_MaxSig(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   double* r,
   struct TS* ocomp,
   struct TS* cdata,
   double norm,
   double scale
   )

{
   int i;
   int hi;
   struct Result prev;
   struct Result next;
   if (result->f == 0) return 0;
   FILE* log;
   if (debug >= 0) log = fopen("SigSpec_MaxSig.log", "a");
   if (debug == 0) fprintf(log, "SigSpec_MaxSig: calloc prev.h\n");
   prev.h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   if (debug == 0) fprintf(log, "SigSpec_MaxSig: calloc next.h\n");
   next.h = (struct Harmonic*)calloc(oharm + 1, sizeof(struct Harmonic));
   if (result->f > *r) *r *= .5;
   prev.f = result->f - *r;
   prev.sig = SigSpec_Sig(orig, ts, &prev, ocomp, cdata, norm, scale);
   result->sig = SigSpec_Sig(orig, ts, result, ocomp, cdata, norm, scale);
   next.f = result->f + *r;
   next.sig = SigSpec_Sig(orig, ts, &next, ocomp, cdata, norm, scale);
   for (i = 0; *r > pow(10, 1 - DBL_DIG); i++)
   {
      if (debug > 0)
      {
         fprintf(log, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n", prev.f, prev.sig, result->f, result->sig, next.f, next.sig, *r);
         fflush(log);
      }
      if (result->f == 0)
      {
         if (debug == 0) fprintf(log, "SigSpec_MaxSig: free prev.h\n");
         free(prev.h);
         if (debug == 0) fprintf(log, "SigSpec_MaxSig: free next.h\n");
         free(next.h);
         return 0;
      }
      *r *= .5;
      if ((result->sig >= prev.sig) && (result->sig >= next.sig))
      {
         if ((prev.f = result->f - *r) == DBL_MIN) return -72;
         if ((prev.sig = SigSpec_Sig(orig, ts, &prev, ocomp, cdata, norm, scale)) ==
            DBL_MAX) return -72;
         if ((next.f = result->f + *r) == DBL_MIN) return -74;
         if ((next.sig = SigSpec_Sig(orig, ts, &next, ocomp, cdata, norm, scale)) ==
            DBL_MAX) return -74;
      }
      else if (prev.sig > next.sig)
      {
         next.f = result->f;
         next.sig = result->sig;
         result->f -= *r;
         if (((result->sig = SigSpec_Sig(orig, ts, result, ocomp, cdata, norm,
            scale)) == DBL_MAX) || (result->f == DBL_MIN)) return -73;
      }
      else
      {
         prev.f = result->f;
         prev.sig = result->sig;
         result->f += *r;
         if (((result->sig = SigSpec_Sig(orig, ts, result, ocomp, cdata, norm,
            scale)) == DBL_MAX) || (result->f == DBL_MIN)) return -73;
      }
   }
   if (debug == 0) fprintf(log, "SigSpec_MaxSig: free prev.h\n");
   free(prev.h);
   if (debug == 0) fprintf(log, "SigSpec_MaxSig: free next.h\n");
   free(next.h);
   if (debug >= 0) fclose(log);
   if (result->sig < 0) return 0;
   return result->sig;
}



double SigSpec_Sig(
   struct TS* orig,
   struct TS* ts,
   struct Result* result,
   struct TS* ocomp,
   struct TS* cdata,
   double norm,
   double scale
   )

{
   int hi;
   int ri;
   int si;
   double cossum;
   double cossum2;
   double sinsum;
   double sinsum2;
   double xcossum;
   double xsinsum;
   double axis1;
   double axis2;
   double n1;
   double n2;
   double a;
   double b;
   double sin2th0;
   double cos2th0;
   FILE* log;
   ri = 1;
   do Stat_ZeroMean(ts->d[0], orig->w, 0, orig->r - 1, orig->ss, ri, orig->nss,
      ts->d[0]);
   while (++ri <= orig->nss);
   ts->rms = Stat_SDev(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   ts->ppsc = Stat_PPScatter(ts->d[0], orig->w, 0, orig->r - 1, 0, 0, &ri);
   switch (mode)
   {
   case 1:
      return SigSpec_DFT(orig, ts, result, ocomp, cdata, norm, scale);
      break;
   case 2:
      return SigSpec_Lomb(orig, ts, result, ocomp, cdata, norm, scale);
      break;
   default:
      break;
   }
   if (debug >= 0) log = fopen("SigSpec_Sig.log", "a");
   result->sig = 0;
   for (hi = 0; hi <= oharm; hi++)
   {
      sin2th0 = 0;
      cos2th0 = 0;
      a = 0;
      b = 0;
      for (si = 1; si <= orig->nss; si++)
      {
         cossum = 0;
         cossum2 = 0;
         sinsum = 0;
         sinsum2 = 0;
         xcossum = 0;
         xsinsum = 0;
         for (ri = 0; ri < orig->r; ri++)
            if ((orig->nss < 2) || (orig->ss[ri] == si))
         {
            cossum += orig->w[ri] * cos(2. * PI * (hi + 1.) * result->f *
               orig->d[t][ri]);
            sinsum += orig->w[ri] * sin(2. * PI * (hi + 1.) * result->f *
               orig->d[t][ri]);
            cossum2 += orig->w[ri] * cos(4. * PI * (hi + 1.) * result->f *
               orig->d[t][ri]);
            sinsum2 += orig->w[ri] * sin(4. * PI * (hi + 1.) * result->f *
               orig->d[t][ri]);
            xcossum += orig->w[ri] * ts->d[0][ri] *
               cos(2. * PI * (hi + 1.) * result->f * orig->d[t][ri]);
            xsinsum += orig->w[ri] * ts->d[0][ri] *
               sin(2. * PI * (hi + 1.) * result->f * orig->d[t][ri]);
         }
         a += xcossum;
         b += xsinsum;
         sin2th0 += orig->nsse[si - 1] * sinsum2 - 2. * cossum * sinsum;
         cos2th0 += orig->nsse[si - 1] * cossum2 - pow(cossum, 2) +
            pow(sinsum, 2);
      }
      a /= (double)orig->r;
      b /= (double)orig->r;
      result->h[hi].amp = 2. * sqrt(pow(a, 2) + pow(b, 2));
      result->h[hi].th = atan2(b, a);
      result->h[hi].t0 = .5 * atan2(sin2th0, cos2th0);
      if (result->h[hi].t0 < 0) result->h[hi].t0 += PI;
      axis1 = 0;
      axis2 = 0;
      for (si = 1; si <= orig->nss; si++)
      {
         cossum = 0;
         cossum2 = 0;
         sinsum = 0;
         for (ri = 0; ri < orig->r; ri++)
            if ((orig->nss < 2) || (orig->ss[ri] == si))
         {
            cossum += orig->w[ri] * cos(2. * PI * (hi + 1.) * result->f *
               orig->d[t][ri] - result->h[hi].t0);
            cossum2 += orig->w[ri] * pow(cos(2. * PI * (hi + 1.) * result->f *
               orig->d[t][ri] - result->h[hi].t0), 2);
            sinsum += orig->w[ri] * sin(2. * PI * (hi + 1.) * result->f *
               orig->d[t][ri] - result->h[hi].t0);
         }
         sinsum2 = orig->nsse[si - 1] - cossum2;
         axis1 += cossum2 - pow(cossum, 2) / orig->nsse[si - 1];
         axis2 += sinsum2 - pow(sinsum, 2) / orig->nsse[si - 1];
      }
      if (fabs(axis1) > fabs(axis2))
      {
         result->h[hi].a0 = 2. / orig->r * fabs(axis2);
         result->h[hi].b0 = 2. / orig->r * fabs(axis1);
         if (result->h[hi].t0 < .5 * PI) result->h[hi].t0 += .5 * PI;
         else result->h[hi].t0 -= .5 * PI;
      }
      else
      {
         result->h[hi].a0 = 2. / orig->r * fabs(axis1);
         result->h[hi].b0 = 2. / orig->r * fabs(axis2);
      }
      if (result->f == 0) result->h[hi].sig = 0;
      if (debug > 0)
      {
         fprintf(log, "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n",
            (hi + 1.) * result->f, 
            orig->r * LG_E / pow(ts->rms, 2) * (pow(a * cos(result->h[hi].t0) + b * sin(result->h[hi].t0), 2) / result->h[hi].a0 + pow(a * sin(result->h[hi].t0) - b * cos(result->h[hi].t0), 2) / result->h[hi].b0),
            result->h[hi].a0, result->h[hi].b0, result->h[hi].t0
            );
            fflush(log);
      }
      Diff_Level(ocomp, cdata, (hi + 1.) * result->f, ts, &n1, &n2);
      if (dmode > 1)
      {
         n2 = .5 * n1 * sin(result->h[hi].th) * sqrt(norm);
         n1 = .5 * n1 * cos(result->h[hi].th) * sqrt(norm);
      }
      else if (dmode > 0)
      {
         n1 *= sqrt(norm);
         n2 *= sqrt(norm);
      }
      if ((dmode == 2) && (pow(result->h[hi].amp, 2) <
         4. * (pow(n1, 2) + pow(n2, 2)))) result->h[hi].sig = 0;
      else if (ncomp == 0) result->h[hi].sig = orig->r * LG_E /
         pow(ts->rms, 2) * norm / scale *
         (pow((a - n1) * cos(result->h[hi].t0) +
         (b - n2) * sin(result->h[hi].t0), 2) / result->h[hi].a0 +
         pow((a - n1) * sin(result->h[hi].t0) - (b - n2) *
         cos(result->h[hi].t0), 2) / result->h[hi].b0);
      else result->h[hi].sig = orig->r * LG_E / pow(ts->rms, 2) *
         norm / scale * (pow((a - n1) * cos(result->h[hi].t0) +
         (b - n2) * sin(result->h[hi].t0), 2) / result->h[hi].a0 +
         pow((a - n1) * sin(result->h[hi].t0) -
         (b - n2) * cos(result->h[hi].t0), 2) / result->h[hi].b0) /
         (1. + 1. / ncomp);
      result->sig += result->h[hi].sig;
   }
   if (debug >= 0) fclose(log);
   return result->sig;
}



double SigSpec_SigSpec(
   struct TS* orig,
   struct TS* ts,
   struct Profile* prof,
   struct SigSpec* sigspec,
   struct SigSpec* compspec
   )

{
   int ri;
   int fi;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double fmax = -1;
   double sigmax = 0;
   double freq;
   int useharm = 0;
   int hasmax = 0;
   FILE* log;
   if (debug >= 0) log = fopen("SigSpec_SigSpec.log", "a");
   sigspec->norm = 0;
   sigspec->scale = 0;
   for (fi = 0; fi < nspr; fi++)
   {
      sigspec->a[fi] = 0;
      sigspec->b[fi] = 0;
   }
   for (ri = 0; ri < orig->r; ri++)
   {
      curcos = ts->d[0][ri] * cos(2. * PI * lf * orig->d[t][ri]) / orig->r;
      cursin = ts->d[0][ri] * sin(2. * PI * lf * orig->d[t][ri]) / orig->r;
      dcos = cos(2. * PI * fs * orig->d[t][ri]);
      dsin = sin(2. * PI * fs * orig->d[t][ri]);
      for (fi = 0; fi < nspr; fi++)
      {
         sigspec->a[fi] += orig->w[ri] * curcos;
         sigspec->b[fi] += orig->w[ri] * cursin;
         dbuf = curcos * dcos - cursin * dsin;
         cursin = cursin * dcos + curcos * dsin;
         curcos = dbuf;
         if ((!quiet) && (aad < 2))
            Log_Percent("significance spectrum: Fourier Coefficients",
            (double)ri * nspr + fi, (double)orig->r * nspr);
      }
   }
   for (fi = 0; fi < nspr; fi++)
      sigspec->norm += 4. * (pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2));
   sigspec->norm /= nspr;
   switch (dmode)
   {
   case 0:
      sigspec->scale = sigspec->norm * nspr;
      break;
   case 1:
      for (fi = 0; fi < nspr; fi++)
      {
         dcos = compspec->a[fi] * sqrt(sigspec->norm);
         dsin = compspec->b[fi] * sqrt(sigspec->norm);
         sigspec->scale += 4. * (pow(sigspec->a[fi] - dcos, 2) +
            pow(sigspec->b[fi] - dsin, 2));
      }
      break;
   case 2:
      for (fi = 0; fi < nspr; fi++)
      {
         dcos = .5 * compspec->a[fi] * sqrt(sigspec->norm) * sigspec->a[fi] /
            sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2));
         dsin = .5 * compspec->a[fi] * sqrt(sigspec->norm) * sigspec->b[fi] /
            sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2));
         sigspec->scale += 4. * (pow(sigspec->a[fi] - dcos, 2) +
            pow(sigspec->b[fi] - dsin, 2));
      }
      break;
   default:
      break;
   }
   sigspec->scale /= nspr;
   for (fi = 0; fi < nspr; fi++)
   {
      switch (dmode)
      {
      case 0:
         dcos = 0;
         dsin = 0;
         break;
      case 1:
         dcos = compspec->a[fi] * sqrt(sigspec->norm);
         dsin = compspec->b[fi] * sqrt(sigspec->norm);
         sigspec->ca[fi] = dcos;
         sigspec->cb[fi] = dsin;
         break;
      case 2:
         dcos = .5 * compspec->a[fi] * sqrt(sigspec->norm) * sigspec->a[fi] /
            sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2));
         dsin = .5 * compspec->a[fi] * sqrt(sigspec->norm) * sigspec->b[fi] /
            sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2));
         compspec->a[fi] *= sqrt(sigspec->norm);
         sigspec->ca[fi] = dcos;
         sigspec->cb[fi] = dsin;
         break;
      default:
         break;
      }
      if (((freq = lf + fs * fi) == 0) ||
         ((dmode != 1) && (pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2) <
         pow(dcos, 2) + pow(dsin, 2)))) sigspec->sig[fi] = 0;
      else if (ncomp == 0)
      {
         sigspec->sig[fi] =
            LG_E * orig->r / pow(ts->rms, 2) * 
            (pow((sigspec->a[fi] * cos(prof->t0[fi]) +
            sigspec->b[fi] * sin(prof->t0[fi])) /
            prof->a0[fi], 2) + pow((sigspec->b[fi] *
            cos(prof->t0[fi]) - sigspec->a[fi] *
            sin(prof->t0[fi])) / prof->b0[fi], 2));
         if (debug > 2)
         {
            fprintf(log, "0994       f %lg ", freq);
            fprintf(log,
               "rows %i rms %lg a0 %lg b0 %lg t0 %lg a %lg b %lg sig %lg\n",
               orig->r, ts->rms, prof->a0[fi], prof->b0[fi], prof->t0[fi],
               sigspec->a[fi], sigspec->b[fi], sigspec->sig[fi]);
         }
      }
      else
      {
         sigspec->sig[fi] =
         LG_E * orig->r / pow(ts->rms, 2) * sigspec->norm /
         sigspec->scale / (1. + 1. / ncomp) *
         (pow(((sigspec->a[fi] - dcos) * cos(prof->t0[fi]) +
         (sigspec->b[fi] - dsin) * sin(prof->t0[fi])) /
         prof->a0[fi], 2) + pow(((sigspec->b[fi] - dsin) *
         cos(prof->t0[fi]) - (sigspec->a[fi] - dcos) *
         sin(prof->t0[fi])) / prof->b0[fi], 2));
         if (debug > 2)
         {
            fprintf(log, "1013       ");
            fprintf(log,
               "%i comp datasets norm %lg scale %lg a_comp %lg b_comp %lg\n",
               ncomp, sigspec->norm, sigspec->scale, dcos, dsin);
            fprintf(log, "1017       f %lg ", freq);
            fprintf(log,
               "rows %i rms %lg a0 %lg b0 %lg t0 %lg a %lg b %lg sig %lg\n",
               orig->r, ts->rms, prof->a0[fi], prof->b0[fi], prof->t0[fi],
               sigspec->a[fi], sigspec->b[fi], sigspec->sig[fi]);
         }
      }
      if ((hasmax > 0) && (oharm < 2) && (sigspec->sig[fi] > sigmax))
      {
         if (debug > 2)
            fprintf(log, "1027       New maximum: f %lg sig %lg a %lg.\n", freq,
            sigspec->sig[fi],
            2. * sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2)));
         fmax = freq;
         sigmax = sigspec->sig[fi];
      }
      else if ((hasmax == 0) && (fi > 0) &&
         (sigspec->sig[fi] > sigspec->sig[fi - 1]))
      {
         if (debug > 2)
            fprintf(log, "1037       Initial maximum: f %lg sig %lg a %lg.\n",
            freq, sigspec->sig[fi],
            2. * sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2)));
         fmax = freq;
         sigmax = sigspec->sig[fi];
         hasmax = 1;
      }
      if ((!quiet) && (aad < 2))
         Log_Percent("significance spectrum: significance", (double)fi,
         (double)nspr);
   }
   if (oharm > 1) for (ri = 0; ri < nff; ri++)
   {
      for (fi = 1; fi <= oharm; fi++)
      {
         sigspec->sig[ri] += sigspec->sig[ri + fi * (ri + h0)];
         if ((!quiet) && (aad < 2))
            Log_Percent("significance spectrum: harmonics",
            (double)ri * oharm + fi, (double)nff * oharm);
      }
      if ((useharm > 0) && (sigspec->sig[ri] > sigmax))
      {
         if (debug > 2)
            fprintf(log, "1060       New maximum: f %lg sig %lg a %lg.\n",
            lf + fs * ri, sigspec->sig[fi],
            2. * sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2)));
         fmax = lf + fs * ri;
         sigmax = sigspec->sig[ri];
      }
      else if ((useharm > 0) && (hasmax == 0) && (fi > 0) &&
         (sigspec->sig[fi] > sigspec->sig[fi - 1]))
      {
         if (debug > 2)
            fprintf(log, "1070       Initial maximum: f %lg sig %lg a %lg.\n",
            lf + fs * ri, sigspec->sig[fi],
            2. * sqrt(pow(sigspec->a[fi], 2) + pow(sigspec->b[fi], 2)));
         fmax = lf + fs * ri;
         sigmax = sigspec->sig[fi];
         hasmax = 1;
      }
      if ((useharm < 1) && (ri > 1) && ((lf > 0) || (ri > 2)) &&
         (sigspec->sig[ri - 1] < sigspec->sig[ri])) useharm = 1;
   }
   if (debug > 0) fprintf(log, "1080 Maximum: f %lg sig %lg.\n", fmax, sigmax);
   if (debug >= 0) fclose(log);
   if (sigmax == 0) return -1;
   if (sigmax < 0) return 0;
   return fmax;
}



double SigSpec_SigSpecMax(
   struct TS* orig,
   struct TS* ts,
   struct Profile* prof,
   struct SigSpec* sigspec,
   struct Result* result,
   struct TS* ocomp,
   struct TS* cdata,
   struct SigSpec* compspec
   )

{
   double r = fs;
   if ((result->f = SigSpec_SigSpec(orig, ts, prof, sigspec, compspec)) <= 0)
      return 0;
   return SigSpec_MaxSig(orig, ts, result, &r, ocomp, cdata, sigspec->norm,
      sigspec->scale);
}



int Sim_Exp(
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar
   )

{
   int i;
   int j;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   for (j = 0; (i <= end) && (tdata[i] < simpar[1]); i++)
   {
      data[i] += simpar[2] * exp(simpar[4] * (tdata[i] - simpar[3]));
      j++;
   }
   return j;
}



int Sim_Poly(
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar
   )

{
   int i;
   int j;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   for (j = 0; (i <= end) && (tdata[i] < simpar[1]); i++)
   {
      if ((tdata[i] < simpar[3]) && (simpar[4] > floor(simpar[4]))) data[i] += 
         simpar[2] * pow(fabs(tdata[i] - simpar[3]), simpar[4]);
      else data[i] += simpar[2] * pow(tdata[i] - simpar[3], simpar[4]);
      j++;
   }
   return j;
}



int Sim_RndSteps(
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar,
   int* s
   )

{
   int i;
   int j;
   int br = 0;
   double mean;
   double norm;
   double n;
   int err;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   if (simpar[2] < 0) return -2;
   if (simpar[2] > 1) return -3;
   if (simpar[3] < 0) return -4;
   if ((*s < 0) && ((err = Rnd_Init(s)) != 0)) return -err;
   srand(*s);
   norm = sqrt(12. * GAUSSQUAL);
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   if (tdata[i] > simpar[1]) return 0;
   for (; (i <= end) && (tdata[i] < simpar[1]); i++)
   {
      br = 0;
      n = 0;
      for (j = 0; j < GAUSSQUAL; j++)
      {
         *s = (int)rand();
         n += (double)*s / RAND_MAX;
      }
      mean = (n / (double)GAUSSQUAL - .5) * norm * simpar[2];
      for (j = i; (j <= end) && (tdata[j] < simpar[1]) && (br == 0); j++)
      {
         data[j] += mean;
         *s = (int)rand();
         if ((j > 0) && ((double)*s / RAND_MAX > 
            exp(-fabs(tdata[j] - tdata[j-1]) / simpar[3])))
            br = 1;
      }
      i = j - 1;
   }
   return i;
}



int Sim_Run(
   struct TS* ts
   )

{
   FILE* F;
   struct String fname;
   char* pch;
   char cbuf[MAXCHAR];
   int ibuf;
   double dbuf;
   int err;
   int seed = -1;               
   double simpar[SIMPAR];       
   if (simmode == 0)
   {
      if (!quiet) Log_Header("simulator: replace");
      Vector_Flush(ts->d[x], 0, ts->r - 1);
   }
   else if (!quiet) Log_Header("simulator: add");
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(NULL, -1, -1,
      -1, String_AsChars(&project, cbuf), -1, -1, "ini")),sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 52;
   free(fname.c);
   while ((err = fscanf(F, "%s", cbuf)) == 1)
   {
      if (cbuf[0] == '#') while (fgetc(F) != '\n');
      else if (strcmp(cbuf, "sim:temporal") == 0)
      {
         err = fscanf(F, "%lf%lf%lf%lf%lf%lf", &(simpar[0]), &(simpar[1]),
            &(simpar[2]), &(simpar[3]), &(simpar[4]), &(simpar[5]));
         if (!quiet)
         {
            printf("temporal correlation\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         if ((ibuf = Sim_TempCorr(ts->d[t], ts->d[x], 0, ts->r - 1, simpar,
            &seed)) < 0) return 53;
      }
      else if (strcmp(cbuf, "sim:serial") == 0)
      {
         err = fscanf(F, "%lf%lf%lf%lf%lf%lf", &(simpar[0]), &(simpar[1]),
            &(simpar[2]), &(simpar[3]), &(simpar[4]), &(simpar[5]));
         if (!quiet)
         {
            printf("serial correlation\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         if ((ibuf = Sim_SerCorr(ts->d[t], ts->d[x], 0, ts->r - 1, simpar,
            &seed)) < 0)
            return 54;
      }
      else if (strcmp(cbuf, "sim:signal") == 0)
      {
         err = fscanf(F, "%lf%lf%lf%lf%lf", &(simpar[0]), &(simpar[1]),
            &(simpar[2]), &(simpar[3]), &(simpar[4]));
         if (!quiet)
         {
            printf("signal\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         if ((ibuf = Sim_Signal(ts->d[t], ts->d[x], 0, ts->r - 1, simpar)) <
            0) return 55;
      }
      else if (strcmp(cbuf, "sim:poly") == 0)
      {
         err = fscanf(F, "%lf%lf%lf%lf%lf", &(simpar[0]), &(simpar[1]),
            &(simpar[2]), &(simpar[3]), &(simpar[4]));
         if (!quiet)
         {
            printf("polynomial trend\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         if ((ibuf = Sim_Poly(ts->d[t], ts->d[x], 0, ts->r - 1, simpar)) < 0)
            return 56;
      }
      else if (strcmp(cbuf, "sim:exp") == 0)
      {
         err = fscanf(F, "%lf%lf%lf%lf%lf", &(simpar[0]), &(simpar[1]),
            &(simpar[2]), &(simpar[3]), &(simpar[4]));
         if (!quiet)
         {
            printf("exponential trend\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         if ((ibuf = Sim_Exp(ts->d[t], ts->d[x], 0, ts->r - 1, simpar)) < 0)
            return 57;
      }
      else if (strcmp(cbuf, "sim:zeromean") == 0)
      {
         err = fscanf(F, "%lf%lf", &(simpar[0]), &(simpar[1]));
         if (!quiet)
         {
            printf("zero-mean correction\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         dbuf = Sim_ZeroMean(ts->d[t], ts->d[x], NULL, 0, ts->r - 1, ts->ss,
            ts->nss, simpar);
      }
      else if (strcmp(cbuf, "sim:rndsteps") == 0)
      {
         err = fscanf(F, "%lf%lf%lf%lf", &(simpar[0]), &(simpar[1]), &(simpar[2]),
            &(simpar[3]));
         if (!quiet)
         {
            printf("random steps\n");
            fflush(stdout);
         }
         while (fgetc(F) != '\n');
         if ((ibuf = Sim_RndSteps(ts->d[t], ts->d[x], 0, ts->r - 1, simpar,
            &seed)) < 0) return 58;
      }
   }
   fclose(F);
   String_AsChars(&project, cbuf);
   pch = strtok(cbuf, "/");
   while (pch != NULL)
   {
      sprintf (cbuf, "%s", pch);
      pch = strtok(NULL, "/");
   }
   fname.c = (int*)calloc(String_SetLength(&fname,
      File_NameLength(&project, ts->fi, -1, -1, cbuf, -1, -1, "dat")),
      sizeof(int));
   File_Name(&project, ts->fi, -1, -1, cbuf, -1, -1, "dat", &fname);
   String_AsChars(&fname, cbuf);
   free(fname.c);
   if ((F = fopen(cbuf, "w")) == NULL) return Log_ErrorMessage(191);
   if (DataFile_Write(F, ts->d, ts->s, 0, ts->c - 1, 0, ts->r - 1, ts->cf,
      ts->cw) < 0) return 59;
   fclose(F);
   if ((seed >= 0) && ((err = Rnd_Write(&seed)) != 0)) return err;
   return 0;
}



int Sim_SerCorr(
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar,
   int* s
   )

{
   int i;
   int j;
   int k = 0;
   double norm;
   double n = 0;
   double prev;
   int err;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   if (simpar[5] < 0) return -2;
   if (simpar[5] > 1) return -3;
   if ((*s < 0) && ((err = Rnd_Init(s)) != 0)) return -err;
   srand(*s);
   norm = sqrt(12. * GAUSSQUAL);
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   if (tdata[i] > simpar[1]) return 0;
   for (j = 0; j < GAUSSQUAL; j++)
   {
      *s = (int)rand();
      n += (double)*s / RAND_MAX;
   }
   prev = (n / (double)GAUSSQUAL - .5) * norm * fabs(simpar[2] * 
         pow(tdata[i] - simpar[3], simpar[4]));
   data[i] += prev;
   k++;
   for (++i; (i <= end) && (tdata[i] < simpar[1]); i++)
   {
      n = 0;
      for (j = 0; j < GAUSSQUAL; j++)
      {
         *s = (int)rand();
         n += (double)*s / RAND_MAX;
      }
      prev = simpar[5] * prev + sqrt(1. - pow(simpar[5], 2)) * 
         (n / (double)GAUSSQUAL - .5) * norm * fabs(simpar[2] * 
         pow(tdata[i] - simpar[3], simpar[4]));
      data[i] += prev;
      k++;
   }
   return k;
}



int Sim_Signal(
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar
   )

{
   int i;
   int j;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   for (j = 0; (i <= end) && (tdata[i] < simpar[1]); i++)
   {
      data[i] += simpar[2] * cos(2 * PI * simpar[4] * (tdata[i] - simpar[3]));
      j++;
   }
   return j;
}



int Sim_TempCorr(
   double* tdata,
   double* data,
   int start,
   int end,
   double* simpar,
   int* s
   )

{
   int i;
   int j;
   int k = 0;
   double norm;
   double n = 0;
   double prev;
   int err;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   if (simpar[5] < 0) return -2;
   if (simpar[5] > 1) return -3;
   if ((*s < 0) && ((err = Rnd_Init(s)) != 0)) return -err;
   srand(*s);
   norm = sqrt(12. * GAUSSQUAL);
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   if (tdata[i] > simpar[1]) return 0;
   for (j = 0; j < GAUSSQUAL; j++)
   {
      *s = (int)rand();
      n += (double)*s / RAND_MAX;
   }
   prev = (n / (double)GAUSSQUAL - .5) * norm * fabs(simpar[2] * 
         pow(tdata[i] - simpar[3], simpar[4]));
   data[i] += prev;
   k++;
   for (++i; (i <= end) && (tdata[i] < simpar[1]); i++)
   {
      n = 0;
      for (j = 0; j < GAUSSQUAL; j++)
      {
         *s = (int)rand();
         n += (double)*s / RAND_MAX;
      }
      prev = pow(simpar[5], fabs(tdata[i] - tdata[i - 1])) * prev + 
         sqrt(1. - pow(simpar[5], 2 * fabs(tdata[i] - tdata[i - 1]))) * 
         (n / (double)GAUSSQUAL - .5) * norm * fabs(simpar[2] * 
         pow(tdata[i] - simpar[3], simpar[4]));
      data[i] += prev;
      k++;
   }
   return k;
}



double Sim_ZeroMean(
   double* tdata,
   double* data,
   double* weight,
   int start,
   int end,
   int* ssid,
   int nss,
   double* simpar
   )

{
   int i;
   int j;
   int s;
   if ((simpar[0] == 0) && (simpar[1] == 0))
   {
      simpar[0] = DBL_MIN;
      simpar[1] = DBL_MAX;
   }
   if (simpar[0] >= simpar[1]) return -1;
   for (i = start; (i <= end) && (tdata[i] < simpar[0]); i++);
   for (j = i; (j <= end) && (tdata[j] < simpar[1]); j++);
   if (zeromean == 0) zeromean = -1;
   for (s = 0; s < nss; s++) Stat_ZeroMean(data, weight, i, j - 1, ssid, s, nss,
      data);
   if (zeromean == -1) zeromean = 0;
   return 0;
}



int Sock_Cartesian(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int fi;
   int ph;
   int fill;
   int i;
   double dbuf;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "sock", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "sock", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "sock", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "sock", -1, -1, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 80;
   free(fname.c);
   if (lf == 0)
   {
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         0., 0., 0., 0., PI, 0.);
      fflush(F);
   }
   else for (ph = 0; ph < ceil(sockph / pow(prof->a0[fi - 1], 2)); ph++)
   {
      dbuf = pow(cos(PI * ph / sockph * pow(prof->a0[0], 2)) / prof->a0[0], 2) +
         pow(sin(PI * ph / sockph * pow(prof->a0[0], 2)) / prof->b0[0], 2);
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         lf, - dbuf * cos(PI * ph / sockph * pow(prof->a0[0], 2)),
         - dbuf * sin(PI * ph / sockph * pow(prof->a0[0], 2)),
         lf, dbuf * cos(PI * ph / sockph * pow(prof->a0[0], 2)),
         dbuf * sin(PI * ph / sockph * pow(prof->a0[0], 2)));
      fflush(F);
   }
   for (fi = 1; fi < nff; fi++)
   {
      fill = (int)ceil(fabs(1. / pow(prof->a0[fi], 2) -
         1. / pow(prof->a0[fi - 1], 2)) * sockfill);
      for (i = 1; i < fill; i++)
      {
         if (lf + fs * (fi - 1.) + fs * i / fill == 0)
         {
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               0., 0., 0., 0., PI, 0.);
            fflush(F);
         }
         else for (ph = 0; ph < ceil(sockph * (1. / pow(prof->a0[fi - 1], 2) +
            i / fill * (1. / pow(prof->a0[fi], 2) -
            1. / pow(prof->a0[fi - 1], 2)))); ph++)
         {
            dbuf = pow(cos(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) +
               i / fill *
               (1. / pow(prof->a0[fi], 2) - pow(1. / prof->a0[fi - 1], 2)))) /
               (prof->a0[fi - 1] +
               (prof->a0[fi] - prof->a0[fi - 1]) * i / fill), 2) +
               pow(sin(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) +
               i / fill *
               (1. / pow(prof->a0[fi], 2) - 1. / pow(prof->a0[fi - 1], 2)))) /
               (prof->b0[fi - 1] +
               (prof->b0[fi] - prof->b0[fi - 1]) * i / fill), 2);
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               lf + fs * (fi - 1.) + fs * i / fill,
               - dbuf * cos(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) + i / fill *
               fabs((1. / pow(prof->a0[fi], 2) -
               1. / pow(prof->a0[fi - 1], 2))))),
               - dbuf * sin(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) + i / fill *
               fabs((1. / pow(prof->a0[fi], 2) -
               1. / pow(prof->a0[fi - 1], 2))))),
               lf + fs * (fi - 1.) + fs * i / fill,
               dbuf * cos(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) + i / fill *
               fabs((1. / pow(prof->a0[fi], 2) -
               1. / pow(prof->a0[fi - 1], 2))))),
               dbuf * sin(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) + i / fill *
               fabs((1. / pow(prof->a0[fi], 2) -
               1. / pow(prof->a0[fi - 1], 2))))));
            fflush(F);
            if (!quiet) Log_Percent("sock diagram",
               (double)fi * fill * ceil(sockph / pow(prof->a0[0], 2)) +
               i * ceil(sockph / pow(prof->a0[0], 2)) + ph,
               (double)nff * fill * ceil(sockph / pow(prof->a0[0], 2)));
         }
      }
      if (lf + fs * fi == 0)
      {
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            0., 0., 0., 0., PI, 0.);
         fflush(F);
      }
      else for (ph = 0; ph < ceil(sockph / pow(prof->a0[fi], 2)); ph++)
      {
         dbuf = pow(cos(PI * ph / sockph * pow(prof->a0[fi], 2)) /
            prof->a0[fi], 2) +
            pow(sin(PI * ph / sockph * pow(prof->a0[fi], 2)) / prof->b0[fi], 2);
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            lf + fs * fi,
            - dbuf * cos(PI * ph / sockph * pow(prof->a0[fi], 2)),
            - dbuf * sin(PI * ph / sockph * pow(prof->a0[fi], 2)),
            lf + fs * fi,
            dbuf * cos(PI * ph / sockph * pow(prof->a0[fi], 2)),
            dbuf * sin(PI * ph / sockph * pow(prof->a0[fi], 2)));
         fflush(F);
      }
      if (!quiet) Log_Percent("sock diagram", (double)fi, (double)nff);
   }
   fclose(F);
   return 0;
}



int Sock_Colours(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   FILE* F;
   struct String fname;
   char cbuf[MAXCHAR];
   int ibuf;
   double* rgb[4];
   double* data[4];
   int ri;
   int ci = 0;
   int rows;
   int rgbs = IniFile_SockColours();
   double lower = DBL_MAX;
   double upper = -DBL_MAX;
   double red;
   double green;
   double blue;
   if (rgbs < 0) return 0;
   if (!quiet)
   {
      printf("\rsock diagram: colours                                        ");
      fflush(stdout);
   }
   rgb[0] = (double*)calloc(rgbs, sizeof(double));
   rgb[1] = (double*)calloc(rgbs, sizeof(double));
   rgb[2] = (double*)calloc(rgbs, sizeof(double));
   rgb[3] = (double*)calloc(rgbs, sizeof(double));
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(NULL, -1, -1,
      -1, String_AsChars(&project, cbuf), -1, -1, "ini")),sizeof(int));
   File_Name(NULL, -1, -1, -1, cbuf, -1, -1, "ini", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 81;
   free(fname.c);
   while (fscanf(F, "%s", cbuf) == 1)
   {
      if (strcmp(cbuf, "sock:colour") == 0)
      {
         if (fscanf(F, "%lf%lf%lf%lf", &rgb[0][ci], &rgb[1][ci],
            &rgb[2][ci], &rgb[3][ci]) != 4) return 82;
         if (rgb[0][ci] < 0) return 83;
         if (rgb[1][ci] < 0) return 84;
         if (rgb[2][ci] < 0) return 85;
         if (rgb[3][ci] < 0) return 86;
         while (fgetc(F) != '\n');
         ci++;
      }
   }
   fclose(F);
   if (Dataset_NSortAsc(rgb, NULL, 3, 0, 3, -1, -1, 0, rgbs - 1, NULL,
      "sock diagram: initialise") < 0) return 93;
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "sock", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "sock", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "sock", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "sock", -1, -1, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 87;
   free(fname.c);
   if ((rows = DataFile_CountRows(F)) < 1) return 88;
   rewind(F);
   data[0] = (double*)calloc(rows, sizeof(double));
   data[1] = (double*)calloc(rows, sizeof(double));
   data[2] = (double*)calloc(rows, sizeof(double));
   data[3] = (double*)calloc(rows, sizeof(double));
   for (ri = 0; ri < rows; ri++)
   {
      if (fscanf(F, "%lf%lf%lf", &data[0][ri], &data[1][ri], &data[2][ri]) != 3)
         return 89;
      while (((ibuf = fgetc(F)) != '\n') && (ibuf != EOF));
   }
   fclose(F);
   switch (sockcoord)
   {
   case 0:
      for (ri = 0; ri < rows; ri++)
      {
         data[3][ri] = data[2][ri];
         if (data[3][ri] < lower) lower = data[3][ri];
         if (data[3][ri] > upper) upper = data[3][ri];
      }
      break;
   case 1:
      for (ri = 0; ri < rows; ri++)
      {
         data[3][ri] = sqrt(pow(data[1][ri], 2) + pow(data[2][ri], 2));
         if (data[3][ri] < lower) lower = data[3][ri];
         if (data[3][ri] > upper) upper = data[3][ri];
      }
      break;
   default:
      return 90;
      break;
   }
   if (sockcolmodel == 1)
   {
      if (Dataset_NSortAsc(data, NULL, 3, 0, 3, -1, -1, 0, rows - 1, NULL,
         "sock diagram: rank") < 0) return 91;
      for (ri = 0; ri < rows; ri++) data[3][ri] = 1. * ri / rows;
      lower = 0;
      upper = rows - 1;
   }
   if ((F = fopen(cbuf, "w")) == NULL) return 92;
   for (ri = 0; ri < rows; ri++)
   {
      if (data[3][ri] <= rgb[3][0])
      {
         red = rgb[0][0];
         green = rgb[1][0];
         blue = rgb[2][0];
      }
      else if (data[3][ri] >= rgb[3][rgbs - 1])
      {
         red = rgb[0][rgbs - 1];
         green = rgb[1][rgbs - 1];
         blue = rgb[2][rgbs - 1];
      }
      else
      {
         for (ci = 1; (ci < rgbs) &&
            (data[3][ri] > rgb[3][ci]); ci++);
         red = rgb[0][ci - 1] + (rgb[0][ci] - rgb[0][ci - 1]) * 
            (data[3][ri] - rgb[3][ci - 1]) / (rgb[3][ci] - rgb[3][ci - 1]);
         green = rgb[1][ci - 1] + (rgb[1][ci] - rgb[1][ci - 1]) * 
            (data[3][ri] - rgb[3][ci - 1]) / (rgb[3][ci] - rgb[3][ci - 1]);
         blue = rgb[2][ci - 1] + (rgb[2][ci] - rgb[2][ci - 1]) * 
            (data[3][ri] - rgb[3][ci - 1]) / (rgb[3][ci] - rgb[3][ci - 1]);
      }
      fprintf(F, "%24.16lf %24.16lf %24.16lf %8.0lf\n", data[0][ri], data[1][ri],
         data[2][ri], floor(red + .5) + 256. * floor(green + .5) + 
         02826. * floor(blue + .5));
      fflush(F);
      if (!quiet)
         Log_Percent("sock diagram: colours", (double)ri, (double)rows);
   }
   fclose(F);
   free(data[0]);
   free(data[1]);
   free(data[2]);
   free(data[3]);
   free(rgb[0]);
   free(rgb[1]);
   free(rgb[2]);
   free(rgb[3]);
   return 0;
}



int Sock_Cylindrical(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int fi;
   int ph;
   int fill;
   int i;
   double dbuf;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "sock", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "sock", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "sock", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "sock", -1, -1, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 79;
   free(fname.c);
   if (lf == 0)
   {
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         0., 0., 0., 0., PI, 0.);
      fflush(F);
   }
   else for (ph = 0; ph < ceil(sockph / pow(prof->a0[0], 2)); ph++)
   {
      dbuf = pow(cos(PI * ph / sockph * pow(prof->a0[0], 2)) / prof->a0[0], 2) +
         pow(sin(PI * ph / sockph * pow(prof->a0[0], 2)) / prof->b0[0], 2);
      fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
         lf, PI * ph / sockph * pow(prof->a0[0], 2) - PI, dbuf, lf,
         PI * ph / sockph * pow(prof->a0[0], 2), dbuf);
      fflush(F);
   }
   for (fi = 1; fi < nff; fi++)
   {
      fill = (int)ceil(fabs(1. / pow(prof->a0[fi], 2) -
         1. / pow(prof->a0[fi - 1], 2)) * sockfill);
      for (i = 1; i < fill; i++)
      {
         if (lf + fs * (fi - 1.) + fs * i / fill == 0)
         {
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               0., 0., 0., 0., PI, 0.);
            fflush(F);
         }
         else for (ph = 0; ph < ceil(sockph * (1. / pow(prof->a0[fi - 1], 2) +
            i / fill *
            (1. / pow(prof->a0[fi], 2) - 1. / pow(prof->a0[fi - 1], 2)))); ph++)
         {
            dbuf = pow(cos(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) +
               i / fill *
               (1. / pow(prof->a0[fi], 2) - 1. / pow(prof->a0[fi - 1], 2)))) /
               (prof->a0[fi - 1] +
               (prof->a0[fi] - prof->a0[fi - 1]) * i / fill), 2) +
               pow(sin(PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) +
               i / fill *
               (1. / pow(prof->a0[fi], 2) - 1. / pow(prof->a0[fi - 1], 2)))) /
               (prof->b0[fi - 1] +
               (prof->b0[fi] - prof->b0[fi - 1]) * i / fill), 2);
            fprintf(F,
               "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
               lf + fs * (fi - 1.) + fs * i / fill,
               PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) + i / fill *
               fabs((1. / pow(prof->a0[fi], 2) -
               1. / pow(prof->a0[fi - 1], 2)))) - PI,
               dbuf, lf + fs * (fi - 1.) + fs * i / fill,
               PI * ph / sockph / (1. / pow(prof->a0[fi - 1], 2) + i / fill *
               fabs((1. / pow(prof->a0[fi], 2) -
               1. / pow(prof->a0[fi - 1], 2)))), dbuf);
            fflush(F);
            if (!quiet) Log_Percent("sock diagram",
               (double)fi * fill * ceil(sockph / pow(prof->a0[0], 2)) +
               i * ceil(sockph / pow(prof->a0[0], 2)) + ph,
               (double)nff * fill * ceil(sockph / pow(prof->a0[0], 2)));
         }
      }
      if (lf + fs * fi == 0)
      {
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            0., 0., 0., 0., PI, 0.);
         fflush(F);
      }
      else for (ph = 0; ph < ceil(sockph / pow(prof->a0[fi], 2)); ph++)
      {
         dbuf = pow(cos(PI * ph / sockph * pow(prof->a0[fi], 2)) / prof->a0[fi], 2) +
            pow(sin(PI * ph / sockph * pow(prof->a0[fi], 2)) / prof->b0[fi], 2);
         fprintf(F, "%24.16lf %24.16lf %24.16lf\n%24.16lf %24.16lf %24.16lf\n", 
            lf + fs * fi, PI * ph / sockph * pow(prof->a0[fi], 2) - PI, dbuf,
            lf + fs * fi, PI * ph / sockph * pow(prof->a0[fi], 2), dbuf);
         fflush(F);
      }
      if (!quiet) Log_Percent("sock diagram", (double)fi, (double)nff);
   }
   if (!quiet) Log_Percent("sock diagram", (double)nff, (double)nff);
   fclose(F);
   return 0;
}



int Sock_Generate(
   struct TS* ts,
   struct Profile* prof,
   int ti
   )

{
   int err;
   if (!quiet)
   {
      printf("\rsock diagram                                                 ");
      fflush(stdout);
   }
   switch (sockcoord)
   {
   case 0:
      if ((err = Sock_Cylindrical(ts, prof, ti)) != 0) return err;
      break;
   case 1:
      if ((err = Sock_Cartesian(ts, prof, ti)) != 0) return err;
      break;
   default:
      return 71;
   }
   if ((sockcolmodel >= 0) && ((err = Sock_Colours(ts, prof, ti)) != 0))
      return err;
   return 0;
}



int Spec_Write(
   struct TS* ts,
   struct Profile* prof,
   struct SigSpec* sp,
   int ti
   )

{
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int corro = co;
   int err;
   int ri;
   if (ts->it < 0)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "resspec", -1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "resspec", -1, ti, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "s", ts->it, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "s", ts->it, ti, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 116;
   free(fname.c);
   if (dmode == 0) for (ri = 0; ri < nff; ri++)
   {
      fprintf(F,
         "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n",
         lf + fs * ri, sp->sig[ri] / (oharm + 1.),
         2. * sqrt(pow(sp->a[ri], 2) + pow(sp->b[ri], 2)),
         atan2(sp->b[ri], sp->a[ri]), prof->t0[ri], -1., 0.);
      if (!quiet)
         Log_Percent("write Fourier spectrum", (double)ri, (double)nff);
   }
   else for (ri = 0; ri < nff; ri++)
   {
      fprintf(F,
         "%24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf %24.16lf\n",
         lf + (uf - lf) / (nspr - 1.) * ri, sp->sig[ri] / (oharm + 1.),
         2. * sqrt(pow(sp->a[ri], 2) + pow(sp->b[ri], 2)),
         atan2(sp->b[ri], sp->a[ri]), prof->t0[ri],
         2. * sqrt(pow(sp->ca[ri], 2) + pow(sp->cb[ri], 2)),
         atan2(sp->cb[ri], sp->ca[ri]));
      if (!quiet)
         Log_Percent("write Fourier spectrum", ri + 1., (double)nff);
   }
   fclose(F);
   return 0;
}



int Subsets_Assign(
   double** data,
   struct String** str,
   int cols,
   int rows,
   int* sscol,
   int nssc,
   int* tscolf,
   int* tscolw,
   int* ssid,
   int* nsse
   )

{
   int i;
   int j;
   int k;
   int id;
   int eq;
   int count;
   int result = 0;
   double* comp = (double*)calloc(nssc, sizeof(double));
   struct String** cstr = (struct String**)calloc(nssc, sizeof(struct String*));
   if (nssc == 0) return 0;
   if ((!quiet) && (nsse != NULL)) Log_Header("analysing subsets");
   for (i = 0; i < rows; i++) ssid[i] = -1;
   for (i = 0; i < rows; i++) if (ssid[i] < 0)
   {
      if ((!quiet) && (nsse != NULL)) Log_Int("subset", ssid[i] = ++result);
      else ssid[i] = ++result;
      for (j = 0; j < nssc; j++)
      {
         if ((id = Dataset_GetIndex(sscol[j] - 1, tscolf)) > 0)
         {
            comp[j] = data[--id][i];
            if ((!quiet) && (nsse != NULL))
            {
               if (comp[j] == floor(comp[j])) printf("%.0lf ", comp[j]);
               else printf("%lf ", comp[j]);
               fflush(stdout);
            }
         }
         else
         {
            if (nsse != NULL)
            {
               String_Copy(&(str[-(++id)][i]), cstr[j], 0);
               if (!quiet)
               {
                  String_Display(cstr[j], -1);
                  printf(" ");
                  fflush(stdout);
               }
            }
         }
      }
      count = 1;
      for (k = i + 1; k < rows; k++)
      {
         eq = 1;
         for (j = 0; (j < nssc) && (eq > 0); j++)
         {
             if ((id = Dataset_GetIndex(sscol[j] - 1, tscolf)) > 0)
             {
                if (comp[j] != data[--id][k]) eq = 0;
             }
             else if (String_Comp(cstr[j], &(str[-(++id)][k]))) eq = 0;
         }
         if (eq > 0)
         {
            ssid[k] = result;
            count++;
         }
      }
      if (result == 0) result = 1;
      if (nsse != NULL)
      {
         if (!quiet)
         {
            printf("\n");
            Log_Int("entries", count);
         }
         nsse[result] = count;
         if (!quiet)
         {
            printf("-------------------------------");
            printf("-----------------------------\n");
            fflush(stdout);
         }
      }
   }
   free(comp);
   free(cstr);
   return result;
}



int Subsets_CountEntries(
   struct TS* ts
   )

{
   int ri;
   int ssi;
   ts->nsse[0] = ts->r;
   if (ts->nss < 2) return 0;
   for (ssi = 0; ssi < ts->nss; ssi++)
   {
      ts->nsse[ssi] = 0;
      for (ri = 0; ri < ts->r; ri++) if (ts->ss[ri] == ssi)
         ts->nsse[ts->ss[ri]]++;
   }
   return 0;
}



int TimeRes_Count(
   struct TS* ts,
   int ti
   )

{
   int ri;
   int result;
   if (trnum < 2) return ts->r;
   for (ri = 0; ts->d[t][ri] < ts->d[t][0] + ti * trstep; ri++);
   result = - ri;
   for (; (ri < ts->r) && (ts->d[t][ri] < ts->d[t][0] + ti * trstep +
      trwidth); ri++);
   result += ri;
   return result;
}



int TimeRes_Extract(
   struct TS* from,
   int ti,
   struct TS* to
   )

{
   int ci;
   int ri;
   int start;
   int end;
   int err;
   double norm = 0;
   double centre = trstart + ti * trstep + .5 * trwidth;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   double dbuf;
   for (ri = 0; from->d[t][ri] < trstart + ti * trstep; ri++);
   start = ri;
   end = start + to->r;
   for (ri = start; ri < end; ri++)
   {
      for (ci = 0; ci < to->n; ci++) to->d[ci][ri - start] = from->d[ci][ri];
      for (ci = 0; ci < to->c - to->n; ci++) String_Copy(&from->s[ci][ri],
         &to->s[ci][ri - start], 0);
      to->w[ri - start] = from->w[ri];
      to->ss[ri - start] = from->ss[ri];
   }
   if (trmode >= 0)
   {
      if (nmf < 2)
      {
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, -1, -1, -1, "wts", -1, ti, "dat")),
            sizeof(int));
         File_Name(&project, -1, -1, -1, "wts", -1, ti, "dat", &fname);
      }
      else
      {
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, from->fi, -1, -1, "wts", -1, ti, "dat")),
            sizeof(int));
         File_Name(&project, from->fi, -1, -1, "wts", -1, ti, "dat", &fname);
      }
      if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 115;
      free(fname.c);
   }
   switch (trmode)
   {
   case 0:
      for (ri = 0; ri < to->r; ri++) norm += to->w[ri];
      break;
   case 1:
      for (ri = 0; ri < to->r; ri++)
      {
         if (to->d[t][ri] == centre) fprintf(F, "%24.16lf %24.16lf\n",
            to->d[t][ri], dbuf = 0);
         else fprintf(F, "%24.16lf %24.16lf\n", to->d[t][ri],
            dbuf = pow(fabs(to->d[t][ri] - centre), - trpar));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 2:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[t][ri], dbuf =
            exp(- .5 * pow((to->d[t][ri] - centre) / trpar, 2)));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 3:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[t][ri], dbuf =
            exp(- fabs(to->d[t][ri] - centre) / trpar));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 4:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[t][ri], dbuf =
            cos(2 * PI * trpar * fabs(to->d[t][ri] - centre) - trph));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 5:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[t][ri], dbuf = 
            pow(cos(2 * PI * trpar * fabs(to->d[t][ri] - centre) - trph),
            trexp));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 6:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[t][ri], dbuf = 
            exp(- fabs(to->d[t][ri] - to->d[t][0]) / trpar));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   default:
      norm = to->r;
      break;
   }
   if (trmode >= 0) fclose(F);
   norm /= to->r;
   for (ri = 0; ri < to->r; ri++)
   {
      to->w[ri] /= norm;
   }
   if ((err = Subsets_CountEntries(to)) != 0) return err;
   return 0;
}



int TimeRes_Extract2C(
   struct TS* from,
   int ti,
   struct TS* to
   )

{
   int ci;
   int ri;
   int start;
   int end;
   int err;
   double norm = 0;
   double centre = trstart + ti * trstep + .5 * trwidth;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   double dbuf;
   for (ri = 0; from->d[t][ri] < trstart + ti * trstep; ri++);
   start = ri;
   end = start + to->r;
   for (ri = start; ri < end; ri++)
   {
      to->d[0][ri - start] = from->d[t][ri];
      to->d[1][ri - start] = from->d[x][ri];
      to->w[ri - start] = from->w[ri];
      to->ss[ri - start] = from->ss[ri];
   }
   if (trmode >= 0)
   {
      if (nmf < 2)
      {
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, -1, -1, -1, "wts", -1, ti, "dat")),
            sizeof(int));
         File_Name(&project, -1, -1, -1, "wts", -1, ti, "dat", &fname);
      }
      else
      {
         fname.c = (int*)calloc(String_SetLength(&fname,
            File_NameLength(&project, from->fi, -1, -1, "wts", -1, ti, "dat")),
            sizeof(int));
         File_Name(&project, from->fi, -1, -1, "wts", -1, ti, "dat", &fname);
      }
      if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 119;
      free(fname.c);
   }
   switch (trmode)
   {
   case 0:
      for (ri = 0; ri < to->r; ri++) norm += to->w[ri];
      break;
   case 1:
      for (ri = 0; ri < to->r; ri++)
      {
         if (to->d[0][ri] == centre) fprintf(F, "%24.16lf %24.16lf\n",
            to->d[0][ri], dbuf = 0);
         else fprintf(F, "%24.16lf %24.16lf\n", to->d[0][ri],
            dbuf = pow(fabs(to->d[0][ri] - centre), - trpar));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 2:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[0][ri], dbuf =
            exp(- .5 * pow((to->d[0][ri] - centre) / trpar, 2)));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 3:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[0][ri], dbuf =
            exp(- fabs(to->d[0][ri] - centre) / trpar));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 4:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[0][ri], dbuf =
            cos(2 * PI * trpar * fabs(to->d[0][ri] - centre) - trph));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 5:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[0][ri], dbuf = 
            pow(cos(2 * PI * trpar * fabs(to->d[0][ri] - centre) - trph),
            trexp));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   case 6:
      for (ri = 0; ri < to->r; ri++)
      {
         fprintf(F, "%24.16lf %24.16lf\n", to->d[0][ri], dbuf = 
            exp(- fabs(to->d[0][ri] - to->d[0][0]) / trpar));
         to->w[ri] *= dbuf;
         norm += to->w[ri];
      }
      break;
   default:
      norm = to->r;
      break;
   }
   if (trmode >= 0) fclose(F);
   norm /= to->r;
   for (ri = 0; ri < to->r; ri++) to->w[ri] /= norm;
   if ((err = Subsets_CountEntries(to)) != 0) return err;
   to->rms = Stat_SDev(to->d[1], to->w, 0, to->r - 1, 0, 0, &err);
   to->ppsc = Stat_PPScatter(to->d[1], to->w, 0, to->r - 1, 0, 0, &err);
   return 0;
}



int TimeRes_Par(
   struct TS* from,
   int ti,
   struct TS* to
   )

{
   to->fi = from->fi;
   to->it = from->it;
   to->c = from->c;
   to->n = from->n;
   if ((to->r = TimeRes_Count(from, ti)) <= 0) return 64;
   to->nss = from->nss;
   to->ct = from->ct;
   to->pi = from->pi;
   return 0;
}



int TimeRes_Par2C(
   struct TS* from,
   int ti,
   struct TS* to
   )

{
   to->fi = from->fi;
   to->it = from->it;
   to->c = 2;
   to->n = 2;
   if ((to->r = TimeRes_Count(from, ti)) <= 0) return 64;
   to->nss = from->nss;
   to->ct = from->ct;
   to->pi = from->pi;
   return 0;
}



int TimeRes_Prepare(
   struct TS* ts
   )

{
   int fi = 0;
   int ri;
   double tstart = DBL_MAX;
   double tend = -DBL_MAX;
   do if ((ts[fi].ct != 0) && (ts[fi].d[t][ts[fi].r - 1] - ts[fi].d[t][0] >
      tend - tstart))
   {
      tstart = ts[fi].d[t][0];
      tend = ts[fi].d[t][ts[fi].r - 1];
   } while (++fi < nmf);
   trstart = tstart;
   if (trmode < 0)
   {
      trwidth = tend - tstart;
      trnum = 1;
      trstep = trwidth;
   }
   else
   {
      trnum = (int)floor((tend - tstart - trwidth) / trstep + 1.5);
      trstep = (tend - tstart - trwidth) / (trnum - 1.);
   }
   if (trnum < 1) return 50;
   if (trstep <= 0) return 51;
   return 0;
}



int TS_AssignProfiles(
   struct TS* ts
   )

{
   int pi = 0;                  
   int fi;                      
   int ci;                      
   int ri;                      
   int match;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   for (fi = 0; fi < nmf; fi++) ts[fi].pi = -1;
   for (fi = 0; fi < nmf; fi++) if (ts[fi].ct == 1)
   {
      for (ci = 0; (ts[fi].pi == -1) && (ci < fi); ci++) if (ts[ci].ct == 1)
      {
         match = 1;
         if ((ts[ci].r != ts[fi].r) || (ts[ci].nss != ts[fi].nss)) match = 0;
         if (match > 0) for (ri = 0; (match == 1) && (ri < ts[fi].r); ri++)
            if ((ts[ci].d[t][ri] != ts[fi].d[t][ri]) ||
            (ts[ci].w[ri] != ts[fi].w[ri]) ||
            (ts[ci].ss[ri] != ts[fi].ss[ri])) match = 0;
         if (match > 0) ts[fi].pi = ts[ci].pi;
      }
      if (ts[fi].pi == -1) ts[fi].pi = pi++;
      else pi++;
   }
   return pi;
}



int TS_Check(
   struct TS* ts
   )

{
   int ci;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(NULL,
      ts->fi, -1, -1, String_AsChars(&project, cbuf), -1, -1, "dat")),
      sizeof(int));
   File_Name(NULL, ts->fi, -1, -1, cbuf, -1, -1, "dat", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 40;
   free(fname.c);
   for (ci = 0; ci < ts->c; ci++) ts->cw[ci] = DataFile_ColWidth(F, ci);
   ts->n = DataFile_NumericCols(F, ts->cf, 0, ts->c - 1, 0, ts->r - 1);
   fclose(F);
   if (ts->cf[t] != 1) return 41;
   if (ts->cf[x] != 1) return 42;
   for (ci = 0; ci < nwc; ci++) if (ts->cf[wraw[ci]] != 1) return 43;
   return 0;
}



int TS_Count(
   struct TS* ts
   )

{   
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(NULL,
      ts->fi, -1, -1, String_AsChars(&project, cbuf), -1, -1, "dat")),
      sizeof(int));
   File_Name(NULL, ts->fi, -1, -1, cbuf, -1, -1, "dat", &fname);
   String_AsChars(&fname, cbuf);
   if (!quiet)
   {
      printf("\r%s                              \n", cbuf);
      fflush(stdout);
   }
   if ((F = fopen(cbuf, "r")) == NULL) return 38;
   free(fname.c);
   ts->c = DataFile_CountCol(F);
   ts->r = DataFile_CountRows(F);
   fclose(F);
   if (ts->c < 2) return 39;
   return 0;
}



int TS_CountSubsetEntries(
   struct TS* ts
   )

{
   int i;
   int checksum = 0;
   if (ts->nss == 1)
   {
      ts->nsse[0] = ts->r;
      return 0;
   }
   for (i = 0; i < ts->nss; i++) ts->nsse[i] = 0;
   for (i = 0; i < ts->r; i++) ++ts->nsse[ts->ss[i] - 1];
   for (i = 0; i < ts->nss; i++) checksum += ts->nsse[i];
   if (checksum != ts->r) return 49;
   return 0;
}



int TS_Par(
   struct TS* from,
   struct TS* to
   )

{
   int i;
   to->fi = from->fi;
   to->it = from->it;
   to->c = from->c;
   to->n = from->n;
   to->r = from->r;
   to->nss = from->nss;
   to->ct = from->ct;
   to->pi = from->pi;
   to->rms = from->rms;
   to->ppsc = from->ppsc;
   to->meantime = from->meantime;
   return 0;
}



int TS_Read(
   struct TS* ts
   )

{
   int i;
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   int entries;
   fname.c = (int*)calloc(String_SetLength(&fname,
      File_NameLength(NULL, ts->fi, -1, -1, String_AsChars(&project, cbuf), -1,
      -1, "dat")), sizeof(int));
   File_Name(NULL, ts->fi, -1, -1, cbuf, -1, -1, "dat", &fname);
   if ((F = fopen(String_AsChars(&fname, cbuf), "r")) == NULL) return 45;
   free(fname.c);
   if ((entries = DataFile_Read(F, ts->d, ts->s, 0, ts->c - 1, 0, ts->r - 1,
      ts->cf)) != ts->c * ts->r) return 46;
   fclose(F);
   return 0;
}



int TS_Sort(
   struct TS* ts
   )

{
   int* width;
   int i;
   int j = 0;
   width = (int*)calloc(ts->c - ts->n, sizeof(int));
   j = 0;
   for (i = 0; i < ts->c; i++) if (ts->cf[i] == 0) width[j++] = ts->cw[i];
   if (quiet)
   {
      if (Dataset_NSortAsc(ts->d, ts->s, t, 0, ts->n - 1, 0, ts->c - ts->n - 1,
         0, ts->r - 1, width, NULL) < 0) return 47;
   }
   else if (Dataset_NSortAsc(ts->d, ts->s, t, 0, ts->n - 1, 0, ts->c - ts->n - 1,
         0, ts->r - 1, width, "time series: sort") < 0) return 47;
   free(width);
   return 0;
}



int TS_Statistics(
   struct TS* ts,
   int colt,
   int colx
   )

{
   int err;
   int i;
   ts->meantime = Stat_Mean(ts->d[colt], ts->w, 0, ts->r - 1, 0, 0, &err);
   ts->rms = Stat_SDev(ts->d[colx], ts->w, 0, ts->r - 1, 0, 0, &err);
   ts->ppsc = Stat_PPScatter(ts->d[colx], ts->w, 0, ts->r - 1, 0, 0, &err);
   return 0;
}



int TS_Subsets(
   struct TS* ts
   )

{
   int i;
   int j;
   int k;
   int id;
   int eq;
   int count;
   double* comp;
   struct String* cstr;
   for (i = 0; i < ts->r; i++) ts->ss[i] = -1;
   if (nssc <= 0)
   {
      ts->nss = 1;
      return 0;
   }
   comp = (double*)calloc(nssc, sizeof(double));
   cstr = (struct String*)calloc(nssc, sizeof(struct String));
   ts->nss = 0;
   for (i = 0; i < ts->r; i++) if (ts->ss[i] < 0)
   {
      ts->ss[i] = ++ts->nss;
      for (j = 0; j < nssc; j++)
      {
         if ((id = Dataset_GetIndex(ssraw[j], ts->cf)) > 0)
            comp[j] = ts->d[--id][i];
         else
         {
            cstr[j].c = (int*)calloc(String_SetLength(&(cstr[j]),
               String_GetLength(&(ts->s[-(++id)][i]))), sizeof(int));
            String_Copy(&(ts->s[-id][i]), &(cstr[j]), 0);
         }
      }
      count = 1;
      for (k = i + 1; k < ts->r; k++)
      {
         eq = 1;
         for (j = 0; (j < nssc) && (eq > 0); j++)
         {
             if ((id = Dataset_GetIndex(ssraw[j], ts->cf)) > 0)
             {
                if (comp[j] != ts->d[--id][k]) eq = 0;
             }
             else if (String_Comp(&(cstr[j]), &(ts->s[-(++id)][k]))) eq = 0;
         }
         if (eq > 0)
         {
            ts->ss[k] = ts->nss;
            count++;
         }
      }
   }
   free(comp);
   for (j = 0; j < nssc; j++) free(cstr[j].c);
   free(cstr);
   return 0;
}



int TS_TimeShift(
   struct TS* ts,
   int colt,
   double shift
   )

{
   int i;
   for (i = 0; i < ts->r; i++) ts->d[colt][i] += shift;
   return 0;
}



int TS_Weights(
   struct TS* ts
   )

{
   double wsum = 0;
   int ci;
   int ri;
   for (ci = 0; ci < nwc; ci++) for (ri = 0; ri < ts->r; ri++)
      ts->w[ri] *= pow(ts->d[wraw[ci]][ri], wx[ci]);
   for (ri = 0; ri < ts->r; ri++) wsum += ts->w[ri];
   if (wsum == 0) return 48;
   wsum /= ts->r;
   for (ri = 0; ri < ts->r; ri++) ts->w[ri] /= wsum;
   return 0;
}



int TS_Write(
   struct TS* orig,
   struct TS* ts,
   int ti
   )

{
   struct String fname;
   FILE* F;
   char cbuf[MAXCHAR];
   int corro = co;
   int err;
   int ri;
   int ci;
   int ni;
   if (ts->it < 0)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, ti, -1, "residuals", -1, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "residuals", -1, ti, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "t", ts->it, ti, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "t", ts->it, ti, "dat", &fname);
   }
   if ((F = fopen(String_AsChars(&fname, cbuf), "w")) == NULL) return 117;
   free(fname.c);
   for (ri = 0; ri < orig->r; ri++)
   {
      ni = 0;
      for (ci = 0; ci < orig->c; ci++)
      {
         if (orig->cf[ci] == 1)
         {
            if (ni == x) fprintf(F, "%24.16lf ", ts->d[0][ri]);
            else fprintf(F, "%24.16lf ", orig->d[ni][ri]);
            ++ni;
         }
         else String_Write(&orig->s[ci - ni][ri], F, orig->cw[ci] + 1);
      }
      fprintf(F, "\n");
      if (!quiet)
         Log_Percent("write time series", ri + 1., (double)orig->r);
   }
   fclose(F);
   return 0;
}



int Win_Generate(
   struct TS* ts,
   int ti
   )

{
   int ri;
   int fi;
   double curcos;
   double cursin;
   double dcos;
   double dsin;
   double dbuf;
   double* win[2];
   struct String fname;
   char cbuf[MAXCHAR];
   FILE* F;
   if (!quiet)
   {
      printf("\rspectral window                                              ");
      fflush(stdout);
   }
   win[0] = (double*)calloc(nspr, sizeof(double));
   win[1] = (double*)calloc(nspr, sizeof(double));
   for (fi = 0; fi < nspr; fi++)
   {
      win[0][fi] = 0;
      win[1][fi] = 0;
   }
   for (ri = 0; ri < ts->r; ri++)
   {
      curcos = ts->w[ri] * cos(2. * PI * lf * ts->d[t][ri]);
      cursin = ts->w[ri] * sin(2. * PI * lf * ts->d[t][ri]);
      dcos = cos(2. * PI * fs * ts->d[t][ri]);
      dsin = sin(2. * PI * fs * ts->d[t][ri]);
      for (fi = 0; fi < nspr; fi++)
      {
         win[0][fi] += curcos;
         win[1][fi] += cursin;
         dbuf = curcos * dcos - cursin * dsin;
         cursin = cursin * dcos + curcos * dsin;
         curcos = dbuf;
         if (!quiet) Log_Percent("spectral window", (double)ri * nspr + fi,
            (double)ts->r * nspr);
      }
   }
   if (trnum > 1)
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "win", ti, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "win", ti, -1, "dat", &fname);
   }
   else
   {
      fname.c = (int*)calloc(String_SetLength(&fname, File_NameLength(&project,
         ts->fi, -1, -1, "win", -1, -1, "dat")), sizeof(int));
      File_Name(&project, ts->fi, -1, -1, "win", -1, -1, "dat", &fname);
   }
   String_AsChars(&fname, cbuf);
   free(fname.c);
   if ((F = fopen(cbuf, "w")) == NULL) return 69;
   for (fi = 0; fi < nspr; fi++) fprintf(F, "%24.16lf %24.16lf %24.16lf\n",
      lf + fi * fs,
      sqrt(pow(win[0][fi], 2) + pow(win[1][fi], 2)) / ts->r,
      atan2(win[1][fi], win[0][fi]));
   fclose(F);
   free(win[0]);
   free(win[1]);
   return 0;
}



